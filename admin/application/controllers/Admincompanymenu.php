<?php

//defined('BASEPATH') OR exit('No direct script access allowed');

//session_start(); //we need to start session in order to access it through CI

Class Admincompanymenu extends CI_Controller {

	public function __construct() {
		parent::__construct();
		// Load database
		$this->load->model('admin_company_menu_model');
	}

	public function index() {

		$data = array();
		$data['name'] = (!empty($this->input->post('name'))) ? strtolower(trim($this->input->post('name'))) : "";
		$data['language_id'] = (!empty($this->input->post('language_id'))) ? strtolower(trim($this->input->post('language_id'))) : "";
		$data['menu_id'] = (!empty($this->input->post('menu_id'))) ? strtolower(trim($this->input->post('menu_id'))) : "";
		$data['company_id'] = (!empty($this->input->post('company_id'))) ? strtolower(trim($this->input->post('company_id'))) : "";

		$config = array();
	    $config['base_url'] = base_url().'menu';
	    $config['total_rows'] = $this->admin_company_menu_model->countCompanyMenu($data);
	    $config['per_page'] = 20;
	    $config['uri_segment'] = 2;
	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';
	    $config['prev_tag_open'] = '<li>';
	    $config['prev_tag_close'] = '</li>';
	    $config['next_tag_open'] = '<li>';
	    $config['next_tag_close'] = '</li>';
	    $config['first_tag_open'] = '<li>';
	    $config['first_tag_close'] ='</li>';
	    $config['last_tag_open'] = '<li>';
	    $config['last_tag_close'] ='</li>';
	    $config['cur_tag_open'] = '<li class="active"><a>';
	    $config['cur_tag_close'] = '</a></li>';
	    //$config['num_links'] = round($config['total_rows']/$config['per_page']);
	    $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
	    $this->pagination->initialize($config);

	    $data['menuList'] = $this->admin_company_menu_model->getMenuList();
	    $data['companyList'] = $this->admin_company_menu_model->getCompanyList();
	    $data['languageList'] = $this->admin_company_menu_model->getLanugageList();
	    $data['companymenuList'] = $this->admin_company_menu_model->getCompanyMenuList($config['per_page'], $page, $data);
		$data["links"] = $this->pagination->create_links();

		$this->load->template('company_menu/company_menu_list', $data);
	}

	public function add() {
		$data['page_title'] = "Add Company Menu";
		$data['button_title'] = "Add";
		$data['menuList'] = $this->admin_company_menu_model->getMenuList();
	    $data['companyList'] = $this->admin_company_menu_model->getCompanyList();
		$this->load->template('company_menu/company_menu_form', $data);
	}

	public function edit($id) {
		$data['page_title'] = "Edit Company Menu";
		$data['button_title'] = "Edit";
		$data['menuList'] = $this->admin_company_menu_model->getMenuList();
	    $data['companyList'] = $this->admin_company_menu_model->getCompanyList();
		$data['menuData'] = $this->admin_company_menu_model->getMenuById($id);
		$this->load->template('company_menu/company_menu_form', $data);
	}

	public function delete($id) {
		$result = $this->admin_company_menu_model->deleteMenu($id);
		redirect('company-menu');
	}

	public function save_menu_process() {

		//$this->form_validation->set_rules('menu', 'Name', 'trim|required');
		$this->form_validation->set_rules('menu_id', 'Type', 'trim|required');
		$this->form_validation->set_rules('company_id', 'Company', 'trim|required');

		$data = array(
			'name' => strtolower(trim($this->input->post('name'))),
			'menu_id'  => strtolower(trim($this->input->post('menu_id'))),
			'company_id'  => strtolower(trim($this->input->post('company_id'))),
			'link' => strtolower(trim($this->input->post('link'))),
			'order_menu' => strtolower(trim($this->input->post('order_menu'))),
			'id'  => $this->input->post('id'),
		);

		$data['page_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit Menu" : "Add Menu";
		$data['button_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";
		$data['menuList'] = $this->admin_company_menu_model->getMenuList();
	    $data['companyList'] = $this->admin_company_menu_model->getCompanyList();

		$isDuplicate = $this->admin_company_menu_model->checkMenubyName($data['name'], $data['menu_id'], $data['company_id']);

		if ($this->form_validation->run() == FALSE):
			$data['error_message'] = 'Check your input';
			$this->load->template('company_menu/company_menu_form', $data);
		elseif($isDuplicate && $data['button_title'] == "Add"):
			$data['error_message'] = 'Duplicate Menu Name';
			$this->load->template('company_menu/company_menu_form', $data);
		else:

			$menu_data['name'] = $data['name'];
			$menu_data['menu_id'] = $data['menu_id'];
			$menu_data['company_id'] = $data['company_id'];
			$menu_data['link'] = $data['link'];
			$menu_data['order_menu'] = $data['order_menu'];
			$menu_data['id']   = $data['id'];

			$result = $this->admin_company_menu_model->saveMenu($menu_data);

			if($result === TRUE) {
				$display_message = (isset($data['id']) && $data['id'] > 0) ? "Edit Company Menu Successfully" : "Add Company Menu Successfully";
				$page_title = (isset($data['id']) && $data['id'] > 0) ? "Edit Company Menu" : "Add Company Menu";
				$button_title = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";
				$menuData  = (isset($data['id']) && $data['id'] > 0) ? $this->admin_company_menu_model->getMenuById($data['id']) : "";
				$menuList = $this->admin_company_menu_model->getMenuList();
	   			$companyList = $this->admin_company_menu_model->getCompanyList();


				$data = array(
					'message_display' => $display_message,
					'page_title' => $page_title,
					'button_title' => $button_title,
					'menuList' => $menuList,
					'companyList' => $companyList,
					'menuData' => $menuData
				);
				
				$this->load->template('company_menu/company_menu_form', $data);
			} else {
				$data['error_message'] = 'Cannot save your data';
				$this->load->template('company_menu/company_menu_form', $data);
			}

		endif;
	}
}
