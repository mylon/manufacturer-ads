<?php

//defined('BASEPATH') OR exit('No direct script access allowed');

//session_start(); //we need to start session in order to access it through CI

require FCPATH.'/vendor/autoload.php';
use Aws\S3\S3Client;
use Aws\S3\MultipartUploader;
use Aws\Exception\MultipartUploadException;

Class Admincompanynews extends CI_Controller {

	public function __construct() {
		parent::__construct();
		// Load database
		$this->load->model('admin_company_news_model');
	}

	public function index() {

		$data = array();
		$data['title'] = (!empty($this->input->post('title'))) ? strtolower(trim($this->input->post('title'))) : "";
		$data['start_date'] 	= (!empty($this->input->post('start_date'))) ? strtolower(trim($this->input->post('start_date'))) : "";
		$data['end_date'] 	= (!empty($this->input->post('end_date'))) ? strtolower(trim($this->input->post('end_date'))) : "";
		$data['created_by'] 	= (!empty($this->input->post('staff_id'))) ? strtolower(trim($this->input->post('staff_id'))) : "";
		$data['company_id'] 	= (!empty($this->input->post('company_id'))) ? strtolower(trim($this->input->post('company_id'))) : "";
		$data['category_id'] 	= (!empty($this->input->post('category_id'))) ? strtolower(trim($this->input->post('category_id'))) : "";
		$data['language_id'] 	= (!empty($this->input->post('language_id'))) ? strtolower(trim($this->input->post('language_id'))) : "";

		$config = array();
	    $config['base_url'] = base_url().'company-news';
	    $config['total_rows'] = $this->admin_company_news_model->countNews($data);
	    $config['per_page'] = 20;
	    $config['uri_segment'] = 2;
	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';
	    $config['prev_tag_open'] = '<li>';
	    $config['prev_tag_close'] = '</li>';
	    $config['next_tag_open'] = '<li>';
	    $config['next_tag_close'] = '</li>';
	    $config['first_tag_open'] = '<li>';
	    $config['first_tag_close'] ='</li>';
	    $config['last_tag_open'] = '<li>';
	    $config['last_tag_close'] ='</li>';
	    $config['cur_tag_open'] = '<li class="active"><a>';
	    $config['cur_tag_close'] = '</a></li>';
	    //$config['num_links'] = round($config['total_rows']/$config['per_page']);
	    $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
	    $this->pagination->initialize($config);

	    $data['staffList'] = $this->admin_company_news_model->getStaffList();
	    $data['companyList'] = $this->admin_company_news_model->getCompanyList();
	    $data['categoryList'] = $this->admin_company_news_model->getNewsCategoryList();
	    $data['languageList'] = $this->admin_company_news_model->getLanguageList();
	    $data['NewsList'] = $this->admin_company_news_model->getNewsList($config['per_page'], $page, $data);
		$data["links"] = $this->pagination->create_links();

		$this->load->template('company_news/company_news_list', $data);
	}

	public function add() {
		$data['page_title'] = "Add News";
		$data['button_title'] = "Add";
		$data['companyList'] = $this->admin_company_news_model->getCompanyList();
		$data['languageList'] = $this->admin_company_news_model->getLanguageList();
		$data['categoryList'] = $this->admin_company_news_model->getNewsCategoryList();
		$this->load->template('company_news/company_news_form', $data);
	}

	public function edit($id) {
		$data['page_title'] = "Edit News";
		$data['button_title'] = "Edit";
		$data['newsData'] = $this->admin_company_news_model->getNewsById($id);
		$data['companyList'] = $this->admin_company_news_model->getCompanyList();
		$data['languageList'] = $this->admin_company_news_model->getLanguageList();
		$data['categoryList'] = $this->admin_company_news_model->getNewsCategoryList();
		$this->load->template('company_news/company_news_form', $data);
	}


	public function delete($id) {
		$result = $this->admin_company_news_model->deleteNews($id);
		redirect('company-news');
	}

	public function save_news_process() {

		$this->form_validation->set_rules('title', 'News Title', 'trim|required');

		$data = array(
			'title' => trim($this->input->post('title')),
			'description'  => htmlspecialchars(trim($this->input->post('description'))),
			'meta_title' => trim($this->input->post('meta_title')),
			'meta_description'  => htmlspecialchars(trim($this->input->post('meta_description'))),
			'author_name'  => trim($this->input->post('author_name')),
			'author_job'  => trim($this->input->post('author_job')),
			'company_id'  => $this->input->post('company_id'),
			'category_id'  => $this->input->post('category_id'),
			'language_id'  => $this->input->post('language_id'),
			'staff_id'  => trim($this->input->post('staff_id')),
			'id'  => $this->input->post('id'),
		);

		$data['page_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit News" : "Add News";
		$data['button_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";
		$data['companyList'] = $this->admin_company_news_model->getCompanyList();
		$data['languageList'] = $this->admin_company_news_model->getLanguageList();
		$data['categoryList'] = $this->admin_company_news_model->getNewsCategoryList();

		//$isDuplicate = $this->admin_company_model->checkCompanybyName($data['name']);

		if ($this->form_validation->run() == FALSE):
			$data['error_message'] = 'Check your input';
			$this->load->template('company_news/company_news_form', $data);
		else:

			$news_data['title'] = $data['title'];
			$news_data['description'] = $data['description'];
			$news_data['meta_title'] = $data['meta_title'];
			$news_data['meta_description'] = $data['meta_description'];
			$news_data['company_id'] = $data['company_id'];
			$news_data['category_id'] = $data['category_id'];
			$news_data['language_id'] = $data['language_id'];
			$news_data['author_name'] = $data['author_name'];
			$news_data['author_job'] = $data['author_job'];
			$news_data['id']   = $data['id'];

			if (isset($data['id']) && $data['id'] > 0) {
				$news_data['edited_by'] = $data['staff_id'];
			} else {
				$news_data['created_by'] = $data['staff_id'];
			}

			$result = $this->admin_company_news_model->saveNews($news_data);

			if($result === TRUE) {
				$display_message = (isset($data['id']) && $data['id'] > 0) ? "News News Successfully" : "Add News Successfully";
				$page_title = (isset($data['id']) && $data['id'] > 0) ? "Edit News" : "Add News";
				$button_title = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";
				$newsData  = (isset($data['id']) && $data['id'] > 0) ? $this->admin_company_news_model->getNewsById($data['id']) : "";
				$companyList = $this->admin_company_news_model->getCompanyList();
				$languageList = $this->admin_company_news_model->getLanguageList();
				$categoryList = $this->admin_company_news_model->getNewsCategoryList();

				$data = array(
					'message_display' => $display_message,
					'page_title' => $page_title,
					'button_title' => $button_title,
					'companyList' => $companyList,
					'languageList' => $languageList,
					'categoryList' => $categoryList,
					'newsData' => $newsData
				);
				
				$this->load->template('company_news/company_news_form', $data);
			} else {
				$data['error_message'] = 'Cannot save your data';
				$this->load->template('company_news/company_news_form', $data);
			}

		endif;
	}

	public function upload_image($id) {
        $data['id'] = $id;
        $data['files'] = $this->admin_company_news_model->getNewsImage($id);
		$this->load->template('company_news/company_news_images', $data);
    }

    public function description_upload_image($id) {
        $data['id'] = $id;
        $data['files'] = $this->admin_company_news_model->getNewsDescriptionImages($id);
		$this->load->template('company_news/company_news_description_images', $data);
    }

    public function upload_author($id) {
        $data['id'] = $id;
        $data['files'] = $this->admin_company_news_model->getAuthorImage($id);
		$this->load->template('company_news/company_news_author', $data);
    }

   
    public function save_image($id) {

    	$s3Client = new S3Client([
		    'version'     => 'latest',
		    'region'      => 'ap-southeast-1',
		    'credentials' => [
		        'key'    => 'AKIAJ7YGNWKVXXIWLBCA',
		        'secret' => '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'
		    ]
		]);

		
    	if($this->input->post('fileSubmit') && !empty($_FILES['userFiles']['name'])){
  
            $uploader = new MultipartUploader($s3Client, fopen($_FILES['userFiles']['tmp_name'], 'r'), [
			    'bucket' => 'manufacturer-bucket',
			    'key' => 'company-news/news-company-'.$id.'-'.$_FILES['userFiles']['name']
			]);

			try {
			    $result = $uploader->upload();
			    $statusMsg  = "Upload complete";
			    $this->session->set_flashdata('statusMsg',$statusMsg);

			    $this->db->where('id', $id);
			    $data['images'] = 'news-company-'.$id.'-'.$_FILES['userFiles']['name'];
			    $data['edited_by'] = $this->session->userdata['manufacturer_staff_logged_in']['staff_id'];
			    $data['updated_date'] = date('Y-m-d H:i:s');
				$result = $this->db->update('company_news', $data); 
				
			} catch (MultipartUploadException $e) {
			    $statusMsg = $e->getMessage() . "\n";
			    $this->session->set_flashdata('statusMsg',$statusMsg);
			}
    		
    	}

    	redirect('company-news/upload-image/'.$id);
    }

    public function description_save_image($id) {

    	$s3Client = new S3Client([
		    'version'     => 'latest',
		    'region'      => 'ap-southeast-1',
		    'credentials' => [
		        'key'    => 'AKIAJ7YGNWKVXXIWLBCA',
		        'secret' => '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'
		    ]
		]);

		
    	if($this->input->post('fileSubmit') && !empty($_FILES['userFiles']['name'])){
  
            $uploader = new MultipartUploader($s3Client, fopen($_FILES['userFiles']['tmp_name'], 'r'), [
			    'bucket' => 'manufacturer-bucket',
			    'key' => 'news-description/news-description-'.$id.'-'.$_FILES['userFiles']['name']
			]);

			try {
			    $result = $uploader->upload();
			    $statusMsg  = "Upload complete";
			    $this->session->set_flashdata('statusMsg',$statusMsg);

			    $data['image_url'] = 'news-description-'.$id.'-'.$_FILES['userFiles']['name'];
			    $data['news_id'] = $id;
				$result = $this->db->insert('news_description_images', $data); 
				
			} catch (MultipartUploadException $e) {
			    $statusMsg = $e->getMessage() . "\n";
			    $this->session->set_flashdata('statusMsg',$statusMsg);
			}
    		
    	}

    	redirect('company-news/description-upload-image/'.$id);

    }

    public function save_author($id) {

    	$s3Client = new S3Client([
		    'version'     => 'latest',
		    'region'      => 'ap-southeast-1',
		    'credentials' => [
		        'key'    => 'AKIAJ7YGNWKVXXIWLBCA',
		        'secret' => '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'
		    ]
		]);

		
    	if($this->input->post('fileSubmit') && !empty($_FILES['userFiles']['name'])){
  
            $uploader = new MultipartUploader($s3Client, fopen($_FILES['userFiles']['tmp_name'], 'r'), [
			    'bucket' => 'manufacturer-bucket',
			    'key' => 'company-author/author-company-'.$id.'-'.$_FILES['userFiles']['name']
			]);

			try {
			    $result = $uploader->upload();
			    $statusMsg  = "Upload complete";
			    $this->session->set_flashdata('statusMsg',$statusMsg);

			    $this->db->where('id', $id);
			    $data['author_photo'] = 'author-company-'.$id.'-'.$_FILES['userFiles']['name'];
			    $data['edited_by'] = $this->session->userdata['manufacturer_staff_logged_in']['staff_id'];
			    $data['updated_date'] = date('Y-m-d H:i:s');
				$result = $this->db->update('company_news', $data); 
				
			} catch (MultipartUploadException $e) {
			    $statusMsg = $e->getMessage() . "\n";
			    $this->session->set_flashdata('statusMsg',$statusMsg);
			}
    		
    	}

    	redirect('company-news/upload-author/'.$id);

    }

    public function delete_image($id, $image_name) {

    	$s3Client = new S3Client([
		    'version'     => 'latest',
		    'region'      => 'ap-southeast-1',
		    'credentials' => [
		        'key'    => 'AKIAJ7YGNWKVXXIWLBCA',
		        'secret' => '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'
		    ]
		]);

		$result = $s3Client->deleteObject(array(
			'Bucket' => 'manufacturer-bucket',
			'Key' => 'company-news/'.$image_name
		));

		if($result){
			$result_image = $this->admin_company_news_model->deleteNewsImage($id);
			redirect('company-news/upload-image/'.$id);
		} else {
			 $this->session->set_flashdata('statusMsg','Cannot delete your Image');
		}


    }

    public function description_delete_image($id, $image_name, $news_id) {

    	$s3Client = new S3Client([
		    'version'     => 'latest',
		    'region'      => 'ap-southeast-1',
		    'credentials' => [
		        'key'    => 'AKIAJ7YGNWKVXXIWLBCA',
		        'secret' => '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'
		    ]
		]);

		$result = $s3Client->deleteObject(array(
			'Bucket' => 'manufacturer-bucket',
			'Key' => 'news-description/'.$image_name
		));

		if($result){
			$result_image = $this->admin_company_news_model->deleteNewsDescriptionImage($id);
			redirect('company-news/description-upload-image/'.$news_id);
		} else {
			 $this->session->set_flashdata('statusMsg','Cannot delete your Image');
		}


    }

    public function delete_author($id, $image_name) {

    	$s3Client = new S3Client([
		    'version'     => 'latest',
		    'region'      => 'ap-southeast-1',
		    'credentials' => [
		        'key'    => 'AKIAJ7YGNWKVXXIWLBCA',
		        'secret' => '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'
		    ]
		]);

		$result = $s3Client->deleteObject(array(
			'Bucket' => 'manufacturer-bucket',
			'Key' => 'company-author/'.$image_name
		));

		if($result){
			$result_image = $this->admin_company_news_model->deleteAuthorImage($id);
			redirect('company-news/upload-author/'.$id);
		} else {
			 $this->session->set_flashdata('statusMsg','Cannot delete your Image');
		}


    }


}
