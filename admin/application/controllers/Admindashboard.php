<?php

//defined('BASEPATH') OR exit('No direct script access allowed');

//session_start(); //we need to start session in order to access it through CI

Class Admindashboard extends CI_Controller {

	public function __construct() {
		parent::__construct();
		// Load database
		$this->load->model('admin_dashboard_model');
	}

	public function index() {
		$this->load->template('dashboard');
	}

	public function edit() {
		$this->load->template('profileform');
	}

	public function save_profile_process() {

		$password_validate = true;

		$old_db_password = $this->admin_dashboard_model->getStaffPassword($this->input->post('id'));

		if($this->input->post('old_password') != ""):
			$old_password = md5($this->input->post('old_password'));
			if($old_password != $old_db_password):
				$password_validate = false;
			else:
				if($this->input->post('new_password') != "" && $this->input->post('new_password') != $this->input->post('c_new_password')):
					$password_validate = false;
				endif;
			endif;
		endif;


		if ($password_validate == false):
			
			$data['error_message'] = 'Check your password and confirm password if you want to change';
			$this->load->template('profileform',  $data);

		else:

			if($password_validate == true && $this->input->post('new_password') != "" && $this->input->post('c_new_password') ==  $this->input->post('new_password')):
				$data['password'] = md5($this->input->post('new_password'));
			endif;
			$data['id'] = $this->input->post('id');
			$result = $this->admin_dashboard_model->updateStaff($data);

			if($result === TRUE) {
				redirect('dashboard');
			} else {
				$data['error_message'] = 'Cannot save your data';
				$this->load->template('profileform',  $data);
			}

		endif;
	}
}
