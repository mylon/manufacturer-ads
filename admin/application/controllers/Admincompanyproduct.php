<?php

//defined('BASEPATH') OR exit('No direct script access allowed');

//session_start(); //we need to start session in order to access it through CI

require FCPATH.'/vendor/autoload.php';
use Aws\S3\S3Client;
use Aws\S3\MultipartUploader;
use Aws\Exception\MultipartUploadException;

Class Admincompanyproduct extends CI_Controller {

	public function __construct() {
		parent::__construct();
		// Load database
		$this->load->model('admin_company_products_model');
	}

	public function index() {

		$data = array();
		$data['name'] = (!empty($this->input->post('name'))) ? strtolower(trim($this->input->post('name'))) : "";
		$data['start_date'] 	= (!empty($this->input->post('start_date'))) ? strtolower(trim($this->input->post('start_date'))) : "";
		$data['end_date'] 	= (!empty($this->input->post('end_date'))) ? strtolower(trim($this->input->post('end_date'))) : "";
		$data['created_by'] 	= (!empty($this->input->post('staff_id'))) ? strtolower(trim($this->input->post('staff_id'))) : "";
		$data['company_id'] 	= (!empty($this->input->post('company_id'))) ? strtolower(trim($this->input->post('company_id'))) : "";
		$data['category_id'] 	= (!empty($this->input->post('category_id'))) ? strtolower(trim($this->input->post('category_id'))) : "";

		$config = array();
	    $config['base_url'] = base_url().'company-products';
	    $config['total_rows'] = $this->admin_company_products_model->countProducts($data);
	    $config['per_page'] = 20;
	    $config['uri_segment'] = 2;
	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';
	    $config['prev_tag_open'] = '<li>';
	    $config['prev_tag_close'] = '</li>';
	    $config['next_tag_open'] = '<li>';
	    $config['next_tag_close'] = '</li>';
	    $config['first_tag_open'] = '<li>';
	    $config['first_tag_close'] ='</li>';
	    $config['last_tag_open'] = '<li>';
	    $config['last_tag_close'] ='</li>';
	    $config['cur_tag_open'] = '<li class="active"><a>';
	    $config['cur_tag_close'] = '</a></li>';
	    //$config['num_links'] = round($config['total_rows']/$config['per_page']);
	    $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
	    $this->pagination->initialize($config);

	    $data['categoryList'] = $this->admin_company_products_model->getCategoryList();
	    $data['companyList'] = $this->admin_company_products_model->getCompanyList();
	    $data['staffList'] = $this->admin_company_products_model->getStaffList();
	    $data['ProductList'] = $this->admin_company_products_model->getProductList($config['per_page'], $page, $data);
		$data["links"] = $this->pagination->create_links();

		$this->load->template('company_products/company_products_list', $data);
	}

	public function add() {
		$data['page_title'] = "Add Product";
		$data['button_title'] = "Add";
		$data['companyList'] = $this->admin_company_products_model->getCompanyList();
		$data['categoryList'] = $this->admin_company_products_model->category_data($this->session->userdata['manufacturer_staff_logged_in']['staff_company']);
		$this->load->template('company_products/company_products_form', $data);
	}

	public function edit($id) {
		$data['page_title'] = "Edit Product";
		$data['button_title'] = "Edit";
		$data['productData'] = $this->admin_company_products_model->getProductById($id);
		$data['companyList'] = $this->admin_company_products_model->getCompanyList();
		$data['categoryList'] = $this->admin_company_products_model->category_data($data['productData'][0]->company_id);
		$this->load->template('company_products/company_products_form', $data);
	}


	public function delete($id) {
		$result = $this->admin_company_products_model->deleteProduct($id);
		redirect('company-products');
	}

	public function category_list() {
 		$company_id = $this->input->post('company_id');
        echo(json_encode($this->admin_company_products_model->category_data($company_id)));
	}

	public function save_products_process() {

		$this->form_validation->set_rules('name', 'Name', 'trim|required');

		$data = array(
			'name' => trim($this->input->post('name')),
			'company_id'  => $this->input->post('company_id'),
			'category_id'  => $this->input->post('category_id'),
			'staff_id'  => trim($this->input->post('staff_id')),
			'id'  => $this->input->post('id'),
		);

		$data['page_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit Product" : "Add Product";
		$data['button_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";
		$data['companyList'] = $this->admin_company_products_model->getCompanyList();
		$data['categoryList'] = $this->admin_company_products_model->category_data($data['company_id']);

		//$isDuplicate = $this->admin_company_model->checkCompanybyName($data['name']);

		if ($this->form_validation->run() == FALSE):
			$data['error_message'] = 'Check your input';
			$this->load->template('company_products/company_products_form', $data);
		else:

			$product_data['name'] = $data['name'];
			$product_data['company_id'] = $data['company_id'];
			$product_data['category_id'] = $data['category_id'];
			$product_data['id']   = $data['id'];

			if (isset($data['id']) && $data['id'] > 0) {
				$product_data['edited_by'] = $data['staff_id'];
			} else {
				$product_data['created_by'] = $data['staff_id'];
			}

			$result = $this->admin_company_products_model->saveProduct($product_data);

			if($result === TRUE) {
				$display_message = (isset($data['id']) && $data['id'] > 0) ? "Edit Product Successfully" : "Add Product Successfully";
				$page_title = (isset($data['id']) && $data['id'] > 0) ? "Edit Product" : "Add Product";
				$button_title = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";
				$productData  = (isset($data['id']) && $data['id'] > 0) ? $this->admin_company_products_model->getProductById($data['id']) : "";
				$companyList = $this->admin_company_products_model->getCompanyList();
				$categoryList = $this->admin_company_products_model->category_data($data['company_id']);

				$data = array(
					'message_display' => $display_message,
					'page_title' => $page_title,
					'button_title' => $button_title,
					'companyList' => $companyList,
					'categoryList' => $categoryList,
					'productData' => $productData
				);
				
				$this->load->template('company_products/company_products_form', $data);
			} else {
				$data['error_message'] = 'Cannot save your data';
				$this->load->template('company_products/company_products_form', $data);
			}

		endif;
	}

	public function upload_image($id) {
        $data['id'] = $id;
        $data['files'] = $this->admin_company_products_model->getProductImage($id);
		$this->load->template('company_products/company_products_images', $data);
    }

   
    public function save_image($id) {

    	$s3Client = new S3Client([
		    'version'     => 'latest',
		    'region'      => 'ap-southeast-1',
		    'credentials' => [
		        'key'    => 'AKIAJ7YGNWKVXXIWLBCA',
		        'secret' => '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'
		    ]
		]);

		
    	if($this->input->post('fileSubmit') && !empty($_FILES['userFiles']['name'])){
  
            $uploader = new MultipartUploader($s3Client, fopen($_FILES['userFiles']['tmp_name'], 'r'), [
			    'bucket' => 'manufacturer-bucket',
			    'key' => 'company-products/product-company-'.$id.'-'.$_FILES['userFiles']['name']
			]);

			try {
			    $result = $uploader->upload();
			    $statusMsg  = "Upload complete";
			    $this->session->set_flashdata('statusMsg',$statusMsg);

			    $this->db->where('id', $id);
			    $data['images'] = 'product-company-'.$id.'-'.$_FILES['userFiles']['name'];
				$result = $this->db->update('company_products', $data); 
				
			} catch (MultipartUploadException $e) {
			    $statusMsg = $e->getMessage() . "\n";
			    $this->session->set_flashdata('statusMsg',$statusMsg);
			}
    		
    	}

    	redirect('company-products/upload-image/'.$id);

    }

    public function delete_image($id, $image_name) {

    	$s3Client = new S3Client([
		    'version'     => 'latest',
		    'region'      => 'ap-southeast-1',
		    'credentials' => [
		        'key'    => 'AKIAJ7YGNWKVXXIWLBCA',
		        'secret' => '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'
		    ]
		]);

		$result = $s3Client->deleteObject(array(
			'Bucket' => 'manufacturer-bucket',
			'Key' => 'company-products/'.$image_name
		));

		if($result){
			$result_image = $this->admin_company_products_model->deleteProductImage($id);
			redirect('company-products/upload-image/'.$id);
		} else {
			 $this->session->set_flashdata('statusMsg','Cannot delete your Image');
		}


    }

     public function description_upload_image($id) {
        $data['id'] = $id;
        $data['files'] = $this->admin_company_products_model->getProductDescriptionImages($id);
		$this->load->template('company_products/company_products_description_images', $data);
    }

     public function description_save_image($id) {

    	$s3Client = new S3Client([
		    'version'     => 'latest',
		    'region'      => 'ap-southeast-1',
		    'credentials' => [
		        'key'    => 'AKIAJ7YGNWKVXXIWLBCA',
		        'secret' => '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'
		    ]
		]);

		
    	if($this->input->post('fileSubmit') && !empty($_FILES['userFiles']['name'])){
  
            $uploader = new MultipartUploader($s3Client, fopen($_FILES['userFiles']['tmp_name'], 'r'), [
			    'bucket' => 'manufacturer-bucket',
			    'key' => 'products-description/products-description-'.$id.'-'.$_FILES['userFiles']['name']
			]);

			try {
			    $result = $uploader->upload();
			    $statusMsg  = "Upload complete";
			    $this->session->set_flashdata('statusMsg',$statusMsg);

			    $data['image_url'] = 'products-description-'.$id.'-'.$_FILES['userFiles']['name'];
			    $data['product_id'] = $id;
				$result = $this->db->insert('products_description_images', $data); 
				
			} catch (MultipartUploadException $e) {
			    $statusMsg = $e->getMessage() . "\n";
			    $this->session->set_flashdata('statusMsg',$statusMsg);
			}
    		
    	}

    	redirect('company-products/description-upload-image/'.$id);

    }

    public function description_delete_image($id, $image_name, $news_id) {

    	$s3Client = new S3Client([
		    'version'     => 'latest',
		    'region'      => 'ap-southeast-1',
		    'credentials' => [
		        'key'    => 'AKIAJ7YGNWKVXXIWLBCA',
		        'secret' => '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'
		    ]
		]);

		$result = $s3Client->deleteObject(array(
			'Bucket' => 'manufacturer-bucket',
			'Key' => 'products-description/'.$image_name
		));

		if($result){
			$result_image = $this->admin_company_products_model->deleteProductDescriptionImage($id);
			redirect('company-products/description-upload-image/'.$news_id);
		} else {
			 $this->session->set_flashdata('statusMsg','Cannot delete your Image');
		}


    }

}
