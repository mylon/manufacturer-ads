<?php

//defined('BASEPATH') OR exit('No direct script access allowed');

//session_start(); //we need to start session in order to access it through CI

Class Adminsubcompanycategory extends CI_Controller {

	public function __construct() {
		parent::__construct();
		// Load database
		$this->load->model('admin_sub_company_category_model');
	}

	public function index() {

		$data = array();
		$data['name'] = (!empty($this->input->post('name'))) ? strtolower(trim($this->input->post('name'))) : "";
		$data['company_category_id'] = (!empty($this->input->post('company_category_id'))) ? strtolower(trim($this->input->post('company_category_id'))) : "";

		$config = array();
	    $config['base_url'] = base_url().'sub-companay-category';
	    $config['total_rows'] = $this->admin_sub_company_category_model->countSubCompanyCategory($data);
	    $config['per_page'] = 20;
	    $config['uri_segment'] = 2;
	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';
	    $config['prev_tag_open'] = '<li>';
	    $config['prev_tag_close'] = '</li>';
	    $config['next_tag_open'] = '<li>';
	    $config['next_tag_close'] = '</li>';
	    $config['first_tag_open'] = '<li>';
	    $config['first_tag_close'] ='</li>';
	    $config['last_tag_open'] = '<li>';
	    $config['last_tag_close'] ='</li>';
	    $config['cur_tag_open'] = '<li class="active"><a>';
	    $config['cur_tag_close'] = '</a></li>';
	    //$config['num_links'] = round($config['total_rows']/$config['per_page']);
	    $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
	    $this->pagination->initialize($config);
	   
	    $data['maincategoryList'] = $this->admin_sub_company_category_model->getMainCompanyCategoryList();
	    $data['companyCategoryList'] = $this->admin_sub_company_category_model->getSubCompanyCategoryList($config['per_page'], $page, $data);
		$data["links"] = $this->pagination->create_links();

		$this->load->template('company_sub_category/company_sub_category_list', $data);
	}

	public function add() {
		$data['page_title'] = "Add Sub Company Category";
		$data['button_title'] = "Add";
	    $data['maincategoryList'] = $this->admin_sub_company_category_model->getMainCompanyCategoryList();
		$this->load->template('company_sub_category/company_sub_category_form', $data);
	}

	public function edit($id) {
		$data['page_title'] = "Edit Sub Company Category";
		$data['button_title'] = "Edit";
	    $data['maincategoryList'] = $this->admin_sub_company_category_model->getMainCompanyCategoryList();
		$data['companyCategoryData'] = $this->admin_sub_company_category_model->getSubCompanyCategoryById($id);
		$this->load->template('company_sub_category/company_sub_category_form', $data);
	}

	public function delete($id) {
		$result = $this->admin_sub_company_category_model->deleteCompanyCategory($id);
		redirect('sub-company-category');
	}

	public function category_list() {
 		$company_id = $this->input->post('company_id');
        echo(json_encode($this->admin_sub_company_category_model->category_data($company_id)));
	}

	public function save_company_category_process() {

		$this->form_validation->set_rules('name', 'Company Category Name', 'trim|required');

		$data = array(
			'name' => strtolower(trim($this->input->post('name'))),
			'company_category_id'  => $this->input->post('company_category_id'),
			'id'  => $this->input->post('id'),
		);

		$data['page_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit Sub Company Category" : "Add Sub Company Category";
		$data['button_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";
		$data['maincategoryList'] = $this->admin_sub_company_category_model->getMainCompanyCategoryList();

		$isDuplicate = $this->admin_sub_company_category_model->checkCompanyCategorybyName($data['name'], $data['company_category_id']);

		if ($this->form_validation->run() == FALSE):
			$data['error_message'] = 'Check your input';
			$this->load->template('company_sub_category/company_sub_category_form', $data);
		elseif($isDuplicate && $data['button_title'] == "Add"):
			$data['error_message'] = 'Duplicate Company Category Name';
			$this->load->template('company_sub_category/company_sub_category_form', $data);
		else:

			$company_category_data['name'] = $data['name'];
			$company_category_data['company_category_id']   = $data['company_category_id'];
			$company_category_data['id']   = $data['id'];

			$result = $this->admin_sub_company_category_model->saveCompanyCategory($company_category_data);

			if($result === TRUE) {
				$display_message = (isset($data['id']) && $data['id'] > 0) ? "Edit Sub Company Category Successfully" : "Add Sub Company Category Successfully";
				$page_title = (isset($data['id']) && $data['id'] > 0) ? "Edit Sub Company Category" : "Add Sub Company Category";
				$button_title = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";
				$companyCategoryData  = (isset($data['id']) && $data['id'] > 0) ? $this->admin_sub_company_category_model->getSubCompanyCategoryById($data['id']) : "";
				
				$maincategoryList = $this->admin_sub_company_category_model->getMainCompanyCategoryList();

				$data = array(
					'message_display' => $display_message,
					'page_title' => $page_title,
					'button_title' => $button_title,
					'maincategoryList' => $maincategoryList,
					'companyCategoryData' => $companyCategoryData
				);
				
				$this->load->template('company_sub_category/company_sub_category_form', $data);
			} else {
				$data['error_message'] = 'Cannot save your data';
				$this->load->template('company_sub_category/company_sub_category_form', $data);
			}

		endif;
	}
}
