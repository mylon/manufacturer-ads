<?php

//defined('BASEPATH') OR exit('No direct script access allowed');

//session_start(); //we need to start session in order to access it through CI

Class Adminnewscategoryprofile extends CI_Controller {

	public function __construct() {
		parent::__construct();
		// Load database
		$this->load->model('admin_news_category_profile_model');
	}

	public function index() {

		$data = array();
		$data['name'] = (!empty($this->input->post('name'))) ? strtolower(trim($this->input->post('name'))) : "";
		$data['company_id'] = (!empty($this->input->post('company_id'))) ? strtolower(trim($this->input->post('company_id'))) : "";
		$data['news_category_id'] = (!empty($this->input->post('news_category_id'))) ? strtolower(trim($this->input->post('news_category_id'))) : "";
		$data['language_id'] = (!empty($this->input->post('language_id'))) ? strtolower(trim($this->input->post('language_id'))) : "";

		$config = array();
	    $config['base_url'] = base_url().'news-category-profile';
	    $config['total_rows'] = $this->admin_news_category_profile_model->countNewsCategoryProfile($data);
	    $config['per_page'] = 20;
	    $config['uri_segment'] = 2;
	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';
	    $config['prev_tag_open'] = '<li>';
	    $config['prev_tag_close'] = '</li>';
	    $config['next_tag_open'] = '<li>';
	    $config['next_tag_close'] = '</li>';
	    $config['first_tag_open'] = '<li>';
	    $config['first_tag_close'] ='</li>';
	    $config['last_tag_open'] = '<li>';
	    $config['last_tag_close'] ='</li>';
	    $config['cur_tag_open'] = '<li class="active"><a>';
	    $config['cur_tag_close'] = '</a></li>';
	    //$config['num_links'] = round($config['total_rows']/$config['per_page']);
	    $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
	    $this->pagination->initialize($config);

	    $data['companyList'] = $this->admin_news_category_profile_model->getCompanyList();
	    $data['categoryList'] = $this->admin_news_category_profile_model->getCategoryList();
	    $data['languageList'] = $this->admin_news_category_profile_model->getLanguageList();
	    $data['categoryProfileList'] = $this->admin_news_category_profile_model->getNewsCategoryProfileList($config['per_page'], $page, $data);
		$data["links"] = $this->pagination->create_links();

		$this->load->template('news_category_profile/news_category_profile_list', $data);
	}

	public function add() {
		$data['page_title'] = "Add Category Profile";
		$data['button_title'] = "Add";
		$data['categoryList'] = $this->admin_news_category_profile_model->getCategoryList();
	    $data['languageList'] = $this->admin_news_category_profile_model->getLanguageList();
		$this->load->template('news_category_profile/news_category_profile_form', $data);
	}

	public function edit($id) {
		$data['page_title'] = "Edit Category Profile";
		$data['button_title'] = "Edit";
		 $data['categoryList'] = $this->admin_news_category_profile_model->getCategoryList();
	    $data['languageList'] = $this->admin_news_category_profile_model->getLanguageList();
		$data['categoryData'] = $this->admin_news_category_profile_model->getCategoryProfileById($id);
		$this->load->template('news_category_profile/news_category_profile_form', $data);
	}

	public function delete($id) {
		$result = $this->admin_news_category_profile_model->deleteCategory($id);
		redirect('news-category-profile');
	}

	public function category_list() {
 		$company_id = $this->input->post('company_id');
        echo(json_encode($this->admin_news_category_profile_model->category_data($company_id)));
	}

	public function save_news_category_process() {

		$this->form_validation->set_rules('name', 'Category Name', 'trim|required');

		$data = array(
			'name' => strtolower(trim($this->input->post('name'))),
			'description' => htmlspecialchars(strtolower(trim($this->input->post('description')))),
			'news_category_id' => strtolower(trim($this->input->post('news_category_id'))),
			'language_id' => strtolower(trim($this->input->post('language_id'))),
			'id'  => $this->input->post('id'),
		);

		$data['page_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit Category Profile" : "Add Category Profile";
		$data['button_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";
		$data['categoryList'] = $this->admin_news_category_profile_model->getCategoryList();
	    $data['languageList'] = $this->admin_news_category_profile_model->getLanguageList();

		$isDuplicate = $this->admin_news_category_profile_model->checkcCategorybyName($data['name'], $data['news_category_id'] ,$data['language_id']);

		if ($this->form_validation->run() == FALSE):
			$data['error_message'] = 'Check your input';
			$this->load->template('news_category_profile/news_category_profile_form', $data);
		elseif($isDuplicate && $data['button_title'] == "Add"):
			$data['error_message'] = 'Duplicate Category Name';
			$this->load->template('news_category_profile/news_category_profile_form', $data);
		else:

			$category_data['name'] = $data['name'];
			$category_data['description'] = $data['description'];
			$category_data['news_category_id'] = $data['news_category_id'];
			$category_data['language_id'] = $data['language_id'];
			$category_data['id']   = $data['id'];

			$result = $this->admin_news_category_profile_model->saveCategory($category_data);

			if($result === TRUE) {
				$display_message = (isset($data['id']) && $data['id'] > 0) ? "Edit Category Profile Successfully" : "Add Category Profile Successfully";
				$page_title = (isset($data['id']) && $data['id'] > 0) ? "Edit Category Profile" : "Add Category Profile";
				$button_title = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";
				$categoryData  = (isset($data['id']) && $data['id'] > 0) ?  $this->admin_news_category_profile_model->getCategoryProfileById($data['id']) : "";
				$categoryList = $this->admin_news_category_profile_model->getCategoryList();
	   			$languageList = $this->admin_news_category_profile_model->getLanguageList();

				$data = array(
					'message_display' => $display_message,
					'page_title' => $page_title,
					'button_title' => $button_title,
					'categoryList' => $categoryList,
					'languageList' => $languageList,
					'categoryData' => $categoryData
				);
				
				$this->load->template('news_category_profile/news_category_profile_form', $data);
			} else {
				$data['error_message'] = 'Cannot save your data';
				$this->load->template('news_category_profile/news_category_profile_form', $data);
			}

		endif;
	}
}
