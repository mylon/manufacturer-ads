<?php

//defined('BASEPATH') OR exit('No direct script access allowed');

//session_start(); //we need to start session in order to access it through CI

Class Adminuser extends CI_Controller {

	public function __construct() {
		parent::__construct();
		// Load database
		$this->load->model('admin_users_model');
	}

	public function index() {

		$config = array();
	    $config['base_url'] = base_url().'user';
	    $config['total_rows'] = $this->admin_users_model->countUser();
	    $config['per_page'] = 20;
	    $config['uri_segment'] = 2;
	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';
	    $config['prev_tag_open'] = '<li>';
	    $config['prev_tag_close'] = '</li>';
	    $config['next_tag_open'] = '<li>';
	    $config['next_tag_close'] = '</li>';
	    $config['first_tag_open'] = '<li>';
	    $config['first_tag_close'] ='</li>';
	    $config['last_tag_open'] = '<li>';
	    $config['last_tag_close'] ='</li>';
	    $config['cur_tag_open'] = '<li class="active"><a>';
	    $config['cur_tag_close'] = '</a></li>';
	    //$config['num_links'] = round($config['total_rows']/$config['per_page']);
	    $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
	    $this->pagination->initialize($config);

	    $data['userList'] = $this->admin_users_model->getUserList($config['per_page'], $page);
		$data["links"] = $this->pagination->create_links();

		$this->load->template('users/user_list', $data);
	}

	public function add() {
		$data['page_title'] = "Add User";
		$data['button_title'] = "Add";
		$this->load->template('users/user_form', $data);
	}

	public function edit($id) {
		$data['page_title'] = "Edit User";
		$data['button_title'] = "Edit";
		$data['userData'] = $this->admin_users_model->getUserById($id);
		$this->load->template('users/user_form', $data);
	}

	public function delete($id) {
		$result = $this->admin_users_model->deleteUser($id);
		redirect('user');
	}

	public function save_user_process() {

		$this->form_validation->set_rules('email', 'Email', 'trim|required');

		$data = array(
			'company_name' => strtolower(trim($this->input->post('company_name'))),
			'password' => md5(trim($this->input->post('password'))),
			'confirm_password' => md5(trim($this->input->post('confirm_password'))),
			'email' => strtolower(trim($this->input->post('email'))),
			'name' => strtolower(trim($this->input->post('name'))),
			'id'  => $this->input->post('id'),
		);
		
		$data['page_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit User" : "Add User";
		$data['button_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";

		$isDuplicate = $this->admin_users_model->checkUserbyEmail($data['email']);

		if ($this->form_validation->run() == FALSE):
			$data['error_message'] = 'Check your input';
			$this->load->template('users/user_form', $data);
		elseif($isDuplicate && $data['button_title'] == "Add"):
			$data['error_message'] = 'Duplicate User Email';
			$this->load->template('users/user_form', $data);
		elseif(!empty($data['password']) && ($data['confirm_password'] != $data['password'])):
			$data['error_message'] = 'Password and Confirm password not match';
			$this->load->template('users/user_form', $data);
		else:

			if(!empty($data['password'])):
				$user_data['password'] = $data['password'];
			endif;

			$user_data['company_name'] = $data['company_name'];
			$user_data['email'] = $data['email'];
			$user_data['name'] = $data['name'];
			$user_data['password'] = $data['password'];
			$user_data['id']   = $data['id'];

			$result = $this->admin_users_model->saveUser($user_data);

			if($result === TRUE) {
				$display_message = (isset($data['id']) && $data['id'] > 0) ? "Edit User Successfully" : "Add User Successfully";
				$page_title = (isset($data['id']) && $data['id'] > 0) ? "Edit User" : "User Staff";
				$button_title = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";
				$userData  = (isset($data['id']) && $data['id'] > 0) ?  $this->admin_users_model->getUserById($data['id']) : "";

				$data = array(
					'message_display' => $display_message,
					'page_title' => $page_title,
					'button_title' => $button_title,
					'userData' => $userData
				);
				
				$this->load->template('users/user_form', $data);
			} else {
				$data['error_message'] = 'Cannot save your data';
				$this->load->template('users/user_form', $data);
			}

		endif;
	}
}
