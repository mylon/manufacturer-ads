<?php

//defined('BASEPATH') OR exit('No direct script access allowed');

//session_start(); //we need to start session in order to access it through CI

Class Adminindustrialparkprofile extends CI_Controller {

	public function __construct() {
		parent::__construct();
		// Load database
		$this->load->model('admin_industrial_park_profile_model');
	}

	public function index() {

		$data = array();
		$data['name'] = (!empty($this->input->post('name'))) ? strtolower(trim($this->input->post('name'))) : "";
		$data['country_id'] = (!empty($this->input->post('country_id'))) ? strtolower(trim($this->input->post('country_id'))) : "";
		$data['prefecture_id'] = (!empty($this->input->post('prefecture_id'))) ? strtolower(trim($this->input->post('prefecture_id'))) : "";
		$data['language_id'] = (!empty($this->input->post('language_id'))) ? strtolower(trim($this->input->post('language_id'))) : "";
		$data['industrial_park_id'] = (!empty($this->input->post('industrial_park_id'))) ? strtolower(trim($this->input->post('industrial_park_id'))) : "";

		$config = array();
	    $config['base_url'] = base_url().'industrial-park-profile';
	    $config['total_rows'] = $this->admin_industrial_park_profile_model->countIndustialProfilePark($data);
	    $config['per_page'] = 20;
	    $config['uri_segment'] = 2;
	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';
	    $config['prev_tag_open'] = '<li>';
	    $config['prev_tag_close'] = '</li>';
	    $config['next_tag_open'] = '<li>';
	    $config['next_tag_close'] = '</li>';
	    $config['first_tag_open'] = '<li>';
	    $config['first_tag_close'] ='</li>';
	    $config['last_tag_open'] = '<li>';
	    $config['last_tag_close'] ='</li>';
	    $config['cur_tag_open'] = '<li class="active"><a>';
	    $config['cur_tag_close'] = '</a></li>';
	    //$config['num_links'] = round($config['total_rows']/$config['per_page']);
	    $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
	    $this->pagination->initialize($config);

	    $data['countryList'] = $this->admin_industrial_park_profile_model->getCountryList();
		$data['prefectureList'] = $this->admin_industrial_park_profile_model->getPrefectureListByCountryId($data['country_id']);
	    $data['IndustrialParkProfileList'] = $this->admin_industrial_park_profile_model->getIndustrialParkProfileList($config['per_page'], $page, $data);
	    $data['industrialParkList'] = $this->admin_industrial_park_profile_model->getIndustrialParkList();
	    $data['languageList'] = $this->admin_industrial_park_profile_model->getLanguageList();
		$data["links"] = $this->pagination->create_links();

		$this->load->template('industrial_park_profile/industrial_park_profile_list', $data);
	}

	public function add() {
		$data['page_title'] = "Add Industrial Park";
		$data['button_title'] = "Add";
		$data['countryList'] = $this->admin_industrial_park_profile_model->getCountryList();
		$data['industrialParkList'] = $this->admin_industrial_park_profile_model->getIndustrialParkList();
	    $data['languageList'] = $this->admin_industrial_park_profile_model->getLanguageList();
	    $data['prefectureList'] = $this->admin_industrial_park_profile_model->getPrefectureList();
		$this->load->template('industrial_park_profile/industrial_park_profile_form', $data);
	}

	public function edit($id) {
		$data['page_title'] = "Edit Industrial Park";
		$data['button_title'] = "Edit";
		$data['industrialParkData'] = $this->admin_industrial_park_profile_model->getIndustrialParkProfileById($id);
		$data['countryList'] = $this->admin_industrial_park_profile_model->getCountryList();
		$data['prefectureList'] = $this->admin_industrial_park_profile_model->getPrefectureList();
		$data['industrialParkList'] = $this->admin_industrial_park_profile_model->getIndustrialParkList();
	    $data['languageList'] = $this->admin_industrial_park_profile_model->getLanguageList();
		$this->load->template('industrial_park_profile/industrial_park_profile_form', $data);
	}

	public function populate_prefecture()
    {
        $country_id = $this->input->post('country_id');
        echo(json_encode($this->admin_industrial_park_profile_model->getPrefectureListByCountryId($country_id)));
    }


    public function populate_industrial_park()
    {
        $prefecture_id = $this->input->post('prefecture_id');
        echo(json_encode($this->admin_industrial_park_profile_model->getIndustrialParkByPrefectureID($prefecture_id)));
    }

	public function delete($id) {
		$result = $this->admin_industrial_park_profile_model->deleteIndustrialPark($id);
		redirect('industrial-park-profile');
	}

	public function save_industrial_park_process() {

		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('language_id', 'Language', 'trim|required');
		$this->form_validation->set_rules('industrial_park_id', 'Industrial Park', 'trim|required');

		$data = array(
			'name' => strtolower(trim($this->input->post('name'))),
			'language_id'  => strtolower(trim($this->input->post('language_id'))),
			'industrial_park_id'  => strtolower(trim($this->input->post('industrial_park_id'))),
			'id'  => $this->input->post('id'),
		);

		$data['page_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit Industrial Park Profile" : "Add Industrial Park Profile";
		$data['button_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";
		$data['countryList'] = $this->admin_industrial_park_profile_model->getCountryList();
		$data['industrialParkList'] = $this->admin_industrial_park_profile_model->getIndustrialParkList();
	    $data['languageList'] = $this->admin_industrial_park_profile_model->getLanguageList();
	    $data['prefectureList'] = $this->admin_industrial_park_profile_model->getPrefectureList();

		$isDuplicate = $this->admin_industrial_park_profile_model->checkcDuplicateIndustrial($data['name'], $data['language_id'], $data['industrial_park_id']);

		if ($this->form_validation->run() == FALSE):
			$data['error_message'] = 'Check your input';
			$this->load->template('industrial_park_profile/industrial_park_profile_form', $data);
		elseif($isDuplicate && $data['button_title'] == "Add"):
			$data['error_message'] = 'Duplicate Industrial Park Name';
			$this->load->template('industrial_park_profile/industrial_park_profile_form', $data);
		else:

			$industrial_data['name'] = $data['name'];
			$industrial_data['language_id'] = $data['language_id'];
			$industrial_data['industrial_park_id'] = $data['industrial_park_id'];
			$industrial_data['id']   = $data['id'];

			$result = $this->admin_industrial_park_profile_model->saveIndustrialPark($industrial_data);

			if($result === TRUE) {
				$display_message = (isset($data['id']) && $data['id'] > 0) ? "Edit Industrial Park Profile Successfully" : "Add Industrial Park Profile Successfully";
				$page_title = (isset($data['id']) && $data['id'] > 0) ? "Edit Industrial Park Profile" : "Add Industrial Park Profile";
				$button_title = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";
				$industrialParkData  = (isset($data['id']) && $data['id'] > 0) ? $this->admin_industrial_park_profile_model->getIndustrialParkProfileById($data['id']) : "";
				$prefectureList = $this->admin_industrial_park_profile_model->getPrefectureList();
				
				$data = array(
					'message_display' => $display_message,
					'page_title' => $page_title,
					'button_title' => $button_title,
					'industrialParkData' => $industrialParkData,
					'prefectureList' => $prefectureList,
					'countryList' => $this->admin_industrial_park_profile_model->getCountryList(),
					'industrialParkList' => $this->admin_industrial_park_profile_model->getIndustrialParkList(),
					'languageList' => $this->admin_industrial_park_profile_model->getLanguageList()
				);
				
				$this->load->template('industrial_park_profile/industrial_park_profile_form', $data);
			} else {
				$data['error_message'] = 'Cannot save your data';
				$this->load->template('industrial_park_profile/industrial_park_profile_form', $data);
			}

		endif;
	}
}
