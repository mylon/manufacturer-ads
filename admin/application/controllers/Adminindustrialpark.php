<?php

//defined('BASEPATH') OR exit('No direct script access allowed');

//session_start(); //we need to start session in order to access it through CI

Class Adminindustrialpark extends CI_Controller {

	public function __construct() {
		parent::__construct();
		// Load database
		$this->load->model('admin_industrial_park_model');
	}

	public function index() {

		$data = array();
		$data['name'] = (!empty($this->input->post('name'))) ? strtolower(trim($this->input->post('name'))) : "";
		$data['country_id'] = (!empty($this->input->post('country_id'))) ? strtolower(trim($this->input->post('country_id'))) : "";
		$data['prefecture_id'] = (!empty($this->input->post('prefecture_id'))) ? strtolower(trim($this->input->post('prefecture_id'))) : "";

		$config = array();
	    $config['base_url'] = base_url().'industrial-park';
	    $config['total_rows'] = $this->admin_industrial_park_model->countIndustialPark($data);
	    $config['per_page'] = 20;
	    $config['uri_segment'] = 2;
	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';
	    $config['prev_tag_open'] = '<li>';
	    $config['prev_tag_close'] = '</li>';
	    $config['next_tag_open'] = '<li>';
	    $config['next_tag_close'] = '</li>';
	    $config['first_tag_open'] = '<li>';
	    $config['first_tag_close'] ='</li>';
	    $config['last_tag_open'] = '<li>';
	    $config['last_tag_close'] ='</li>';
	    $config['cur_tag_open'] = '<li class="active"><a>';
	    $config['cur_tag_close'] = '</a></li>';
	    //$config['num_links'] = round($config['total_rows']/$config['per_page']);
	    $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
	    $this->pagination->initialize($config);
	  	
	  	$data['countryList'] = $this->admin_industrial_park_model->getCountryList();
	  	$data['prefectureList'] = $this->admin_industrial_park_model->getPrefectureList();
	  	$data['prefectureProfileList'] = $this->admin_industrial_park_model->getPrefectureProfileList();
	    $data['IndustrialParkList'] = $this->admin_industrial_park_model->getIndustrialParkList($config['per_page'], $page, $data);
		$data["links"] = $this->pagination->create_links();

		$this->load->template('industrial_park/industrial_park_list', $data);
	}

	public function add() {
		$data['page_title'] = "Add Industrial Park";
		$data['button_title'] = "Add";
		$data['countryList'] = $this->admin_industrial_park_model->getCountryList();
	  	$data['prefectureList'] = $this->admin_industrial_park_model->getPrefectureList();
		$this->load->template('industrial_park/industrial_park_form', $data);
	}

	public function edit($id) {
		$data['page_title'] = "Edit Industrial Park";
		$data['button_title'] = "Edit";
		$data['countryList'] = $this->admin_industrial_park_model->getCountryList();
	  	$data['prefectureList'] = $this->admin_industrial_park_model->getPrefectureList();
		$data['industrialParkData'] = $this->admin_industrial_park_model->getIndustrialParkById($id);
		$this->load->template('industrial_park/industrial_park_form', $data);
	}


	public function delete($id) {
		$result = $this->admin_industrial_park_model->deleteIndustrialPark($id);
		redirect('industrial-park');
	}

	public function prefecture_list() {
 		$country_id = $this->input->post('country_id');
        echo(json_encode($this->admin_industrial_park_model->prefecture_data($country_id)));
	}

	public function prefecture_profile_list() {
 		$prefecture_id = $this->input->post('prefecture_id');
        echo(json_encode($this->admin_industrial_park_model->prefecture_profile_data($prefecture_id)));
	}

	public function save_industrial_park_process() {

		$this->form_validation->set_rules('name', 'Industrial Park', 'trim|required');

		$data = array(
			'name' => strtolower(trim($this->input->post('name'))),
			'prefecture_id'  => $this->input->post('prefecture_id'),
			'id'  => $this->input->post('id'),
		);

		$data['page_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit Industrial Park" : "Add Industrial Park";
		$data['button_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";
		$data['countryList'] = $this->admin_industrial_park_model->getCountryList();
	  	$data['prefectureList'] = $this->admin_industrial_park_model->getPrefectureList();

		$isDuplicate = $this->admin_industrial_park_model->checkcDuplicateIndustrial($data['name']);

		if ($this->form_validation->run() == FALSE):
			$data['error_message'] = 'Check your input';
			$this->load->template('industrial_park/industrial_park_form', $data);
		elseif($isDuplicate && $data['button_title'] == "Add"):
			$data['error_message'] = 'Duplicate Industrial Park Name';
			$this->load->template('industrial_park/industrial_park_form', $data);
		else:

			$industrial_data['name'] = $data['name'];
			$industrial_data['id']   = $data['id'];
			$industrial_data['prefecture_id']   = $data['prefecture_id'];

			$result = $this->admin_industrial_park_model->saveIndustrialPark($industrial_data);

			if($result === TRUE) {
				$display_message = (isset($data['id']) && $data['id'] > 0) ? "Edit Industrial Park Successfully" : "Add Industrial Park Successfully";
				$page_title = (isset($data['id']) && $data['id'] > 0) ? "Edit Industrial Park" : "Add Industrial Park";
				$button_title = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";
				$industrialParkData  = (isset($data['id']) && $data['id'] > 0) ? $this->admin_industrial_park_model->getIndustrialParkById($data['id']) : "";
				
			
				$data = array(
					'message_display' => $display_message,
					'page_title' => $page_title,
					'button_title' => $button_title,
					'industrialParkData' => $industrialParkData
				);
				$data['countryList'] = $this->admin_industrial_park_model->getCountryList();
			  	$data['prefectureList'] = $this->admin_industrial_park_model->getPrefectureList();
				
				$this->load->template('industrial_park/industrial_park_form', $data);
			} else {
				$data['error_message'] = 'Cannot save your data';
				$this->load->template('industrial_park/industrial_park_form', $data);
			}

		endif;
	}
}
