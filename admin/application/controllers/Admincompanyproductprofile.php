<?php

//defined('BASEPATH') OR exit('No direct script access allowed');

//session_start(); //we need to start session in order to access it through CI

require FCPATH.'/vendor/autoload.php';
use Aws\S3\S3Client;
use Aws\S3\MultipartUploader;
use Aws\Exception\MultipartUploadException;

Class Admincompanyproductprofile extends CI_Controller {

	public function __construct() {
		parent::__construct();
		// Load database
		$this->load->model('admin_company_products_profile_model');
	}

	public function index() {

		$data = array();
		$data['name'] = (!empty($this->input->post('name'))) ? strtolower(trim($this->input->post('name'))) : "";
		$data['start_date'] 	= (!empty($this->input->post('start_date'))) ? strtolower(trim($this->input->post('start_date'))) : "";
		$data['end_date'] 	= (!empty($this->input->post('end_date'))) ? strtolower(trim($this->input->post('end_date'))) : "";
		$data['created_by'] 	= (!empty($this->input->post('staff_id'))) ? strtolower(trim($this->input->post('staff_id'))) : "";
		$data['category_id'] 	= (!empty($this->input->post('category_id'))) ? strtolower(trim($this->input->post('category_id'))) : "";
		$data['company_id'] 	= (!empty($this->input->post('company_id'))) ? strtolower(trim($this->input->post('company_id'))) : "";
		$data['language_id'] 	= (!empty($this->input->post('language_id'))) ? strtolower(trim($this->input->post('language_id'))) : "";
		$data['company_products_id'] 	= (!empty($this->input->post('company_products_id'))) ? strtolower(trim($this->input->post('company_products_id'))) : "";

		$config = array();
	    $config['base_url'] = base_url().'company-products-profile';
	    $config['total_rows'] = $this->admin_company_products_profile_model->countProductsProfile($data);
	    $config['per_page'] = 20;
	    $config['uri_segment'] = 2;
	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';
	    $config['prev_tag_open'] = '<li>';
	    $config['prev_tag_close'] = '</li>';
	    $config['next_tag_open'] = '<li>';
	    $config['next_tag_close'] = '</li>';
	    $config['first_tag_open'] = '<li>';
	    $config['first_tag_close'] ='</li>';
	    $config['last_tag_open'] = '<li>';
	    $config['last_tag_close'] ='</li>';
	    $config['cur_tag_open'] = '<li class="active"><a>';
	    $config['cur_tag_close'] = '</a></li>';
	    //$config['num_links'] = round($config['total_rows']/$config['per_page']);
	    $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
	    $this->pagination->initialize($config);

	    $data['categoryList'] = $this->admin_company_products_profile_model->getCategoryList();
	    $data['productList'] = $this->admin_company_products_profile_model->getProductList();
	    $data['companyList'] = $this->admin_company_products_profile_model->getCompanyList();
	    $data['staffList'] = $this->admin_company_products_profile_model->getStaffList();
	    $data['languageList'] = $this->admin_company_products_profile_model->getLanguageList();
	    $data['productProfileList'] = $this->admin_company_products_profile_model->getProductProfileList($config['per_page'], $page, $data);
		$data["links"] = $this->pagination->create_links();

		$this->load->template('company_products_profile/company_products_profile_list', $data);
	}

	public function add() {
		$data['page_title'] = "Add Product Profile";
		$data['button_title'] = "Add";
		$data['languageList'] = $this->admin_company_products_profile_model->getLanguageList();
		$data['productList'] = $this->admin_company_products_profile_model->getProductList();
		$this->load->template('company_products_profile/company_products_profile_form', $data);
	}

	public function edit($id) {
		$data['page_title'] = "Edit Product Profile";
		$data['button_title'] = "Edit";
		$data['productData'] = $this->admin_company_products_profile_model->getProductProfileById($id);
		$data['languageList'] = $this->admin_company_products_profile_model->getLanguageList();
		$data['productList'] = $this->admin_company_products_profile_model->getProductList();
		$this->load->template('company_products_profile/company_products_profile_form', $data);
	}

	public function category_list() {
 		$company_id = $this->input->post('company_id');
        echo(json_encode($this->admin_company_products_profile_model->category_data($company_id)));
	}


	public function delete($id) {
		$result = $this->admin_company_products_profile_model->deleteProduct($id);
		redirect('company-products-profile');
	}

	public function save_products_process() {

		$this->form_validation->set_rules('name', 'Name', 'trim|required');

		$data = array(
			'name' => htmlspecialchars(trim($this->input->post('name')), ENT_QUOTES),
			'title'  => htmlspecialchars(trim($this->input->post('title')), ENT_QUOTES),
			'description'  => htmlspecialchars(trim($this->input->post('description'))),
			'meta_title'  => htmlspecialchars(trim($this->input->post('meta_title')), ENT_QUOTES),
			'meta_description'  => htmlspecialchars(trim($this->input->post('meta_description'))),
			'company_products_id' => trim($this->input->post('company_products_id')),
			'specs'  => trim($this->input->post('specs')),
			'language_id'  => $this->input->post('language_id'),
			'staff_id'  => trim($this->input->post('staff_id')),
			'id'  => $this->input->post('id'),
		);

	

		$data['page_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit Product Profile" : "Add Product Profile";
		$data['button_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";
		$data['languageList'] = $this->admin_company_products_profile_model->getLanguageList();
		$data['productList'] = $this->admin_company_products_profile_model->getProductList();
		//$isDuplicate = $this->admin_company_model->checkCompanybyName($data['name']);

		if ($this->form_validation->run() == FALSE):
			$data['error_message'] = 'Check your input';
			$this->load->template('company_products_profile/company_products_profile_form', $data);
		else:

			$product_data['name'] = $data['name'];
			$product_data['title'] = $data['title'];
			$product_data['description'] = $data['description'];
			$product_data['meta_title'] = $data['meta_title'];
			$product_data['meta_description'] = $data['meta_description'];
			$product_data['language_id'] = $data['language_id'];
			$product_data['company_products_id'] = $data['company_products_id'];
			$product_data['specs'] = $data['specs'];
			$product_data['id']   = $data['id'];

			if (isset($data['id']) && $data['id'] > 0) {
				$product_data['edited_by'] = $data['staff_id'];
			} else {
				$product_data['created_by'] = $data['staff_id'];
			}

			$result = $this->admin_company_products_profile_model->saveProduct($product_data);

			if($result === TRUE) {
				$display_message = (isset($data['id']) && $data['id'] > 0) ? "Edit Product Profile Successfully" : "Add Product Profile Successfully";
				$page_title = (isset($data['id']) && $data['id'] > 0) ? "Edit Product Profile" : "Add Product Profile";
				$button_title = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";
				$productData  = (isset($data['id']) && $data['id'] > 0) ? $this->admin_company_products_profile_model->getProductProfileById($data['id']) : "";
				$languageList = $this->admin_company_products_profile_model->getLanguageList();
				$productList = $this->admin_company_products_profile_model->getProductList();
			
				$data = array(
					'message_display' => $display_message,
					'page_title' => $page_title,
					'button_title' => $button_title,
					'languageList' => $languageList,
					'productList' => $productList,
					'productData' => $productData
				);
				
				$this->load->template('company_products_profile/company_products_profile_form', $data);
			} else {
				$data['error_message'] = 'Cannot save your data';
				$this->load->template('company_products_profile/company_products_profile_form', $data);
			}

		endif;
	}

	public function upload_pdf($id) {
        $data['id'] = $id;
        $data['files'] = $this->admin_company_products_profile_model->getProductProfilePDF($id);
		$this->load->template('company_products_profile/company_products_profile_pdf', $data);
    }

    public function save_pdf($id) {

    	$s3Client = new S3Client([
		    'version'     => 'latest',
		    'region'      => 'ap-southeast-1',
		    'credentials' => [
		        'key'    => 'AKIAJ7YGNWKVXXIWLBCA',
		        'secret' => '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'
		    ]
		]);

		
    	if($this->input->post('fileSubmit') && !empty($_FILES['userFiles']['name'])){
  
            $uploader = new MultipartUploader($s3Client, fopen($_FILES['userFiles']['tmp_name'], 'r'), [
			    'bucket' => 'manufacturer-bucket',
			    'key' => 'company-products-pdf/product-profile-brochure-company-'.$id.'-'.$_FILES['userFiles']['name']
			]);

			try {
			    $result = $uploader->upload();
			    $statusMsg  = "Upload complete";
			    $this->session->set_flashdata('statusMsg',$statusMsg);
			  
			    $data['file_url'] = 'product-profile-brochure-company-'.$id.'-'.$_FILES['userFiles']['name'];
			    $data['file_name'] = htmlspecialchars(trim($this->input->post('file_name')), ENT_QUOTES);
			    $data['product_profile_id'] = $id;
				$result = $this->db->insert('products_profile_pdf', $data); 
				
			} catch (MultipartUploadException $e) {
			    $statusMsg = $e->getMessage() . "\n";
			    $this->session->set_flashdata('statusMsg',$statusMsg);
			}
    		
    	}

    	redirect('company-products-profile/upload-pdf/'.$id);

    }

     public function delete_pdf($id, $file_name, $product_profile_id) {

    	$s3Client = new S3Client([
		    'version'     => 'latest',
		    'region'      => 'ap-southeast-1',
		    'credentials' => [
		        'key'    => 'AKIAJ7YGNWKVXXIWLBCA',
		        'secret' => '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'
		    ]
		]);

		$result = $s3Client->deleteObject(array(
			'Bucket' => 'manufacturer-bucket',
			'Key' => 'company-products-pdf/'.$file_name
		));

		if($result){
			$result_image = $this->admin_company_products_profile_model->deleteProductProfileBrochure($id);
			redirect('company-products-profile/upload-pdf/'.$product_profile_id);
		} else {
			 $this->session->set_flashdata('statusMsg','Cannot delete your File');
		}


    }


}
