<?php

//defined('BASEPATH') OR exit('No direct script access allowed');

//session_start(); //we need to start session in order to access it through CI

Class Admincountry extends CI_Controller {

	public function __construct() {
		parent::__construct();
		// Load database
		$this->load->model('admin_country_model');
	}

	public function index() {

		$data = array();
		$data['name'] = (!empty($this->input->post('name'))) ? strtolower(trim($this->input->post('name'))) : "";

		$config = array();
	    $config['base_url'] = base_url().'country';
	    $config['total_rows'] = $this->admin_country_model->countCountry($data);
	    $config['per_page'] = 20;
	    $config['uri_segment'] = 2;
	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';
	    $config['prev_tag_open'] = '<li>';
	    $config['prev_tag_close'] = '</li>';
	    $config['next_tag_open'] = '<li>';
	    $config['next_tag_close'] = '</li>';
	    $config['first_tag_open'] = '<li>';
	    $config['first_tag_close'] ='</li>';
	    $config['last_tag_open'] = '<li>';
	    $config['last_tag_close'] ='</li>';
	    $config['cur_tag_open'] = '<li class="active"><a>';
	    $config['cur_tag_close'] = '</a></li>';
	    //$config['num_links'] = round($config['total_rows']/$config['per_page']);
	    $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
	    $this->pagination->initialize($config);

	    $data['CountryList'] = $this->admin_country_model->getCountryList($config['per_page'], $page, $data);
		$data["links"] = $this->pagination->create_links();

		$this->load->template('country/country_list', $data);
	}

	public function add() {
		$data['page_title'] = "Add Country";
		$data['button_title'] = "Add";
		$this->load->template('country/country_form', $data);
	}

	public function edit($id) {
		$data['page_title'] = "Edit Country";
		$data['button_title'] = "Edit";
		$data['countryData'] = $this->admin_country_model->getCountryById($id);
		$this->load->template('country/country_form', $data);
	}

	public function delete($id) {
		$result = $this->admin_country_model->deleteCountry($id);
		redirect('country');
	}

	public function save_country_process() {

		$this->form_validation->set_rules('name', 'Country Name', 'trim|required');

		$data = array(
			'name' => strtolower(trim($this->input->post('name'))),
			'code'  => strtolower(trim($this->input->post('code'))),
			'id'  => $this->input->post('id'),
		);

		$data['page_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit Country" : "Add Country";
		$data['button_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";

		$isDuplicateCountry = $this->admin_country_model->checkcCountrybyName($data['name']);
		$isDuplicateCode = $this->admin_country_model->checkcCountrybyCode($data['code']);

		if ($this->form_validation->run() == FALSE):
			$data['error_message'] = 'Check your input';
			$this->load->template('country/country_form', $data);
		elseif($isDuplicateCountry && $data['button_title'] == "Add"):
			$data['error_message'] = 'Duplicate Country Name';
			$this->load->template('country/country_form', $data);
		elseif($isDuplicateCode && $data['button_title'] == "Add"):
			$data['error_message'] = 'Duplicate Country Code';
			$this->load->template('country/country_form', $data);
		else:

			$country_data['name'] = $data['name'];
			$country_data['code'] = $data['code'];
			$country_data['id']   = $data['id'];

			$result = $this->admin_country_model->saveCountry($country_data);

			if($result === TRUE) {
				$display_message = (isset($data['id']) && $data['id'] > 0) ? "Edit Country Successfully" : "Add Country Successfully";
				$page_title = (isset($data['id']) && $data['id'] > 0) ? "Edit Country" : "Add Country";
				$button_title = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";
				$countryData  = (isset($data['id']) && $data['id'] > 0) ? $this->admin_country_model->getCountryById($data['id']) : "";


				$data = array(
					'message_display' => $display_message,
					'page_title' => $page_title,
					'button_title' => $button_title,
					'countryData' => $countryData
				);
				
				$this->load->template('country/country_form', $data);
			} else {
				$data['error_message'] = 'Cannot save your data';
				$this->load->template('country/country_form', $data);
			}

		endif;
	}
}
