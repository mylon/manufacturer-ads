<?php

//defined('BASEPATH') OR exit('No direct script access allowed');

//session_start(); //we need to start session in order to access it through CI

Class Adminstaff extends CI_Controller {

	public function __construct() {
		parent::__construct();
		// Load database
		$this->load->model('admin_staff_model');
	}

	public function index() {

		$config = array();
	    $config['base_url'] = base_url().'staff';
	    $config['total_rows'] = $this->admin_staff_model->countStaff();
	    $config['per_page'] = 20;
	    $config['uri_segment'] = 2;
	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';
	    $config['prev_tag_open'] = '<li>';
	    $config['prev_tag_close'] = '</li>';
	    $config['next_tag_open'] = '<li>';
	    $config['next_tag_close'] = '</li>';
	    $config['first_tag_open'] = '<li>';
	    $config['first_tag_close'] ='</li>';
	    $config['last_tag_open'] = '<li>';
	    $config['last_tag_close'] ='</li>';
	    $config['cur_tag_open'] = '<li class="active"><a>';
	    $config['cur_tag_close'] = '</a></li>';
	    //$config['num_links'] = round($config['total_rows']/$config['per_page']);
	    $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
	    $this->pagination->initialize($config);

	    $data['StaffList'] = $this->admin_staff_model->getStaffList($config['per_page'], $page);
		$data["links"] = $this->pagination->create_links();

		$this->load->template('staff/staff_list', $data);
	}

	public function add() {
		$data['page_title'] = "Add Staff";
		$data['button_title'] = "Add";
		$data['companyList'] = $this->admin_staff_model->getCompanyList();
		$this->load->template('staff/staff_form', $data);
	}

	public function edit($id) {
		$data['page_title'] = "Edit Staff";
		$data['button_title'] = "Edit";
		$data['staffData'] = $this->admin_staff_model->getStaffById($id);
		$data['companyList'] = $this->admin_staff_model->getCompanyList();
		$this->load->template('staff/staff_form', $data);
	}

	public function delete($id) {
		$result = $this->admin_staff_model->deleteStaff($id);
		redirect('staff');
	}

	public function save_staff_process() {

		$this->form_validation->set_rules('username', 'Username', 'trim|required');

		$data = array(
			'company_id' => strtolower(trim($this->input->post('company_id'))),
			'password' => md5(trim($this->input->post('password'))),
			'username' => strtolower(trim($this->input->post('username'))),
			'id'  => $this->input->post('id'),
		);
		
		$data['page_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit Staff" : "Add Staff";
		$data['button_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";
		$data['companyList'] = $this->admin_staff_model->getCompanyList();

		$isDuplicate = $this->admin_staff_model->checkStaffbyUserName($data['username']);

		if ($this->form_validation->run() == FALSE):
			$data['error_message'] = 'Check your input';
			$this->load->template('staff/staff_form', $data);
		elseif($isDuplicate && $data['button_title'] == "Add"):
			$data['error_message'] = 'Duplicate Staff Userืame';
			$this->load->template('staff/staff_form', $data);
		else:

			$staff_data['company_id'] = $data['company_id'];
			$staff_data['username'] = $data['username'];
			$staff_data['password'] = $data['password'];
			$staff_data['id']   = $data['id'];

			$result = $this->admin_staff_model->saveStaff($staff_data);

			if($result === TRUE) {
				$display_message = (isset($data['id']) && $data['id'] > 0) ? "Edit Staff Successfully" : "Add Staff Successfully";
				$page_title = (isset($data['id']) && $data['id'] > 0) ? "Edit Staff" : "Add Staff";
				$button_title = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";
				$staffData  = (isset($data['id']) && $data['id'] > 0) ?  $this->admin_staff_model->getStaffById($data['id']) : "";
				$companyList = $this->admin_staff_model->getCompanyList();

				$data = array(
					'message_display' => $display_message,
					'companyList' => $companyList,
					'page_title' => $page_title,
					'button_title' => $button_title,
					'staffData' => $staffData
				);
				
				$this->load->template('staff/staff_form', $data);
			} else {
				$data['error_message'] = 'Cannot save your data';
				$this->load->template('staff/staff_form', $data);
			}

		endif;
	}
}
