<?php

//defined('BASEPATH') OR exit('No direct script access allowed');

//session_start(); //we need to start session in order to access it through CI

Class Admingenre extends CI_Controller {

	public function __construct() {
		parent::__construct();
		// Load database
		$this->load->model('admin_genre_model');
	}

	public function index() {

		$data = array();
		$data['name'] = (!empty($this->input->post('name'))) ? strtolower(trim($this->input->post('name'))) : "";

		$config = array();
	    $config['base_url'] = base_url().'genre';
	    $config['total_rows'] = $this->admin_genre_model->countGenre($data);
	    $config['per_page'] = 20;
	    $config['uri_segment'] = 2;
	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';
	    $config['prev_tag_open'] = '<li>';
	    $config['prev_tag_close'] = '</li>';
	    $config['next_tag_open'] = '<li>';
	    $config['next_tag_close'] = '</li>';
	    $config['first_tag_open'] = '<li>';
	    $config['first_tag_close'] ='</li>';
	    $config['last_tag_open'] = '<li>';
	    $config['last_tag_close'] ='</li>';
	    $config['cur_tag_open'] = '<li class="active"><a>';
	    $config['cur_tag_close'] = '</a></li>';
	    //$config['num_links'] = round($config['total_rows']/$config['per_page']);
	    $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
	    $this->pagination->initialize($config);

	    $data['GenreList'] = $this->admin_genre_model->getGenreList($config['per_page'], $page, $data);
		$data["links"] = $this->pagination->create_links();

		$this->load->template('genre/genre_list', $data);
	}

	public function add() {
		$data['page_title'] = "Add Genre";
		$data['button_title'] = "Add";
		$this->load->template('genre/genre_form', $data);
	}

	public function edit($id) {
		$data['page_title'] = "Edit Genre";
		$data['button_title'] = "Edit";
		$data['genreData'] = $this->admin_genre_model->getGenreById($id);
		$this->load->template('genre/genre_form', $data);
	}

	public function delete($id) {
		$result = $this->admin_genre_model->deleteGenre($id);
		redirect('genre');
	}

	public function save_genre_process() {

		$this->form_validation->set_rules('name', 'Genre Name', 'trim|required');

		$data = array(
			'name' => strtolower(trim($this->input->post('name'))),
			'id'  => $this->input->post('id'),
		);

		$data['page_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit Genre" : "Add Genre";
		$data['button_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";

		$isDuplicate = $this->admin_genre_model->checkcGenrebyName($data['name']);

		if ($this->form_validation->run() == FALSE):
			$data['error_message'] = 'Check your input';
			$this->load->template('genre/genre_form', $data);
		elseif($isDuplicate && $data['button_title'] == "Add"):
			$data['error_message'] = 'Duplicate Genre Name';
			$this->load->template('genre/genre_form', $data);
		else:

			$genre_data['name'] = $data['name'];
			$genre_data['id']   = $data['id'];

			$result = $this->admin_genre_model->saveGenre($genre_data);

			if($result === TRUE) {
				$display_message = (isset($data['id']) && $data['id'] > 0) ? "Edit Genre Successfully" : "Add Genre Successfully";
				$page_title = (isset($data['id']) && $data['id'] > 0) ? "Edit Genre" : "Add Genre";
				$button_title = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";
				$genreData  = (isset($data['id']) && $data['id'] > 0) ?  $this->admin_genre_model->getGenreById($data['id']) : "";


				$data = array(
					'message_display' => $display_message,
					'page_title' => $page_title,
					'button_title' => $button_title,
					'genreData' => $genreData
				);
				
				$this->load->template('genre/genre_form', $data);
			} else {
				$data['error_message'] = 'Cannot save your data';
				$this->load->template('genre/genre_form', $data);
			}

		endif;
	}
}
