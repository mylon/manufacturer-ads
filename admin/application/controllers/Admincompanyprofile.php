<?php

//defined('BASEPATH') OR exit('No direct script access allowed');

//session_start(); //we need to start session in order to access it through CI

require FCPATH.'/vendor/autoload.php';
use Aws\S3\S3Client;
use Aws\S3\MultipartUploader;
use Aws\Exception\MultipartUploadException;

Class Admincompanyprofile extends CI_Controller {

	public function __construct() {
		parent::__construct();
		// Load database
		$this->load->model('admin_company_profile_model');
	}

	public function index() {

		$data = array();
		$data['name'] 				= (!empty($this->input->post('name'))) ? strtolower(trim($this->input->post('name'))) : "";
		$data['created_by'] 		= (!empty($this->input->post('staff_id'))) ? strtolower(trim($this->input->post('staff_id'))) : "";
		$data['category_id'] 		= (!empty($this->input->post('category_id'))) ? strtolower(trim($this->input->post('category_id'))) : "";
		$data['sub_category_id'] 	= (!empty($this->input->post('sub_category_id'))) ? strtolower(trim($this->input->post('sub_category_id'))) : "";
		$data['country_id'] 		= (!empty($this->input->post('country_id'))) ? strtolower(trim($this->input->post('country_id'))) : "";
		$data['prefecture_id'] 		= (!empty($this->input->post('prefecture_id'))) ? strtolower(trim($this->input->post('prefecture_id'))) : "";
		$data['industrial_park_id'] = (!empty($this->input->post('industrial_park_id'))) ? strtolower(trim($this->input->post('industrial_park_id'))) : "";
		$data['company_id'] 		= (!empty($this->input->post('company_id'))) ? strtolower(trim($this->input->post('company_id'))) : "";
		$data['language_id'] 		= (!empty($this->input->post('language_id'))) ? strtolower(trim($this->input->post('language_id'))) : "";

		$config = array();
	    $config['base_url'] = base_url().'company-profile';
	    $config['total_rows'] = $this->admin_company_profile_model->countCompanyProfile($data);
	    $config['per_page'] = 20;
	    $config['uri_segment'] = 2;
	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';
	    $config['prev_tag_open'] = '<li>';
	    $config['prev_tag_close'] = '</li>';
	    $config['next_tag_open'] = '<li>';
	    $config['next_tag_close'] = '</li>';
	    $config['first_tag_open'] = '<li>';
	    $config['first_tag_close'] ='</li>';
	    $config['last_tag_open'] = '<li>';
	    $config['last_tag_close'] ='</li>';
	    $config['cur_tag_open'] = '<li class="active"><a>';
	    $config['cur_tag_close'] = '</a></li>';
	    //$config['num_links'] = round($config['total_rows']/$config['per_page']);
	    $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
	    $this->pagination->initialize($config);

	    $data['CompanyProfileList'] = $this->admin_company_profile_model->getCompanyProfileList($config['per_page'], $page, $data);
	    $data['CompanyList'] = $this->admin_company_profile_model->getCompanyList();
	    $data['languageList'] = $this->admin_company_profile_model->getLanugageList();
	    $data['staffList'] = $this->admin_company_profile_model->getStaffList();
	    $data['companyCategory'] = $this->admin_company_profile_model->getCompanyCategoryList();
	    $data['country'] = $this->admin_company_profile_model->getCountryList();
	    $data['prefectureList'] = $this->admin_company_profile_model->getPrefectureList();
	    $data['subcompanyCategory'] = $this->admin_company_profile_model->getSubCompanyCategoryList();
		$data['industrialParkList'] = $this->admin_company_profile_model->getIndustrialParkList();
		$data["links"] = $this->pagination->create_links();

		$this->load->template('company_profile/company_profile_list', $data);
	}

	public function add() {
		$data['page_title'] = "Add Company Profile";
		$data['button_title'] = "Add";
		$data['companyCategory'] = $this->admin_company_profile_model->getCompanyCategoryList();
		$data['subcompanyCategory'] = $this->admin_company_profile_model->getSubCompanyCategoryList();
		$data['CompanyList'] = $this->admin_company_profile_model->getCompanyList();
	    $data['languageList'] = $this->admin_company_profile_model->getLanugageList();
	    $data['prefectureList'] = $this->admin_company_profile_model->getPrefectureList();
		$data['country'] = $this->admin_company_profile_model->getCountryList();
		$this->load->template('company_profile/company_profile_form', $data);
	}

	public function edit($id) {
		$data['page_title'] = "Edit Company Profile";
		$data['button_title'] = "Edit";
		$data['companyData'] = $this->admin_company_profile_model->getCompanyProfileById($id);
		$data['CompanyList'] = $this->admin_company_profile_model->getCompanyList();
	    $data['languageList'] = $this->admin_company_profile_model->getLanugageList();
		$data['country'] = $this->admin_company_profile_model->getCountryList();
		$data['companyCategory'] = $this->admin_company_profile_model->getCompanyCategoryList();
		$data['subcompanyCategory'] = $this->admin_company_profile_model->getSubCompanyCategoryList();
		$data['prefectureList'] = $this->admin_company_profile_model->prefecture_data($data['companyData'][0]->country_id);
		$data['industrialParkList'] = $this->admin_company_profile_model->industrial_park_data($data['companyData'][0]->country_id, $data['companyData'][0]->prefecture_id );
		$data['subCategoryList'] = $this->admin_company_profile_model->getSubcategorylist($id);
		$this->load->template('company_profile/company_profile_form', $data);
	}

	public function category_list() {
 		$company_id = $this->input->post('company_id');
        echo(json_encode($this->admin_company_profile_model->category_data($company_id)));
	}

	public function sub_category_list() {
 		$company_category_id = $this->input->post('company_category_id');
        echo(json_encode($this->admin_company_profile_model->sub_category_data($company_category_id)));
	}

	public function prefecture_list() {
 		$country_id = $this->input->post('country_id');
        echo(json_encode($this->admin_company_profile_model->prefecture_data($country_id)));
	}

	public function industrial_park_list(){
		$prefecture_id = $this->input->post('prefecture_id');
        echo(json_encode($this->admin_company_profile_model->industrial_park_data($prefecture_id)));
	}

	public function delete($id) {
		$result = $this->admin_company_profile_model->deleteCompany($id);
		redirect('company-profile');
	}

	public function save_company_process() {

		$this->form_validation->set_rules('name', 'Name', 'trim|required');

		$data = array(
			'name' => trim($this->input->post('name')),
			'title' => trim($this->input->post('title')),
			'product_line' => trim($this->input->post('product_line')),
			'subject' => trim($this->input->post('subject')),
			'description'  => htmlspecialchars(trim($this->input->post('description'))),
			'ceo'  => trim($this->input->post('ceo')),
			'category_id'  => $this->input->post('category_id'),
			'sub_category_id'  => $this->input->post('sub_category_id'),
			'company_id'  => $this->input->post('company_id'),
			'language_id'  => $this->input->post('language_id'),
			'address'  => trim($this->input->post('address')),
			'country_id'  => $this->input->post('country_id'),
			'prefecture_id'  => $this->input->post('prefecture_id'),
			'industrial_park_id'  => $this->input->post('industrial_park_id'),
			'tel'  => trim($this->input->post('tel')),
			'tel1'  => trim($this->input->post('tel1')),
			'tel2'  => trim($this->input->post('tel2')),
			'tel3'  => trim($this->input->post('tel3')),
			'tel4'  => trim($this->input->post('tel4')),
			'web_url'  => strtolower(trim($this->input->post('web_url'))),
			'founded_date'  => trim($this->input->post('founded_date')),
			'email'  => trim($this->input->post('email')),
			'capital'  => trim($this->input->post('capital')),
			'head_company'  => trim($this->input->post('head_company')),
			'child_company'  => trim($this->input->post('child_company')),
			'shareholder'  => trim($this->input->post('shareholder')),
			'payroll_no'  => trim($this->input->post('payroll_no')),
			'accounting_period'  => trim($this->input->post('accounting_period')),
			'line_bank'  => trim($this->input->post('line_bank')),
			'certification'  => trim($this->input->post('certification')),
			'main_client'  => trim($this->input->post('main_client')),
			'product_meta_title'  => trim($this->input->post('product_meta_title')),
			'product_meta_description'  => htmlspecialchars(trim($this->input->post('product_meta_description'))),
			'news_meta_title'  => trim($this->input->post('news_meta_title')),
			'news_meta_description'  => htmlspecialchars(trim($this->input->post('news_meta_description'))),
			'staff_id'  => trim($this->input->post('staff_id')),
			'id'  => $this->input->post('id'),
		);

		$data['page_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit Company Profile" : "Add Company Profile";
		$data['button_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";
		$data['CompanyList'] = $this->admin_company_profile_model->getCompanyList();
	    $data['languageList'] = $this->admin_company_profile_model->getLanugageList();
	    $data['subcompanyCategory'] = $this->admin_company_profile_model->getSubCompanyCategoryList();
	    $data['subCategoryList'] = $this->admin_company_profile_model->getSubcategorylist($data['id']);

		//$isDuplicate = $this->admin_company_profile_model->checkCompanyProfilebyName($data['name']);

		if ($this->form_validation->run() == FALSE):
			$data['error_message'] = 'Check your input';
			$this->load->template('company_profile/company_profile_form', $data);
		//elseif($isDuplicate && $data['button_title'] == "Add"):
			//$data['error_message'] = 'Duplicate Company Name';
			//$this->load->template('company_profile/company_profile_form', $data);
		else:

			$company_data['name'] = $data['name'];
			$company_data['title'] = $data['title'];
			$company_data['product_line'] = $data['product_line'];
			$company_data['subject'] = $data['subject'];
			$company_data['ceo'] = $data['ceo'];
			$company_data['description'] = $data['description'];
			$company_data['category_id'] = $data['category_id'];
			$company_data['company_id'] = $data['company_id'];
			$company_data['language_id'] = $data['language_id'];
			$company_data['address'] = $data['address'];
			$company_data['country_id'] = $data['country_id'];
			$company_data['prefecture_id'] = $data['prefecture_id'];
			$company_data['industrial_park_id'] = $data['industrial_park_id'];
			$company_data['tel'] = $data['tel'];
			$company_data['tel1'] = $data['tel1'];
			$company_data['tel2'] = $data['tel2'];
			$company_data['tel3'] = $data['tel3'];
			$company_data['tel4'] = $data['tel4'];
			$company_data['web_url'] = $data['web_url'];
			$company_data['founded_date'] = $data['founded_date'];
			$company_data['email'] = $data['email'];
			$company_data['capital'] = $data['capital'];
			$company_data['head_company'] = $data['head_company'];
			$company_data['child_company'] = $data['child_company'];
			$company_data['shareholder'] = $data['shareholder'];
			$company_data['payroll_no'] = $data['payroll_no'];
			$company_data['accounting_period'] = $data['accounting_period'];
			$company_data['line_bank'] = $data['line_bank'];
			$company_data['certification'] = $data['certification'];
			$company_data['product_meta_title'] = $data['product_meta_title'];
			$company_data['product_meta_description'] = $data['product_meta_description'];
			$company_data['news_meta_title'] = $data['news_meta_title'];
			$company_data['news_meta_description'] = $data['news_meta_description'];
			$company_data['main_client'] = $data['main_client'];
			$company_data['id']   = $data['id'];

			if (isset($data['id']) && $data['id'] > 0) {
				$company_data['edited_by'] = $data['staff_id'];
			} else {
				$company_data['created_by'] = $data['staff_id'];
			}

			$result = $this->admin_company_profile_model->saveCompany($company_data, $data['sub_category_id']);

			if($result === TRUE) {
				$display_message = (isset($data['id']) && $data['id'] > 0) ? "Edit Company Profile Successfully" : "Add Company Profile Successfully";
				$page_title = (isset($data['id']) && $data['id'] > 0) ? "Edit Company Profile" : "Add Company Profile";
				$button_title = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";
				$companyData  = (isset($data['id']) && $data['id'] > 0) ? $this->admin_company_profile_model->getCompanyProfileById($data['id']) : "";
				$country = $this->admin_company_profile_model->getCountryList();
				$prefectureList = $this->admin_company_profile_model->prefecture_data($data['country_id'] );
				$industrialParkList = $this->admin_company_profile_model->industrial_park_data($data['prefecture_id']);
				$companyCategory = $this->admin_company_profile_model->getCompanyCategoryList();
				$CompanyList = $this->admin_company_profile_model->getCompanyList();
	    		$languageList = $this->admin_company_profile_model->getLanugageList();
	    		$subcompanyCategory = $this->admin_company_profile_model->getSubCompanyCategoryList();
	    		$subCategoryList = $this->admin_company_profile_model->getSubcategorylist($data['id']);

				$data = array(
					'message_display' => $display_message,
					'page_title' => $page_title,
					'button_title' => $button_title,
					'country' => $country,
					'prefectureList' => $prefectureList,
					'industrialParkList' => $industrialParkList,
					'companyCategory' => $companyCategory,
					'CompanyList' => $CompanyList,
					'languageList' => $languageList,
					'subcompanyCategory' => $subcompanyCategory,
					'subCategoryList' => $subCategoryList,
					'companyData' => $companyData
				);
				
				$this->load->template('company_profile/company_profile_form', $data);
			} else {
				$data['error_message'] = 'Cannot save your data';
				$this->load->template('company_profile/company_profile_form', $data);
			}

		endif;
	}

}
