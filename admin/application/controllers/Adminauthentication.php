<?php

//defined('BASEPATH') OR exit('No direct script access allowed');

//session_start(); //we need to start session in order to access it through CI
Class Adminauthentication extends CI_Controller {

	public function __construct() {
		parent::__construct();
		// Load database
		$this->load->model('admin_login');
	}

	// Show login page
	public function index() {
		if(isset($this->session->userdata['manufacturer_staff_logged_in'])):
			redirect('company');
		else:
			$this->load->template('login_form');
		endif;
	}

	// Check for user login process
	public function admin_login_process() {

		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if ($this->form_validation->run() == FALSE):

			if(isset($this->session->userdata['manufacturer_staff_logged_in'])):
				//$this->load->template('admin_page');
				redirect('company');
			else:
				$this->load->template('login_form');
			endif;

		else:
			$data = array(
				'username' => $this->input->post('username'),
				'password' => $this->input->post('password')
			);

			$result = $this->admin_login->login($data);

			if ($result == TRUE) :

				$username = $this->input->post('username');
				$result = $this->admin_login->read_user_information($username);

				if ($result != false) :
					$session_data = array(
						'staff_username' => $result[0]->username,
						'staff_company' => $result[0]->company_id,
						'staff_type' => $result[0]->type,
						'staff_id' => $result[0]->id
					);
					// Add user data in session
					$this->session->set_userdata('manufacturer_staff_logged_in', $session_data);
					//$this->load->template('admin_page');
					redirect('company');
				endif;
			else:
				$data = array('error_message' => 'Invalid Username or Password');
				$this->load->template('login_form', $data);
			endif;
		endif;
	}

	// Logout from admin page
	public function logout() {
		// Removing session data
		$sess_array = array('staff_username' => '');
		$this->session->unset_userdata('manufacturer_staff_logged_in', $sess_array);
		$data['message_display'] = 'Successfully Logout';
		$this->load->template('login_form', $data);
	}
}
