<?php

//defined('BASEPATH') OR exit('No direct script access allowed');

//session_start(); //we need to start session in order to access it through CI

require FCPATH.'/vendor/autoload.php';
use Aws\S3\S3Client;
use Aws\S3\MultipartUploader;
use Aws\Exception\MultipartUploadException;

Class Admincompanyarticle extends CI_Controller {

	public function __construct() {
		parent::__construct();
		// Load database
		$this->load->model('admin_company_article_model');
	}

	public function index() {

		$config = array();
	    $config['base_url'] = base_url().'company-article';
	    $config['total_rows'] = $this->admin_company_article_model->countArticle();
	    $config['per_page'] = 20;
	    $config['uri_segment'] = 2;
	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';
	    $config['prev_tag_open'] = '<li>';
	    $config['prev_tag_close'] = '</li>';
	    $config['next_tag_open'] = '<li>';
	    $config['next_tag_close'] = '</li>';
	    $config['first_tag_open'] = '<li>';
	    $config['first_tag_close'] ='</li>';
	    $config['last_tag_open'] = '<li>';
	    $config['last_tag_close'] ='</li>';
	    $config['cur_tag_open'] = '<li class="active"><a>';
	    $config['cur_tag_close'] = '</a></li>';
	    //$config['num_links'] = round($config['total_rows']/$config['per_page']);
	    $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
	    $this->pagination->initialize($config);

	    $data['ArticleList'] = $this->admin_company_article_model->getArticleList($config['per_page'], $page);
		$data["links"] = $this->pagination->create_links();

		$this->load->template('company_article/company_article_list', $data);
	}

	public function add() {
		$data['page_title'] = "Add Article";
		$data['button_title'] = "Add";
		$data['articleCategory'] = $this->admin_company_article_model->getArticleCategoryList();
		$data['genreList'] = $this->admin_company_article_model->getGenreList();
		$this->load->template('company/company_form', $data);
	}

	public function edit($id) {
		$data['page_title'] = "Edit Country";
		$data['button_title'] = "Edit";
		$data['articleData'] = $this->admin_company_article_model->getArticleById($id);
		$data['country'] = $this->admin_company_article_model->getCountryList();
		$data['prefectureList'] = $this->admin_company_article_model->prefecture_data($data['companyData'][0]->country_id );
		$data['industrialParkList'] = $this->admin_company_article_model->industrial_park_data($data['companyData'][0]->country_id, $data['companyData'][0]->prefecture_id );
		$this->load->template('company/company_form', $data);
	}

	public function prefecture_list() {
 		$country_id = $this->input->post('country_id');
        echo(json_encode($this->admin_company_model->prefecture_data($country_id)));
	}

	public function industrial_park_list(){
		$country_id = $this->input->post('country_id');
		$prefecture_id = $this->input->post('prefecture_id');
        echo(json_encode($this->admin_company_model->industrial_park_data($country_id, $prefecture_id)));
	}

	public function delete($id) {
		$result = $this->admin_company_model->deleteCompany($id);
		redirect('company');
	}

	public function save_company_process() {

		$this->form_validation->set_rules('name', 'Name', 'trim|required');

		$data = array(
			'name' => trim($this->input->post('name')),
			'description'  => trim($this->input->post('description')),
			'ceo'  => trim($this->input->post('ceo')),
			'category_id'  => $this->input->post('category_id'),
			'address'  => trim($this->input->post('address')),
			'country_id'  => $this->input->post('country_id'),
			'prefecture_id'  => $this->input->post('prefecture_id'),
			'industrial_park_id'  => $this->input->post('industrial_park_id'),
			'tel'  => trim($this->input->post('tel')),
			'web_url'  => strtolower(trim($this->input->post('web_url'))),
			'founded_date'  => trim($this->input->post('founded_date')),
			'email'  => trim($this->input->post('email')),
			'capital'  => trim($this->input->post('capital')),
			'head_company'  => trim($this->input->post('head_company')),
			'child_company'  => trim($this->input->post('child_company')),
			'shareholder'  => trim($this->input->post('shareholder')),
			'payroll_no'  => trim($this->input->post('payroll_no')),
			'accounting_period'  => trim($this->input->post('accounting_period')),
			'line_bank'  => trim($this->input->post('line_bank')),
			'certification'  => trim($this->input->post('certification')),
			'main_client'  => trim($this->input->post('main_client')),
			'staff_id'  => trim($this->input->post('staff_id')),
			'id'  => $this->input->post('id'),
		);

		$data['page_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit Company" : "Add Company";
		$data['button_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";

		$isDuplicate = $this->admin_company_model->checkCompanybyName($data['name']);

		if ($this->form_validation->run() == FALSE):
			$data['error_message'] = 'Check your input';
			$this->load->template('company/company_form', $data);
		elseif($isDuplicate && $data['button_title'] == "Add"):
			$data['error_message'] = 'Duplicate Company Name';
			$this->load->template('company/company_form', $data);
		else:

			$company_data['name'] = $data['name'];
			$company_data['ceo'] = $data['ceo'];
			$company_data['description'] = $data['description'];
			$company_data['category_id'] = $data['category_id'];
			$company_data['address'] = $data['address'];
			$company_data['country_id'] = $data['country_id'];
			$company_data['prefecture_id'] = $data['prefecture_id'];
			$company_data['industrial_park_id'] = $data['industrial_park_id'];
			$company_data['tel'] = $data['tel'];
			$company_data['web_url'] = $data['web_url'];
			$company_data['founded_date'] = $data['founded_date'];
			$company_data['email'] = $data['email'];
			$company_data['capital'] = $data['capital'];
			$company_data['head_company'] = $data['head_company'];
			$company_data['child_company'] = $data['child_company'];
			$company_data['shareholder'] = $data['shareholder'];
			$company_data['payroll_no'] = $data['payroll_no'];
			$company_data['accounting_period'] = $data['accounting_period'];
			$company_data['line_bank'] = $data['line_bank'];
			$company_data['certification'] = $data['certification'];
			$company_data['main_client'] = $data['main_client'];
			$company_data['id']   = $data['id'];

			if (isset($data['id']) && $data['id'] > 0) {
				$company_data['edited_by'] = $data['staff_id'];
			} else {
				$company_data['created_by'] = $data['staff_id'];
			}

			$result = $this->admin_company_model->saveCompany($company_data);

			if($result === TRUE) {
				$display_message = (isset($data['id']) && $data['id'] > 0) ? "Edit Company Successfully" : "Add Company Successfully";
				$page_title = (isset($data['id']) && $data['id'] > 0) ? "Edit Company" : "Add Company";
				$button_title = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";
				$companyData  = (isset($data['id']) && $data['id'] > 0) ? $this->admin_company_model->getCompanyById($data['id']) : "";


				$data = array(
					'message_display' => $display_message,
					'page_title' => $page_title,
					'button_title' => $button_title,
					'companyData' => $companyData
				);
				
				$this->load->template('company/company_form', $data);
			} else {
				$data['error_message'] = 'Cannot save your data';
				$this->load->template('company/company_form', $data);
			}

		endif;
	}

	public function upload_logo($id) {
        $data['id'] = $id;
        $data['files'] = $this->admin_company_model->getLogoImage($id);
		$this->load->template('company/company_logo', $data);
    }

    public function upload_images($id) {
        $data['id'] = $id;
        $data['files'] = $this->admin_company_model->getCompanyImages($id);
		$this->load->template('company/company_images', $data);
    }

    public function save_images($id) {

    	$s3Client = new S3Client([
		    'version'     => 'latest',
		    'region'      => 'ap-southeast-1',
		    'credentials' => [
		        'key'    => 'AKIAJ7YGNWKVXXIWLBCA',
		        'secret' => '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'
		    ]
		]);

		
    	if($this->input->post('fileSubmit') && !empty($_FILES['userFiles']['name'])){
  
            $uploader = new MultipartUploader($s3Client, fopen($_FILES['userFiles']['tmp_name'], 'r'), [
			    'bucket' => 'manufacturer-bucket',
			    'key' => 'company-images/company-'.$id.'-'.$_FILES['userFiles']['name']
			]);

			try {
			    $result = $uploader->upload();
			    $statusMsg  = "Upload complete";
			    $this->session->set_flashdata('statusMsg',$statusMsg);
			  
			    $data['image_url'] = 'company-'.$id.'-'.$_FILES['userFiles']['name'];
			    $data['company_id'] = $id;
				$result = $this->db->insert('company_images', $data); 
				
			} catch (MultipartUploadException $e) {
			    $statusMsg = $e->getMessage() . "\n";
			    $this->session->set_flashdata('statusMsg',$statusMsg);
			}
    		
    	}

    	 redirect('company/upload-images/'.$id);

    }

    public function save_logo($id) {

    	$s3Client = new S3Client([
		    'version'     => 'latest',
		    'region'      => 'ap-southeast-1',
		    'credentials' => [
		        'key'    => 'AKIAJ7YGNWKVXXIWLBCA',
		        'secret' => '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'
		    ]
		]);

		
    	if($this->input->post('fileSubmit') && !empty($_FILES['userFiles']['name'])){
  
            $uploader = new MultipartUploader($s3Client, fopen($_FILES['userFiles']['tmp_name'], 'r'), [
			    'bucket' => 'manufacturer-bucket',
			    'key' => 'logo/logo-company-'.$id.'-'.$_FILES['userFiles']['name']
			]);

			try {
			    $result = $uploader->upload();
			    $statusMsg  = "Upload complete";
			    $this->session->set_flashdata('statusMsg',$statusMsg);

			    $this->db->where('id', $id);
			    $data['logo'] = 'logo-company-'.$id.'-'.$_FILES['userFiles']['name'];
			    $data['logo_link'] = $this->input->post('logo_link');
				$result = $this->db->update('company', $data); 
				
			} catch (MultipartUploadException $e) {
			    $statusMsg = $e->getMessage() . "\n";
			    $this->session->set_flashdata('statusMsg',$statusMsg);
			}
    		
    	}

    	 redirect('company/upload-logo/'.$id);

    }

    public function delete_logo($id, $image_name) {

    	$s3Client = new S3Client([
		    'version'     => 'latest',
		    'region'      => 'ap-southeast-1',
		    'credentials' => [
		        'key'    => 'AKIAJ7YGNWKVXXIWLBCA',
		        'secret' => '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'
		    ]
		]);

		$result = $s3Client->deleteObject(array(
			'Bucket' => 'manufacturer-bucket',
			'Key' => 'logo/'.$image_name
		));

		if($result){
			$result_image = $this->admin_company_model->deleteCompnayLogo($id);
			redirect('company/upload-logo/'.$id);
		} else {
			 $this->session->set_flashdata('statusMsg','Cannot delete your Image');
		}


    }

    public function delete_images($id, $image_name, $company_id) {

    	$s3Client = new S3Client([
		    'version'     => 'latest',
		    'region'      => 'ap-southeast-1',
		    'credentials' => [
		        'key'    => 'AKIAJ7YGNWKVXXIWLBCA',
		        'secret' => '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'
		    ]
		]);

		$result = $s3Client->deleteObject(array(
			'Bucket' => 'manufacturer-bucket',
			'Key' => 'company-images/'.$image_name
		));

		if($result){
			$result_image = $this->admin_company_model->deleteCompnayImages($id);
			redirect('company/upload-images/'.$company_id);
		} else {
			 $this->session->set_flashdata('statusMsg','Cannot delete your Image');
		}


    }
}
