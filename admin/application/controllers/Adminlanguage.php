<?php

//defined('BASEPATH') OR exit('No direct script access allowed');

//session_start(); //we need to start session in order to access it through CI

Class Adminlanguage extends CI_Controller {

	public function __construct() {
		parent::__construct();
		// Load database
		$this->load->model('admin_language_model');
	}

	public function index() {

		$data = array();
		$data['name'] = (!empty($this->input->post('name'))) ? strtolower(trim($this->input->post('name'))) : "";

		$config = array();
	    $config['base_url'] = base_url().'language';
	    $config['total_rows'] = $this->admin_language_model->countLanguage($data);
	    $config['per_page'] = 20;
	    $config['uri_segment'] = 2;
	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';
	    $config['prev_tag_open'] = '<li>';
	    $config['prev_tag_close'] = '</li>';
	    $config['next_tag_open'] = '<li>';
	    $config['next_tag_close'] = '</li>';
	    $config['first_tag_open'] = '<li>';
	    $config['first_tag_close'] ='</li>';
	    $config['last_tag_open'] = '<li>';
	    $config['last_tag_close'] ='</li>';
	    $config['cur_tag_open'] = '<li class="active"><a>';
	    $config['cur_tag_close'] = '</a></li>';
	    //$config['num_links'] = round($config['total_rows']/$config['per_page']);
	    $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
	    $this->pagination->initialize($config);

	    $data['LanguageList'] = $this->admin_language_model->getLanguageList($config['per_page'], $page, $data);
		$data["links"] = $this->pagination->create_links();

		$this->load->template('language/language_list', $data);
	}

	public function add() {
		$data['page_title'] = "Add Language";
		$data['button_title'] = "Add";
		$this->load->template('language/language_form', $data);
	}

	public function edit($id) {
		$data['page_title'] = "Edit Language";
		$data['button_title'] = "Edit";
		$data['languageData'] = $this->admin_language_model->getLanguageById($id);
		$this->load->template('language/language_form', $data);
	}

	public function delete($id) {
		$result = $this->admin_language_model->deleteLanguage($id);
		redirect('language');
	}

	public function save_language_process() {

		$this->form_validation->set_rules('name', 'Language Name', 'trim|required');

		$data = array(
			'name' => strtolower(trim($this->input->post('name'))),
			'code' => strtolower(trim($this->input->post('code'))),
			'id'  => $this->input->post('id'),
		);

		$data['page_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit Language" : "Add Language";
		$data['button_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";

		$isDuplicate = $this->admin_language_model->checkcLanguagebyName($data['name']);

		if ($this->form_validation->run() == FALSE):
			$data['error_message'] = 'Check your input';
			$this->load->template('language/language_form', $data);
		elseif($isDuplicate && $data['button_title'] == "Add"):
			$data['error_message'] = 'Duplicate Language Name';
			$this->load->template('language/language_form', $data);
		else:

			$language_data['name'] = $data['name'];
			$language_data['code'] = $data['code'];
			$language_data['id']   = $data['id'];

			$result = $this->admin_language_model->saveLanguage($language_data);

			if($result === TRUE) {
				$display_message = (isset($data['id']) && $data['id'] > 0) ? "Edit Language Successfully" : "Add Language Successfully";
				$page_title = (isset($data['id']) && $data['id'] > 0) ? "Edit Language" : "Add Language";
				$button_title = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";
				$languageData  = (isset($data['id']) && $data['id'] > 0) ?  $this->admin_language_model->getLanguageById($data['id']) : "";


				$data = array(
					'message_display' => $display_message,
					'page_title' => $page_title,
					'button_title' => $button_title,
					'languageData' => $languageData
				);
				
				$this->load->template('language/language_form', $data);
			} else {
				$data['error_message'] = 'Cannot save your data';
				$this->load->template('language/language_form', $data);
			}

		endif;
	}
}
