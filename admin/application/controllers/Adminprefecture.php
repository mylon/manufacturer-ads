<?php

//defined('BASEPATH') OR exit('No direct script access allowed');

//session_start(); //we need to start session in order to access it through CI

Class Adminprefecture extends CI_Controller {

	public function __construct() {
		parent::__construct();
		// Load database
		$this->load->model('admin_prefecture_model');
	}

	public function index() {

		$data = array();
		$data['name'] = (!empty($this->input->post('name'))) ? strtolower(trim($this->input->post('name'))) : "";
		$data['country_id'] = (!empty($this->input->post('country_id'))) ? strtolower(trim($this->input->post('country_id'))) : "";

		$config = array();
	    $config['base_url'] = base_url().'prefecture';
	    $config['total_rows'] = $this->admin_prefecture_model->countPrefecture($data);
	    $config['per_page'] = 20;
	    $config['uri_segment'] = 2;
	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';
	    $config['prev_tag_open'] = '<li>';
	    $config['prev_tag_close'] = '</li>';
	    $config['next_tag_open'] = '<li>';
	    $config['next_tag_close'] = '</li>';
	    $config['first_tag_open'] = '<li>';
	    $config['first_tag_close'] ='</li>';
	    $config['last_tag_open'] = '<li>';
	    $config['last_tag_close'] ='</li>';
	    $config['cur_tag_open'] = '<li class="active"><a>';
	    $config['cur_tag_close'] = '</a></li>';
	    //$config['num_links'] = round($config['total_rows']/$config['per_page']);
	    $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
	    $this->pagination->initialize($config);
	   
	    $data['countryList'] = $this->admin_prefecture_model->getCountryList();
	    $data['PrefectureList'] = $this->admin_prefecture_model->getPrefectureList($config['per_page'], $page, $data);
		$data["links"] = $this->pagination->create_links();

		$this->load->template('prefecture/prefecture_list', $data);
	}

	public function add() {
		$data['page_title'] = "Add Prefecture";
		$data['button_title'] = "Add";
		$data['countryList'] = $this->admin_prefecture_model->getCountryList();
		$this->load->template('prefecture/prefecture_form', $data);
	}

	public function edit($id) {
		$data['page_title'] = "Edit Prefecture";
		$data['button_title'] = "Edit";
		$data['countryList'] = $this->admin_prefecture_model->getCountryList();
		$data['prefectureData'] = $this->admin_prefecture_model->getPrefectureById($id);
		$this->load->template('prefecture/prefecture_form', $data);
	}

	public function delete($id) {
		$result = $this->admin_prefecture_model->deletePrefecture($id);
		redirect('prefecture');
	}

	public function save_prefecture_process() {

		$this->form_validation->set_rules('name', 'Prefecture', 'trim|required');

		$data = array(
			'name' => strtolower(trim($this->input->post('name'))),
			'country_id' => strtolower(trim($this->input->post('country_id'))),
			'id'  => $this->input->post('id'),
		);

		$data['page_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit Prefecture" : "Add Prefecture";
		$data['button_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";
		$data['countryList'] = $this->admin_prefecture_model->getCountryList();

		$isDuplicate = $this->admin_prefecture_model->checkcPrefectureName($data['name']);

		if ($this->form_validation->run() == FALSE):
			$data['error_message'] = 'Check your input';
			$this->load->template('prefecture/prefecture_form', $data);
		elseif($isDuplicate && $data['button_title'] == "Add"):
			$data['error_message'] = 'Duplicate Prefecture Name';
			$this->load->template('prefecture/prefecture_form', $data);
		else:

			$prefecture_data['name'] = $data['name'];
			$prefecture_data['country_id'] = $data['country_id'];
			$prefecture_data['id']   = $data['id'];

			$result = $this->admin_prefecture_model->savePrefecture($prefecture_data);

			if($result === TRUE) {
				$display_message = (isset($data['id']) && $data['id'] > 0) ? "Edit Prefecture Successfully" : "Add Prefecture Successfully";
				$page_title = (isset($data['id']) && $data['id'] > 0) ? "Edit Prefecture" : "Add Prefecture";
				$button_title = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";
				$prefectureData  = (isset($data['id']) && $data['id'] > 0) ? $this->admin_prefecture_model->getPrefectureById($data['id']) : "";
				$countryList = $this->admin_prefecture_model->getCountryList();

				$data = array(
					'message_display' => $display_message,
					'page_title' => $page_title,
					'button_title' => $button_title,
					'countryList' => $countryList,
					'prefectureData' => $prefectureData
				);
				
				$this->load->template('prefecture/prefecture_form', $data);
			} else {
				$data['error_message'] = 'Cannot save your data';
				$this->load->template('prefecture/prefecture_form', $data);
			}

		endif;
	}
}
