<?php

//defined('BASEPATH') OR exit('No direct script access allowed');

//session_start(); //we need to start session in order to access it through CI

require FCPATH.'/vendor/autoload.php';
use Aws\S3\S3Client;
use Aws\S3\MultipartUploader;
use Aws\Exception\MultipartUploadException;

Class Admincompany extends CI_Controller {

	public function __construct() {
		parent::__construct();
		// Load database
		$this->load->model('admin_company_model');
	}

	public function index() {

		$data = array();
		$data['name'] 				= (!empty($this->input->post('name'))) ? strtolower(trim($this->input->post('name'))) : "";

		$config = array();
	    $config['base_url'] = base_url().'company';
	    $config['total_rows'] = $this->admin_company_model->countCompany($data);
	    $config['per_page'] = 20;
	    $config['uri_segment'] = 2;
	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';
	    $config['prev_tag_open'] = '<li>';
	    $config['prev_tag_close'] = '</li>';
	    $config['next_tag_open'] = '<li>';
	    $config['next_tag_close'] = '</li>';
	    $config['first_tag_open'] = '<li>';
	    $config['first_tag_close'] ='</li>';
	    $config['last_tag_open'] = '<li>';
	    $config['last_tag_close'] ='</li>';
	    $config['cur_tag_open'] = '<li class="active"><a>';
	    $config['cur_tag_close'] = '</a></li>';
	    //$config['num_links'] = round($config['total_rows']/$config['per_page']);
	    $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
	    $this->pagination->initialize($config);

	    $data['companyType'] = $this->admin_company_model->getCompanyType();
	    $data['CompanyList'] = $this->admin_company_model->getCompanyList($config['per_page'], $page, $data);
		$data["links"] = $this->pagination->create_links();

		$this->load->template('company/company_list', $data);
	}

	public function add() {
		$data['page_title'] = "Add Company";
		$data['button_title'] = "Add";
		$data['companyType'] = $this->admin_company_model->getCompanyType();
		$this->load->template('company/company_form', $data);
	}

	public function edit($id) {
		$data['page_title'] = "Edit Company";
		$data['button_title'] = "Edit";
		$data['companyType'] = $this->admin_company_model->getCompanyType();
		$data['companyData'] = $this->admin_company_model->getCompanyById($id);
		$this->load->template('company/company_form', $data);
	}

	public function delete($id) {
		$result = $this->admin_company_model->deleteCompany($id);
		redirect('company');
	}

	public function save_company_process() {

		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('web_url', 'URL', 'trim|required');

		$data = array(
			'name' => trim($this->input->post('name')),
			'web_url' => strtolower(trim($this->input->post('web_url'))),
			'type' => strtolower(trim($this->input->post('type'))),
			'colour_menu' => strtolower(trim($this->input->post('colour_menu'))),
			'colour_link' => strtolower(trim($this->input->post('colour_link'))),
			'bgcolor' => strtolower(trim($this->input->post('bgcolor'))),
			'google_analytics' => strtolower(trim($this->input->post('google_analytics'))),
			'id'  => $this->input->post('id')
		);

		$data['page_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit Company" : "Add Company";
		$data['button_title'] = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";
		$data['companyType'] = $this->admin_company_model->getCompanyType();

		$isDuplicate = $this->admin_company_model->checkCompanybyName($data['name']);

		if ($this->form_validation->run() == FALSE):
			$data['error_message'] = 'Check your input';
			$this->load->template('company/company_form', $data);
		elseif($isDuplicate && $data['button_title'] == "Add"):
			$data['error_message'] = 'Duplicate Company Name';
			$this->load->template('company/company_form', $data);
		else:

			$company_data['name'] = $data['name'];
			$company_data['web_url']   = $data['web_url'];
			$company_data['type']   = $data['type'];
			$company_data['colour_menu']   = $data['colour_menu'];
			$company_data['colour_link']   = $data['colour_link'];
			$company_data['bgcolor']   = $data['bgcolor'];
			$company_data['google_analytics']   = $data['google_analytics'];
			$company_data['id']   = $data['id'];

			$result = $this->admin_company_model->saveCompany($company_data);

			if($result === TRUE) {
				$display_message = (isset($data['id']) && $data['id'] > 0) ? "Edit Company Successfully" : "Add Company Successfully";
				$page_title = (isset($data['id']) && $data['id'] > 0) ? "Edit Company" : "Add Company";
				$button_title = (isset($data['id']) && $data['id'] > 0) ? "Edit" : "Add";
				$companyData  = (isset($data['id']) && $data['id'] > 0) ? $this->admin_company_model->getCompanyById($data['id']) : "";
				$companyType = $this->admin_company_model->getCompanyType();

				$data = array(
					'message_display' => $display_message,
					'page_title' => $page_title,
					'button_title' => $button_title,
					'companyData' => $companyData,
					'companyType' => $companyType
				);
				
				$this->load->template('company/company_form', $data);
			} else {
				$data['error_message'] = 'Cannot save your data';
				$this->load->template('company/company_form', $data);
			}

		endif;
	}

	public function upload_logo($id) {
        $data['id'] = $id;
        $data['files'] = $this->admin_company_model->getLogoImage($id);
		$this->load->template('company/company_logo', $data);
    }

    public function save_logo($id) {

    	$s3Client = new S3Client([
		    'version'     => 'latest',
		    'region'      => 'ap-southeast-1',
		    'credentials' => [
		        'key'    => 'AKIAJ7YGNWKVXXIWLBCA',
		        'secret' => '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'
		    ]
		]);

		
    	if($this->input->post('fileSubmit') && !empty($_FILES['userFiles']['name'])){
  
            $uploader = new MultipartUploader($s3Client, fopen($_FILES['userFiles']['tmp_name'], 'r'), [
			    'bucket' => 'manufacturer-bucket',
			    'key' => 'logo/logo-company-'.$id.'-'.$_FILES['userFiles']['name']
			]);

			try {
			    $result = $uploader->upload();
			    $statusMsg  = "Upload complete";
			    $this->session->set_flashdata('statusMsg',$statusMsg);

			    $this->db->where('id', $id);
			    $data['logo'] = 'logo-company-'.$id.'-'.$_FILES['userFiles']['name'];
			    $data['logo_link'] = $this->input->post('logo_link');
				$result = $this->db->update('company', $data); 
				
			} catch (MultipartUploadException $e) {
			    $statusMsg = $e->getMessage() . "\n";
			    $this->session->set_flashdata('statusMsg',$statusMsg);
			}
    		
    	}

    	 redirect('company/upload-logo/'.$id);

    }

    public function delete_logo($id, $image_name) {

    	$s3Client = new S3Client([
		    'version'     => 'latest',
		    'region'      => 'ap-southeast-1',
		    'credentials' => [
		        'key'    => 'AKIAJ7YGNWKVXXIWLBCA',
		        'secret' => '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'
		    ]
		]);

		$result = $s3Client->deleteObject(array(
			'Bucket' => 'manufacturer-bucket',
			'Key' => 'logo/'.$image_name
		));

		if($result){
			$result_image = $this->admin_company_model->deleteCompnayLogo($id);
			redirect('company/upload-logo/'.$id);
		} else {
			 $this->session->set_flashdata('statusMsg','Cannot delete your Image');
		}

    }

     public function upload_images($id) {
        $data['id'] = $id;
        $data['files'] = $this->admin_company_model->getCompanyImages($id);
		$this->load->template('company/company_images', $data);
    }

    public function save_images($id) {

    	$s3Client = new S3Client([
		    'version'     => 'latest',
		    'region'      => 'ap-southeast-1',
		    'credentials' => [
		        'key'    => 'AKIAJ7YGNWKVXXIWLBCA',
		        'secret' => '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'
		    ]
		]);

		
    	if($this->input->post('fileSubmit') && !empty($_FILES['userFiles']['name'])){
  
            $uploader = new MultipartUploader($s3Client, fopen($_FILES['userFiles']['tmp_name'], 'r'), [
			    'bucket' => 'manufacturer-bucket',
			    'key' => 'company-images/company-'.$id.'-'.$_FILES['userFiles']['name']
			]);

			try {
			    $result = $uploader->upload();
			    $statusMsg  = "Upload complete";
			    $this->session->set_flashdata('statusMsg',$statusMsg);
			  
			    $data['image_url'] = 'company-'.$id.'-'.$_FILES['userFiles']['name'];
			    $data['company_id'] = $id;
			    $data['image_link'] = $this->input->post('image_link');
				$result = $this->db->insert('company_images', $data); 
				
			} catch (MultipartUploadException $e) {
			    $statusMsg = $e->getMessage() . "\n";
			    $this->session->set_flashdata('statusMsg',$statusMsg);
			}
    		
    	}

    	 redirect('company/upload-images/'.$id);

    }

    public function delete_images($id, $image_name, $company_id) {

    	$s3Client = new S3Client([
		    'version'     => 'latest',
		    'region'      => 'ap-southeast-1',
		    'credentials' => [
		        'key'    => 'AKIAJ7YGNWKVXXIWLBCA',
		        'secret' => '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'
		    ]
		]);

		$result = $s3Client->deleteObject(array(
			'Bucket' => 'manufacturer-bucket',
			'Key' => 'company-images/'.$image_name
		));

		if($result){
			$result_image = $this->admin_company_model->deleteCompnayImages($id);
			redirect('company/upload-images/'.$company_id);
		} else {
			 $this->session->set_flashdata('statusMsg','Cannot delete your Image');
		}


    }
}
