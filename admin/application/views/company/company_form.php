<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!isset($this->session->userdata['manufacturer_staff_logged_in'])) {
    header("location: ".base_url()."login");
    exit;
}

$id 				= (!empty($companyData)) ? $companyData[0]->id : 0;
$name  				= (!empty($companyData)) ? $companyData[0]->name : '';
$web_url  			= (!empty($companyData)) ? $companyData[0]->web_url : '';
$colour_menu  		= (!empty($companyData)) ? $companyData[0]->colour_menu : '';
$colour_link  		= (!empty($companyData)) ? $companyData[0]->colour_link : '';
$bgcolor  			= (!empty($companyData)) ? $companyData[0]->bgcolor : '';
$google_analytics  	= (!empty($companyData)) ? $companyData[0]->google_analytics : '';
$type  		= (!empty($companyData)) ? $companyData[0]->type : '';

?>

<div class="container-fluid fix-height-container">
	<div class="content-title">
		<h1><?= $page_title; ?></h1>
	</div>
	<?php
		if (isset($error_message)) :
			echo "<div class='error_msg bg-danger'>";
			if (isset($error_message)) {
				echo $error_message;
			}
			echo validation_errors();
			echo "</div>";
		endif;

		if (isset($message_display)) {
			echo "<div class='message bg-success'>";
			echo $message_display;
			echo "</div>";
		}
	?>
	<form class="form-horizontal" method="post" action="<?= base_url(); ?>company/save-company">
		<div class="form-group">
		    <label for="name" class="col-sm-1 control-label">Name</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="name" id="name" value="<?= $name; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="web_url" class="col-sm-1 control-label">URL</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="web_url" id="web_url" value="<?= $web_url; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="colour_link" class="col-sm-1 control-label">Link Colour</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="colour_link" id="colour_link" value="<?= $colour_link; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="colour_menu" class="col-sm-1 control-label">Menu Colour</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="colour_menu" id="colour_menu" value="<?= $colour_menu; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="bgcolor" class="col-sm-1 control-label">BG Colour</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="bgcolor" id="bgcolor" value="<?= $bgcolor; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="type" class="col-sm-1 control-label">Type</label>
		    <div class="col-sm-4">
		      <select class="form-control" name="type">
		      		<option value="">Please Select</option>
		      		<?php foreach($companyType as $k => $v):?>
		      		<?php $selected = ($type == $k) ? "selected" : "";?>
				  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
					<?php endforeach; ?>
				</select>
		    </div>
		</div>
		<!--<div class="form-group">
		    <label for="google_analytics" class="col-sm-1 control-label">Google Analytics</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="google_analytics" id="google_analytics" value="<?= $google_analytics; ?>">
		    </div>
		</div>-->
		<div class="form-group signup-button-center">
		    <div class="col-sm-offset-1 col-sm-4">
		      <a class="btn btn-default btn-custom-grey" href="<?= base_url();?>company" role="button">Back</a>
		      <button type="submit" class="btn btn-primary btn-custom-blue"><?= $button_title; ?></button>
		    </div>
		</div>
		<input type="hidden" class="form-control" name="staff_id" id="staff_id" value="<?= $this->session->userdata['manufacturer_staff_logged_in']['staff_id']; ?>">
		<input type="hidden" class="form-control" name="id" id="id" value="<?= $id; ?>">
	</form>
</div>