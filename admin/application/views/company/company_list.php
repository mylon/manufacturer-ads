<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!isset($this->session->userdata['manufacturer_staff_logged_in'])) {
    header("location: ".base_url()."login");
    exit;
}

$company_name = isset($name) ? $name : "";
$company_type = isset($type_id) ? $type_id : "";
$company_id   = $this->session->userdata['manufacturer_staff_logged_in']['staff_company'];

?>
<script type="text/javascript">
	var url="<?php echo base_url();?>";
    function deleteCheck(id) {
        var r=confirm("Do you want to delete this company?");
        if (r==true) {
        	window.location = url+"company/delete/"+id;
        } 
        else {
        	 return false;
        }
    } 
</script>
<div class="container-fluid fix-height-container">
	<div class="content-title">
		<h1>Company List</h1>
	</div>
	<?php
		if (isset($message_display)) {
			echo "<div class='message bg-success'>";
			echo $message_display;
			echo "</div>";
		}
	?>

	<div class="search-content">
		<form class="form-horizontal" method="post" action="<?= base_url(); ?>company">
			<div class="form-group">
			    <label for="name" class="col-sm-1 control-label">Name</label>
			    <div class="col-sm-2">
			      <input type="text" class="form-control" name="name" id="name" value="<?= $company_name; ?>">
			    </div>
			</div>
			<div class="form-group">
			    <label for="type_id" class="col-sm-1 control-label">Type</label>
			    <div class="col-sm-2">
			      <select class="form-control" name="type_id">
			      		<option value="">Please Select</option>
			      		<?php foreach($companyType as $k => $v):?>
			      		<?php $selected = ($company_type == $k) ? "selected" : "";?>
					  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
						<?php endforeach; ?>
					</select>
			    </div>
			</div>
			<div class="form-group signup-button-center">
			    <div class="col-sm-offset-1 col-sm-4">
			      <a class="btn btn-default btn-custom-grey" href="<?= base_url();?>company" role="button">Reset</a>
			      <button type="submit" class="btn btn-primary btn-custom-blue">Search</button>
			    </div>
			</div>
		</form>
	</div>

	<div id="hospital-list" class="table-responsive">
		<?php if($company_id <= 0): ?>
		<div class="add-button add-button-right">
			<a class="btn btn-primary" role="button" href="<?= base_url(); ?>company/add">Add Compnany</a>
		</div>
	<?php endif;?>
		<table class="table table-striped">
			<thead>
				<th>ID</th>
				<th>Name</th>
				<th>Type</th>
				<th>Logo</th>
				<th>Images</th>
				<th>Edit</th>
				<?php if($company_id <= 0): ?><th>Delete</th><?php endif;?>
			</thead>
			<tbody>
				<?php if(!empty($CompanyList)): ?>
					<?php foreach($CompanyList as $company):?>
						<tr>
							<td><?= $company->id; ?></td>
							<td><?= ucfirst($company->name); ?></td>
							<td><?= ucfirst($company->type_name); ?></td>
							<td><a href="<?= base_url(); ?>company/upload-logo/<?= $company->id; ?>">Upload</a></td>
							<td><a href="<?= base_url(); ?>company/upload-images/<?= $company->id; ?>">Upload</a></td>
							<td><a href="<?= base_url(); ?>company/edit/<?= $company->id; ?>">Edit</a></td>
							<?php if($company_id <= 0): ?><td><a href="javascript:void(0);" onclick="deleteCheck(<?= $company->id ?>);">Delete</a></td><?php endif; ?>
						</tr>
						
					<?php endforeach;?>
				<?php else: ?>
						<tr>
							<td colspan="7">No results found</td>
						</tr>
				<?php endif;?>
			</tbody>
		</table>
	</div>
	<nav>
		<ul class="pagination">
			<?php echo $links; ?>
		</ul>
	</nav>
</div>
