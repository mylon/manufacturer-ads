<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!isset($this->session->userdata['manufacturer_staff_logged_in'])) {
    header("location: ".base_url()."login");
    exit;
}

$id 				= (!empty($productData)) ? $productData[0]->id : 0;
$name  				= (!empty($productData)) ? $productData[0]->name : '';
$company_id  		= (!empty($productData)) ? $productData[0]->company_id : '';
$category_id  		= (!empty($productData)) ? $productData[0]->category_id : '';
$staff_company_id = $this->session->userdata['manufacturer_staff_logged_in']['staff_company'];

?>

<div class="container-fluid fix-height-container">
	<div class="content-title">
		<h1><?= $page_title; ?></h1>
	</div>
	<?php
		if (isset($error_message)) :
			echo "<div class='error_msg bg-danger'>";
			if (isset($error_message)) {
				echo $error_message;
			}
			echo validation_errors();
			echo "</div>";
		endif;

		if (isset($message_display)) {
			echo "<div class='message bg-success'>";
			echo $message_display;
			echo "</div>";
		}
	?>
	<form class="form-horizontal" method="post" action="<?= base_url(); ?>company-products/save-company-products">
		<div class="form-group">
		    <label for="name" class="col-sm-1 control-label">Product Name</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="name" id="name" value="<?= $name; ?>">
		    </div>
		</div>
		<?php if($staff_company_id > 0): ?>
			<input type="hidden" class="form-control" name="company_id" id="company_id" value="<?= $staff_company_id; ?>">
		<?php else:?>
		<div class="form-group">
		    <label for="company_id" class="col-sm-1 control-label">Company</label>
		    <div class="col-sm-4">
		      <select class="form-control" name="company_id" id="company_id">
		      		<option value="">Please Select</option>
		      		<?php foreach($companyList as $k => $v):?>
		      		<?php $selected = ($company_id == $k) ? "selected" : "";?>
				  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
					<?php endforeach; ?>
				</select>
		    </div>
		</div>
		<?php endif;?>
		<div class="form-group">
		    <label for="category_id" class="col-sm-1 control-label">Category</label>
		    <div class="col-sm-4">
		      <select class="form-control" name="category_id" id="category_id">
		      		<option value="">Please Select</option>
		      		<?php foreach($categoryList as $k => $v):?>
		      		<?php $selected = ($category_id == $k) ? "selected" : "";?>
				  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
					<?php endforeach; ?>
				</select>
		    </div>
		</div>
		<div class="form-group signup-button-center">
		    <div class="col-sm-offset-1 col-sm-4">
		      <a class="btn btn-default btn-custom-grey" href="<?= base_url();?>company-products" role="button">Back</a>
		      <button type="submit" class="btn btn-primary btn-custom-blue"><?= $button_title; ?></button>
		    </div>
		</div>
		<input type="hidden" class="form-control" name="staff_id" id="staff_id" value="<?= $this->session->userdata['manufacturer_staff_logged_in']['staff_id']; ?>">
		<input type="hidden" class="form-control" name="id" id="id" value="<?= $id; ?>">
	</form>
</div>
<script type="text/javascript">
	function capitalize(string) {
		return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
	}
	
	$('#company_id').change(function(){
	    var company_id = $(this).val();
	    $("#category_id > option").remove();
	    $.ajax({
	        type: "POST",
	        url: "<?php echo site_url('admincompanyproductprofile/category_list'); ?>",
	        data: {company_id: company_id},
	        dataType: 'json',
	        success:function(data){
	        	$('#category_id').html('<option value="0">Please Select</option>')
	            $.each(data,function(k, v){
	                var opt = $('<option />');
	                opt.val(k);
	                opt.text(capitalize(v));
	                $('#category_id').append(opt);
	            });
	        }
	    });
	});
</script>