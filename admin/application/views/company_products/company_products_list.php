<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!isset($this->session->userdata['manufacturer_staff_logged_in'])) {
    header("location: ".base_url()."login");
    exit;
}

$product_name = isset($name) ? $name : "";
$start_date = isset($start_date) ? $start_date : "";
$end_date = isset($end_date) ? $end_date : "";
$staff_id = isset($created_by) ? $created_by : "";
$company_id = isset($company_id) ? $company_id : "";
$category_id = isset($category_id) ? $category_id : "";
$staff_company_id = $this->session->userdata['manufacturer_staff_logged_in']['staff_company'];

?>
<script type="text/javascript">
	var url="<?php echo base_url();?>";
    function deleteCheck(id) {
        var r=confirm("Do you want to delete this product?");
        if (r==true) {
        	window.location = url+"company-products/delete/"+id;
        } 
        else {
        	 return false;
        }
    } 
</script>
<div class="container-fluid fix-height-container">
	<div class="content-title">
		<h1>Product List</h1>
	</div>
	<?php
		if (isset($message_display)) {
			echo "<div class='message bg-success'>";
			echo $message_display;
			echo "</div>";
		}
	?>
	<div class="search-content">
		<form class="form-horizontal" method="post" action="<?= base_url(); ?>company-products">
			<div class="form-group">
			    <label for="name" class="col-sm-1 control-label">Name</label>
			    <div class="col-sm-2">
			      <input type="text" class="form-control" name="name" id="name" value="<?= $product_name; ?>">
			    </div>
			</div>
			<div class="form-group">
			    <label for="title" class="col-sm-1 control-label">Created</label>
			    <div class="col-sm-2">
			      <input type="text" class="form-control" name="start_date" id="start_date" value="<?= $start_date; ?>"> (YYYY-MM-DD)
			    </div>
			</div>
			<div class="form-group">
			    <label for="title" class="col-sm-1 control-label">Edited</label>
			    <div class="col-sm-2">
			      <input type="text" class="form-control" name="end_date" id="end_date" value="<?= $end_date; ?>"> (YYYY-MM-DD)
			    </div>
			</div>
			<?php if($staff_company_id > 0): ?>
				<input type="hidden" class="form-control" name="company_id" id="company_id" value="<?= $staff_company_id; ?>">
			<?php else:?>
			<div class="form-group">
		    	<label for="company_id" class="col-sm-1 control-label">Company</label>
			    <div class="col-sm-2">
			      <select class="form-control" name="company_id" id="company_id">
			      		<option value="">Please Select</option>
			      		<?php foreach($companyList as $k => $v):?>
			      		<?php $selected = ($company_id == $k) ? "selected" : "";?>
					  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
						<?php endforeach; ?>
					</select>
			    </div>
			</div>
			<?php endif;?>
			<div class="form-group">
			    <label for="category_id" class="col-sm-1 control-label">Category</label>
			    <div class="col-sm-2">
			      <select class="form-control" name="category_id" id="category_id">
			      		<option value="">Please Select</option>
			      		<?php foreach($categoryList as $k => $v):?>
			      		<?php $selected = ($category_id == $k) ? "selected" : "";?>
					  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
						<?php endforeach; ?>
					</select>
			    </div>
			</div>
			<div class="form-group">
		    	<label for="staff_id" class="col-sm-1 control-label">Created By</label>
			    <div class="col-sm-2">
			      <select class="form-control" name="staff_id">
			      		<option value="">Please Select</option>
			      		<?php foreach($staffList as $k => $v):?>
			      		<?php $selected = ($staff_id == $k) ? "selected" : "";?>
					  		<option value="<?= $k; ?>" <?= $selected?>><?= $v; ?></option>
						<?php endforeach; ?>
					</select>
			    </div>
			</div>
			<div class="form-group signup-button-center">
			    <div class="col-sm-offset-1 col-sm-4">
			      <a class="btn btn-default btn-custom-grey" href="<?= base_url();?>company-products" role="button">Reset</a>
			      <button type="submit" class="btn btn-primary btn-custom-blue">Search</button>
			    </div>
			</div>
		</form>
	</div>
	<div id="hospital-list" class="table-responsive">
		<div class="add-button add-button-right">
			<a class="btn btn-primary" role="button" href="<?= base_url(); ?>company-products/add">Add Product</a>
		</div>
		<table class="table table-striped">
			<thead>
				<th>ID</th>
				<th>Name</th>
				<th>Company</th>
				<th>Category</th>
				<th>Images</th>
				<th>Description Images</th>
				<th>Created By</th>
				<th>Edited By</th>
				<th>Created Date</th>
				<th>Edit</th>
				<th>Delete</th>
			</thead>
			<tbody>
				<?php if(!empty($ProductList)): ?>
					<?php foreach($ProductList as $product):?>
						<tr>
							<td><?= $product->id; ?></td>
							<td><?= ucfirst($product->name); ?></td>
							<td><?= ucfirst($product->company_name); ?></td>
							<td><?= ucfirst($product->category_name); ?></td>
							<td><a href="<?= base_url(); ?>company-products/upload-image/<?= $product->id; ?>">Upload</a></td>
							<td><a href="<?= base_url(); ?>company-products/description-upload-image/<?= $product->id; ?>">Upload</a></td>
							<td><?= ucfirst($product->created_by); ?></td>
							<td><?= ucfirst($product->edited_by); ?></td>
							<td><?= date('d/m/Y', strtotime($product->created_date)); ?></td>
							<td><a href="<?= base_url(); ?>company-products/edit/<?= $product->id; ?>">Edit</a></td>
							<td><a href="javascript:void(0);" onclick="deleteCheck(<?= $product->id ?>);">Delete</a></td>
						</tr>
						
					<?php endforeach;?>
				<?php else: ?>
						<tr>
							<td colspan="9">No results found</td>
						</tr>
				<?php endif;?>
			</tbody>
		</table>
	</div>
	<nav>
		<ul class="pagination">
			<?php echo $links; ?>
		</ul>
	</nav>
</div>
<script type="text/javascript">
	function capitalize(string) {
		return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
	}
	
	$('#company_id').change(function(){
	    var company_id = $(this).val();
	    $("#category_id > option").remove();
	    $.ajax({
	        type: "POST",
	        url: "<?php echo site_url('admincompanyproductprofile/category_list'); ?>",
	        data: {company_id: company_id},
	        dataType: 'json',
	        success:function(data){
	        	$('#category_id').html('<option value="0">Please Select</option>')
	            $.each(data,function(k, v){
	                var opt = $('<option />');
	                opt.val(k);
	                opt.text(capitalize(v));
	                $('#category_id').append(opt);
	            });
	        }
	    });
	});
</script>