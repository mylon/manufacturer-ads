<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!isset($this->session->userdata['manufacturer_staff_logged_in'])) {
    header("location: ".base_url()."login");
    exit;
}

$id 				= (!empty($productData)) ? $productData[0]->id : 0;
$title  			= (!empty($productData)) ? $productData[0]->title : '';
$name  				= (!empty($productData)) ? $productData[0]->name : '';
$description  		= (!empty($productData)) ? $productData[0]->description : '';
$specs  			= (!empty($productData)) ? $productData[0]->specs : '';
$language_id  		= (!empty($productData)) ? $productData[0]->language_id : '';
$company_products_id  		= (!empty($productData)) ? $productData[0]->company_products_id : '';
$meta_title  		= (!empty($productData)) ? $productData[0]->meta_title : '';
$meta_description  	= (!empty($productData)) ? $productData[0]->meta_description : '';
?>

<div class="container-fluid fix-height-container">
	<div class="content-title">
		<h1><?= $page_title; ?></h1>
	</div>
	<?php
		if (isset($error_message)) :
			echo "<div class='error_msg bg-danger'>";
			if (isset($error_message)) {
				echo $error_message;
			}
			echo validation_errors();
			echo "</div>";
		endif;

		if (isset($message_display)) {
			echo "<div class='message bg-success'>";
			echo $message_display;
			echo "</div>";
		}
	?>
	<form class="form-horizontal" method="post" action="<?= base_url(); ?>company-products-profile/save-company-products">
		<div class="form-group">
		    <label for="title" class="col-sm-1 control-label">Product Title</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="title" id="title" value="<?= htmlspecialchars_decode($title); ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="name" class="col-sm-1 control-label">Product Name</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="name" id="name" value="<?= $name; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="code" class="col-sm-1 control-label">Description</label>
		    <div class="col-sm-4">
		      <textarea class="form-control" rows="3" name="description" id="description"><?= htmlspecialchars_decode($description); ?></textarea>
		    </div>
		</div>
		<div class="form-group">
		    <label for="meta_title" class="col-sm-1 control-label">Meta Title</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="meta_title" id="meta_title" value="<?= $meta_title; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="meta_description" class="col-sm-1 control-label">Meta Description</label>
		    <div class="col-sm-4">
		      <textarea class="form-control" rows="3" name="meta_description" id="meta_description"><?= htmlspecialchars_decode($meta_description); ?></textarea>
		    </div>
		</div>
		<div class="form-group">
		    <label for="code" class="col-sm-1 control-label">Specs</label>
		    <div class="col-sm-4">
		      <textarea class="form-control" rows="3" name="specs" id="specs"><?= $specs; ?></textarea>
		    </div>
		</div>
		<div class="form-group">
		    <label for="company_products_id" class="col-sm-1 control-label">Products</label>
		    <div class="col-sm-4">
		      <select class="form-control" name="company_products_id" id="company_products_id">
		      		<option value="">Please Select</option>
		      		<?php foreach($productList as $k => $v):?>
		      		<?php $selected = ($company_products_id == $k) ? "selected" : "";?>
				  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
					<?php endforeach; ?>
				</select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="language_id" class="col-sm-1 control-label">Langugae</label>
		    <div class="col-sm-4">
		      <select class="form-control" name="language_id">
		      		<option value="">Please Select</option>
		      		<?php foreach($languageList as $k => $v):?>
		      		<?php $selected = ($language_id == $k) ? "selected" : "";?>
				  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
					<?php endforeach; ?>
				</select>
		    </div>
		</div>
		<div class="form-group signup-button-center">
		    <div class="col-sm-offset-1 col-sm-4">
		      <a class="btn btn-default btn-custom-grey" href="<?= base_url();?>company-products-profile" role="button">Back</a>
		      <button type="submit" class="btn btn-primary btn-custom-blue"><?= $button_title; ?></button>
		    </div>
		</div>
		<input type="hidden" class="form-control" name="staff_id" id="staff_id" value="<?= $this->session->userdata['manufacturer_staff_logged_in']['staff_id']; ?>">
		<input type="hidden" class="form-control" name="id" id="id" value="<?= $id; ?>">
	</form>
</div>