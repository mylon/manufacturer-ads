<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!isset($this->session->userdata['manufacturer_staff_logged_in'])) {
    header("location: ".base_url()."login");
    exit;
}

$logo_path = 'https://s3-ap-southeast-1.amazonaws.com/manufacturer-bucket/company-products-pdf/';

?>

<script type="text/javascript">
    var url="<?php echo base_url();?>";
    function deleteCheck(file_id, fileUrl, product_profile_id) {
        var r=confirm("Do you want to delete this file?");
        if (r==true) {
            window.location = url+"company-products-profile/delete-pdf/"+file_id+"/"+fileUrl+"/"+product_profile_id;
        } 
        else {
             return false;
        }
    } 
</script>

<div class="container-fluid admin-container image-gallery">
	<h1>Product Brochure</h1>
	<div class="row">
        <div class="col-lg-12">
            <div class="col-lg-12">
                <p><?php echo $this->session->flashdata('statusMsg'); ?></p>
            </div>
            <form class="form-horizontal" enctype="multipart/form-data" action="<?php echo base_url(); ?>company-products-profile/save-pdf/<?php echo $id; ?>" method="post">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="file_name" class="col-sm-4 control-label">File Name</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="file_name" id="file_name" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="uploadImage">Choose File</label>
                    <div class="col-sm-8">
                        <input type="file" id="uploadImage" class="form-control" name="userFiles"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="uploadImage"></label>
                    <div class="col-sm-4">
                         <input class="btn btn-primary" type="submit" name="fileSubmit" value="UPLOAD"/>
                    </div>
                </div>
            </div>
            </form>
        </div>
        <div class="col-lg-12">
            <div class="row">
                <table class="table table-striped">
                    <thead>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Download</th>
                        <th>Delete</th>
                    </thead>
                    <tbody>
                        <?php if(!empty($files)): foreach($files as $file): if(!empty($file->file_url)):?>
                        <tr>
                            <td><?php echo $file->id?></td>
                            <td><?php echo $file->file_name?></td>
                            <td><a class="btn btn-primary" target="_blank" href="<?php echo $logo_path.$file->file_url?>">Download</a></td>
                            <td><button type="button" class="btn btn-danger" onclick="deleteCheck('<?= $file->id ?>', '<?= $file->file_url ?>', '<?= $file->product_profile_id ?>');">Delete</button></td>
                        </tr>
                        <?php endif; endforeach; else: ?>
                        <tr>
                            <td colspan="4">No results found</td>
                        </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>