<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!isset($this->session->userdata['manufacturer_staff_logged_in'])) {
    header("location: ".base_url()."login");
    exit;
}

?>
<script type="text/javascript">
	var url="<?php echo base_url();?>";
    function deleteCheck(id) {
        var r=confirm("Do you want to delete this category?");
        if (r==true) {
        	window.location = url+"article-category/delete/"+id;
        } 
        else {
        	 return false;
        }
    } 
</script>
<div class="container-fluid fix-height-container">
	<div class="content-title">
		<h1>Article Category List</h1>
	</div>
	<?php
		if (isset($message_display)) {
			echo "<div class='message bg-success'>";
			echo $message_display;
			echo "</div>";
		}
	?>
	<div id="hospital-list" class="table-responsive">
		<div class="add-button add-button-right">
			<a class="btn btn-primary" role="button" href="<?= base_url(); ?>article-category/add">Add Category</a>
		</div>
		<table class="table table-striped">
			<thead>
				<th>ID</th>
				<th>Name</th>
				<th>Edit</th>
				<th>Delete</th>
			</thead>
			<tbody>
				<?php if(!empty($CategoryList)): ?>
					<?php foreach($CategoryList as $category):?>
						<tr>
							<td><?= $category->id; ?></td>
							<td><?= ucfirst($category->name); ?></td>
							<td><a href="<?= base_url(); ?>article-category/edit/<?= $category->id; ?>">Edit</a></td>
							<td><a href="javascript:void(0);" onclick="deleteCheck(<?= $category->id ?>);">Delete</a></td>
						</tr>
						
					<?php endforeach;?>
				<?php else: ?>
						<tr>
							<td colspan="4">No results found</td>
						</tr>
				<?php endif;?>
			</tbody>
		</table>
	</div>
	<nav>
		<ul class="pagination">
			<?php echo $links; ?>
		</ul>
	</nav>
</div>