
<div id="main" class="fix-height-container">
	<div id="admin-login-form" class="container-fluid content-wrapper">
		<div class="content-title">
			<h1>Welcome to Manufacturer Advertising Management</h1>
		</div>
		<form class="form-horizontal" action="<?= base_url(); ?>login" method="post">
			<?php
				if (isset($logout_message)) {
					echo "<div class='message bg-success'>";
					echo $logout_message;
					echo "</div>";
				}

				if (isset($message_display)) {
					echo "<div class='message bg-success'>";
					echo $message_display;
					echo "</div>";
				}
				
				if (isset($error_message)) :
					echo "<div class='error_msg bg-danger'>";
					if (isset($error_message)) {
						echo $error_message;
					}
					echo validation_errors();
					echo "</div>";
				endif;
			?>
			<div class="form-group">
				<label for="username" class="col-sm-2 control-label">Username :</label>
				<div class="col-sm-10">
					<input type="text" name="username" id="username" placeholder="username" class="form-control"/>
				</div>
			</div>
			<div class="form-group">
				<label for="password" class="col-sm-2 control-label">Password :</label>
				<div class="col-sm-10">
					<input type="password" name="password" id="password" placeholder="**********" class="form-control"/>
				</div>
			</div>
			<div class="form-group signup-button-center">
			    <div class="col-sm-offset-2 col-sm-10">
			      <button type="submit" class="btn btn-primary btn-custom-blue">Sign in</button>
			    </div>
			 </div>
		</form>
	</div>
</div>
	