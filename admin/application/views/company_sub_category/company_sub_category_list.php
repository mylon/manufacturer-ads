<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!isset($this->session->userdata['manufacturer_staff_logged_in'])) {
    header("location: ".base_url()."login");
    exit;
}

$category_name = isset($name) ? $name : "";
$company_category_id = isset($company_category_id) ? $company_category_id : "";

?>
<script type="text/javascript">
	var url="<?php echo base_url();?>";
    function deleteCheck(id) {
        var r=confirm("Do you want to delete this company category?");
        if (r==true) {
        	window.location = url+"sub-company-category/delete/"+id;
        } 
        else {
        	 return false;
        }
    } 
</script>
<div class="container-fluid fix-height-container">
	<div class="content-title">
		<h1>Sub Company Category List</h1>
	</div>
	<?php
		if (isset($message_display)) {
			echo "<div class='message bg-success'>";
			echo $message_display;
			echo "</div>";
		}
	?>
	<div class="search-content">
		<form class="form-horizontal" method="post" action="<?= base_url(); ?>sub-company-category">
			<div class="form-group">
			    <label for="name" class="col-sm-1 control-label">Name</label>
			    <div class="col-sm-2">
			      <input type="text" class="form-control" name="name" id="name" value="<?= $category_name; ?>">
			    </div>
			</div>
			<div class="form-group">
			    <label for="company_category_id" class="col-sm-1 control-label">Main Category</label>
			    <div class="col-sm-2">
			      <select class="form-control" name="company_category_id" id="company_category_id">
			      		<option value="">Please Select</option>
			      		<?php foreach($maincategoryList as $k => $v):?>
			      		<?php $selected = ($company_category_id == $k) ? "selected" : "";?>
					  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
						<?php endforeach; ?>
					</select>
			    </div>
			</div>
			<div class="form-group signup-button-center">
			    <div class="col-sm-offset-1 col-sm-4">
			      <a class="btn btn-default btn-custom-grey" href="<?= base_url();?>sub-company-category" role="button">Reset</a>
			      <button type="submit" class="btn btn-primary btn-custom-blue">Search</button>
			    </div>
			</div>
		</form>
	</div>
	<div id="hospital-list" class="table-responsive">
		<div class="add-button add-button-right">
			<a class="btn btn-primary" role="button" href="<?= base_url(); ?>sub-company-category/add">Add Sub Company Category</a>
		</div>
		<table class="table table-striped">
			<thead>
				<th>ID</th>
				<th>Name</th>
				<th>Main Category</th>
				<th>Edit</th>
				<th>Delete</th>
			</thead>
			<tbody>
				<?php if(!empty($companyCategoryList)): ?>
					<?php foreach($companyCategoryList as $company_category):?>
						<tr>
							<td><?= $company_category->id; ?></td>
							<td><?= ucfirst($company_category->name); ?></td>
							<td><?= ucfirst($company_category->main_category_name); ?></td>
							<td><a href="<?= base_url(); ?>sub-company-category/edit/<?= $company_category->id; ?>">Edit</a></td>
							<td><a href="javascript:void(0);" onclick="deleteCheck(<?= $company_category->id ?>);">Delete</a></td>
						</tr>
					<?php endforeach;?>
				<?php else: ?>
						<tr>
							<td colspan="5">No results found</td>
						</tr>
				<?php endif;?>
			</tbody>
		</table>
	</div>
	<nav>
		<ul class="pagination">
			<?php echo $links; ?>
		</ul>
	</nav>
</div>