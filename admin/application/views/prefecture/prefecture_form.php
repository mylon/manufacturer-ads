<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!isset($this->session->userdata['manufacturer_staff_logged_in'])) {
    header("location: ".base_url()."login");
    exit;
}

$id 		 = (!empty($prefectureData)) ? $prefectureData[0]->id : 0;
$name  		 = (!empty($prefectureData)) ? $prefectureData[0]->name : '';
$country_id  		 = (!empty($prefectureData)) ? $prefectureData[0]->country_id : '';

?>

<div class="container-fluid fix-height-container">
	<div class="content-title">
		<h1><?= $page_title; ?></h1>
	</div>
	<?php
		if (isset($error_message)) :
			echo "<div class='error_msg bg-danger'>";
			if (isset($error_message)) {
				echo $error_message;
			}
			echo validation_errors();
			echo "</div>";
		endif;

		if (isset($message_display)) {
			echo "<div class='message bg-success'>";
			echo $message_display;
			echo "</div>";
		}
	?>
	<form class="form-horizontal" method="post" action="<?= base_url(); ?>prefecture/save-prefecture">
		<div class="form-group">
		    <label for="name" class="col-sm-1 control-label">Prefecture</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="name" id="name" value="<?= $name; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="country_id" class="col-sm-1 control-label">Country</label>
		    <div class="col-sm-4">
		      	<select class="form-control" name="country_id" id="country_id">
		      		<option value="">Please Select</option>
		      		<?php if(!empty($countryList)): ?>
			      		<?php foreach($countryList as $k => $v):?>
			      		<?php $selected = ($country_id == $k) ? "selected" : "";?>
					  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
						<?php endforeach; ?>
					<?php endif; ?>
				</select>
		    </div>
		</div>
		<div class="form-group signup-button-center">
		    <div class="col-sm-offset-1 col-sm-4">
		      <a class="btn btn-default btn-custom-grey" href="<?= base_url();?>prefecture" role="button">Back</a>
		      <button type="submit" class="btn btn-primary btn-custom-blue"><?= $button_title; ?></button>
		    </div>
		</div>
		<input type="hidden" class="form-control" name="id" id="id" value="<?= $id; ?>">
	</form>
</div>
