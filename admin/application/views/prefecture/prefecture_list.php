<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!isset($this->session->userdata['manufacturer_staff_logged_in'])) {
    header("location: ".base_url()."login");
    exit;
}

$prefecture_name = isset($name) ? $name : "";
$country_id  = isset($country_id) ? $country_id : "";

?>
<script type="text/javascript">
	var url="<?php echo base_url();?>";
    function deleteCheck(id) {
        var r=confirm("Do you want to delete this prefecture?");
        if (r==true) {
        	window.location = url+"prefecture/delete/"+id;
        } 
        else {
        	 return false;
        }
    } 
</script>
<div class="container-fluid fix-height-container">
	<div class="content-title">
		<h1>Prefecture List</h1>
	</div>
	<?php
		if (isset($message_display)) {
			echo "<div class='message bg-success'>";
			echo $message_display;
			echo "</div>";
		}
	?>
	<div class="search-content">
		<form class="form-horizontal" method="post" action="<?= base_url(); ?>prefecture">
			<div class="form-group">
			    <label for="name" class="col-sm-1 control-label">Name</label>
			    <div class="col-sm-2">
			      <input type="text" class="form-control" name="name" id="name" value="<?= $prefecture_name; ?>">
			    </div>
			</div>
			<div class="form-group">
			    <label for="country_id" class="col-sm-1 control-label">Country</label>
			    <div class="col-sm-2">
			      	<select class="form-control" name="country_id" id="country_id">
			      		<option value="">Please Select</option>
			      		<?php if(!empty($countryList)): ?>
				      		<?php foreach($countryList as $k => $v):?>
				      		<?php $selected = ($country_id == $k) ? "selected" : "";?>
						  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
							<?php endforeach; ?>
						<?php endif; ?>
					</select>
			    </div>
			</div>
			<div class="form-group signup-button-center">
			    <div class="col-sm-offset-1 col-sm-4">
			      <a class="btn btn-default btn-custom-grey" href="<?= base_url();?>prefecture" role="button">Reset</a>
			      <button type="submit" class="btn btn-primary btn-custom-blue">Search</button>
			    </div>
			</div>
		</form>
	</div>
	<div id="hospital-list" class="table-responsive">
		<div class="add-button add-button-right">
			<a class="btn btn-primary" role="button" href="<?= base_url(); ?>prefecture/add">Add Prefecture</a>
		</div>
		<table class="table table-striped">
			<thead>
				<th>ID</th>
				<th>Name</th>
				<th>Country</th>
				<th>Edit</th>
				<th>Delete</th>
			</thead>
			<tbody>
				<?php if(!empty($PrefectureList)): ?>
					<?php foreach($PrefectureList as $prefecture):?>
						<tr>
							<td><?= $prefecture->id; ?></td>
							<td><?= ucfirst($prefecture->name); ?></td>
							<td><?= ucfirst($prefecture->country_name); ?></td>
							<td><a href="<?= base_url(); ?>prefecture/edit/<?= $prefecture->id; ?>">Edit</a></td>
							<td><a href="javascript:void(0);" onclick="deleteCheck(<?= $prefecture->id ?>);">Delete</a></td>
						</tr>
					<?php endforeach;?>
				<?php else: ?>
						<tr>
							<td colspan="4">No results found</td>
						</tr>
				<?php endif;?>
			</tbody>
		</table>
	</div>
	<nav>
		<ul class="pagination">
			<?php echo $links; ?>
		</ul>
	</nav>
</div>