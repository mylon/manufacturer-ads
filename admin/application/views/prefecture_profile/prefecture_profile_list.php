<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!isset($this->session->userdata['manufacturer_staff_logged_in'])) {
    header("location: ".base_url()."login");
    exit;
}

$prefecture_name = isset($name) ? $name : "";
$prefecture_id = isset($prefecture_id) ? $prefecture_id : "";
$country_id = isset($country_id) ? $country_id : "";
$language_id = isset($language_id) ? $language_id : "";

?>
<script type="text/javascript">
	var url="<?php echo base_url();?>";
    function deleteCheck(id) {
        var r=confirm("Do you want to delete this prefecture?");
        if (r==true) {
        	window.location = url+"prefecture-profile/delete/"+id;
        } 
        else {
        	 return false;
        }
    } 
</script>
<div class="container-fluid fix-height-container">
	<div class="content-title">
		<h1>Prefecture Profile List</h1>
	</div>
	<?php
		if (isset($message_display)) {
			echo "<div class='message bg-success'>";
			echo $message_display;
			echo "</div>";
		}
	?>
	<div class="search-content">
		<form class="form-horizontal" method="post" action="<?= base_url(); ?>prefecture-profile">
			<div class="form-group">
			    <label for="name" class="col-sm-1 control-label">Name</label>
			    <div class="col-sm-2">
			      <input type="text" class="form-control" name="name" id="name" value="<?= $prefecture_name; ?>">
			    </div>
			</div>
			<div class="form-group">
			    <label for="country_id" class="col-sm-1 control-label">Country</label>
			    <div class="col-sm-2">
			      <select class="form-control" name="country_id" id="country_id">
			      		<option value="">Please Select</option>
			      		<?php foreach($countryList as $k => $v):?>
			      		<?php $selected = ($country_id == $k) ? "selected" : "";?>
					  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
						<?php endforeach; ?>
					</select>
			    </div>
			</div>
			<div class="form-group">
			    <label for="prefecture_id" class="col-sm-1 control-label">Prefecture</label>
			    <div class="col-sm-2">
			      <select class="form-control" name="prefecture_id" id="prefecture_id">
			      		<option value="">Please Select</option>
			      		<?php foreach($prefectureList as $k => $v):?>
			      		<?php $selected = ($prefecture_id == $k) ? "selected" : "";?>
					  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
						<?php endforeach; ?>
					</select>
			    </div>
			</div>
			<div class="form-group">
			    <label for="language_id" class="col-sm-1 control-label">Language</label>
			    <div class="col-sm-2">
			      <select class="form-control" name="language_id">
			      		<option value="">Please Select</option>
			      		<?php foreach($languageList as $k => $v):?>
			      		<?php $selected = ($language_id == $k) ? "selected" : "";?>
					  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
						<?php endforeach; ?>
					</select>
			    </div>
			</div>
			<div class="form-group signup-button-center">
			    <div class="col-sm-offset-1 col-sm-4">
			      <a class="btn btn-default btn-custom-grey" href="<?= base_url();?>prefecture-profile" role="button">Reset</a>
			      <button type="submit" class="btn btn-primary btn-custom-blue">Search</button>
			    </div>
			</div>
		</form>
	</div>
	<div id="hospital-list" class="table-responsive">
		<div class="add-button add-button-right">
			<a class="btn btn-primary" role="button" href="<?= base_url(); ?>prefecture-profile/add">Add Prefecture</a>
		</div>
		<table class="table table-striped">
			<thead>
				<th>ID</th>
				<th>Prefecture</th>
				<th>Name</th>
				<th>Country</th>
				<th>Language</th>
				<th>Edit</th>
				<th>Delete</th>
			</thead>
			<tbody>
				<?php if(!empty($prefectureProfileList)): ?>
					<?php foreach($prefectureProfileList as $prefecture_profile):?>
						<tr>
							<td><?= $prefecture_profile->id; ?></td>
							<td><?= ucfirst($prefecture_profile->prefecture_name); ?></td>
							<td><?= ucfirst($prefecture_profile->name); ?></td>
							<td><?= ucfirst($prefecture_profile->country_name); ?></td>
							<td><?= ucfirst($prefecture_profile->language_name); ?></td>
							<td><a href="<?= base_url(); ?>prefecture-profile/edit/<?= $prefecture_profile->id; ?>">Edit</a></td>
							<td><a href="javascript:void(0);" onclick="deleteCheck(<?= $prefecture_profile->id ?>);">Delete</a></td>
						</tr>
					<?php endforeach;?>
				<?php else: ?>
						<tr>
							<td colspan="7">No results found</td>
						</tr>
				<?php endif;?>
			</tbody>
		</table>
	</div>
	<nav>
		<ul class="pagination">
			<?php echo $links; ?>
		</ul>
	</nav>
</div>
<script type="text/javascript">
	function capitalize(string) {
		return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
	}
	
	$('#country_id').change(function(){
	    var country_id = $(this).val();
	    $("#prefecture_id > option").remove();
	    $.ajax({
	        type: "POST",
	        url: "<?php echo site_url('adminprefectureprofile/country_list'); ?>",
	        data: {country_id: country_id},
	        dataType: 'json',
	        success:function(data){
	        	$('#prefecture_id').html('<option value="0">Please Select</option>')
	            $.each(data,function(k, v){
	                var opt = $('<option />');
	                opt.val(k);
	                opt.text(capitalize(v));
	                $('#prefecture_id').append(opt);
	            });
	        }
	    });
	});
</script>