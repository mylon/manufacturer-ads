<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!isset($this->session->userdata['manufacturer_staff_logged_in'])) {
    header("location: ".base_url()."login");
    exit;
}

$id 	= (!empty($menuData)) ? $menuData[0]->id : 0;
$menu_id  = (!empty($menuData)) ? $menuData[0]->menu_id : '';
$company_id  = (!empty($menuData)) ? $menuData[0]->company_id : '';
$name  = (!empty($menuData)) ? $menuData[0]->name : '';
$link  = (!empty($menuData)) ? $menuData[0]->link : '';
$order_menu  = (!empty($menuData)) ? $menuData[0]->order_menu : '';
$staff_company_id = $this->session->userdata['manufacturer_staff_logged_in']['staff_company'];

?>

<div class="container-fluid fix-height-container">
	<div class="content-title">
		<h1><?= $page_title; ?></h1>
	</div>
	<?php
		if (isset($error_message)) :
			echo "<div class='error_msg bg-danger'>";
			if (isset($error_message)) {
				echo $error_message;
			}
			echo validation_errors();
			echo "</div>";
		endif;

		if (isset($message_display)) {
			echo "<div class='message bg-success'>";
			echo $message_display;
			echo "</div>";
		}
	?>
	<form class="form-horizontal" method="post" action="<?= base_url(); ?>company-menu/save-menu">
		<?php if($staff_company_id > 0): ?>
			<input type="hidden" class="form-control" name="company_id" id="company_id" value="<?= $staff_company_id; ?>">
		<?php else:?>
		<div class="form-group">
		    <label for="company_id" class="col-sm-1 control-label">Company</label>
		    <div class="col-sm-4">
		      <select class="form-control" name="company_id" id="company_id">
		      		<option value="">Please Select</option>
		      		<?php foreach($companyList as $k => $v):?>
		      		<?php $selected = ($company_id == $k) ? "selected" : "";?>
				  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
					<?php endforeach; ?>
				</select>
		    </div>
		</div>
		<?php endif;?>
		<div class="form-group">
		    <label for="menu_id" class="col-sm-1 control-label">Menu Type</label>
		    <div class="col-sm-4">
		      <select class="form-control" name="menu_id" id="menu_id">
		      		<option value="">Please Select</option>
		      		<?php foreach($menuList as $k => $v):?>
		      		<?php $selected = ($menu_id == $k) ? "selected" : "";?>
				  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
					<?php endforeach; ?>
				</select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="name" class="col-sm-1 control-label">Name</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="name" id="name" value="<?= $name; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="link" class="col-sm-1 control-label">Link</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="link" id="link" value="<?= $link; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="order_menu" class="col-sm-1 control-label">Order</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="order_menu" id="order_menu" value="<?= $order_menu; ?>">
		    </div>
		</div>
		<div class="form-group signup-button-center">
		    <div class="col-sm-offset-1 col-sm-4">
		      <a class="btn btn-default btn-custom-grey" href="<?= base_url();?>company-menu" role="button">Back</a>
		      <button type="submit" class="btn btn-primary btn-custom-blue"><?= $button_title; ?></button>
		    </div>
		</div>
		<input type="hidden" class="form-control" name="id" id="id" value="<?= $id; ?>">
	</form>
</div>
