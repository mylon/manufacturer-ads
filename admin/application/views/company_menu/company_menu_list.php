<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!isset($this->session->userdata['manufacturer_staff_logged_in'])) {
    header("location: ".base_url()."login");
    exit;
}

$name = isset($name) ? $name : "";
$menu_id = isset($menu_id) ? $menu_id : "";
$language_id = isset($language_id) ? $language_id : "";
$company_id = isset($company_id) ? $company_id : "";
$staff_company_id = $this->session->userdata['manufacturer_staff_logged_in']['staff_company'];

?>
<script type="text/javascript">
	var url="<?php echo base_url();?>";
    function deleteCheck(id) {
        var r=confirm("Do you want to delete this menu?");
        if (r==true) {
        	window.location = url+"company-menu/delete/"+id;
        } 
        else {
        	 return false;
        }
    } 
</script>
<div class="container-fluid fix-height-container">
	<div class="content-title">
		<h1>Company Menu List</h1>
	</div>
	<?php
		if (isset($message_display)) {
			echo "<div class='message bg-success'>";
			echo $message_display;
			echo "</div>";
		}
	?>
	<div class="search-content">
		<form class="form-horizontal" method="post" action="<?= base_url(); ?>company-menu">
			<div class="form-group">
			    <label for="name" class="col-sm-1 control-label">Name</label>
			    <div class="col-sm-2">
			      <input type="text" class="form-control" name="name" id="name" value="<?= $name; ?>">
			    </div>
			</div>
			<?php if($staff_company_id > 0): ?>
				<input type="hidden" class="form-control" name="company_id" id="company_id" value="<?= $staff_company_id; ?>">
			<?php else:?>
			<div class="form-group">
			    <label for="company_id" class="col-sm-1 control-label">Company</label>
			    <div class="col-sm-2">
			      <select class="form-control" name="company_id" id="company_id">
			      		<option value="">Please Select</option>
			      		<?php foreach($companyList as $k => $v):?>
			      		<?php $selected = ($company_id == $k) ? "selected" : "";?>
					  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
						<?php endforeach; ?>
					</select>
			    </div>
			</div>
			<?php endif;?>
			<div class="form-group">
			    <label for="menu_id" class="col-sm-1 control-label">Menu Type</label>
			    <div class="col-sm-2">
			      <select class="form-control" name="menu_id" id="menu_id">
			      		<option value="">Please Select</option>
			      		<?php foreach($menuList as $k => $v):?>
			      		<?php $selected = ($menu_id == $k) ? "selected" : "";?>
					  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
						<?php endforeach; ?>
					</select>
			    </div>
			</div>
			<div class="form-group">
			    <label for="language_id" class="col-sm-1 control-label">Language</label>
			    <div class="col-sm-2">
			      <select class="form-control" name="language_id" id="language_id">
			      		<option value="">Please Select</option>
			      		<?php foreach($languageList as $k => $v):?>
			      		<?php $selected = ($language_id == $k) ? "selected" : "";?>
					  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
						<?php endforeach; ?>
					</select>
			    </div>
			</div>
			<div class="form-group signup-button-center">
			    <div class="col-sm-offset-1 col-sm-4">
			      <a class="btn btn-default btn-custom-grey" href="<?= base_url();?>company-menu" role="button">Reset</a>
			      <button type="submit" class="btn btn-primary btn-custom-blue">Search</button>
			    </div>
			</div>
		</form>
	</div>
	<div id="hospital-list" class="table-responsive">
		<div class="add-button add-button-right">
			<a class="btn btn-primary" role="button" href="<?= base_url(); ?>company-menu/add">Add Menu</a>
		</div>
		<table class="table table-striped">
			<thead>
				<th>ID</th>
				<th>Name</th>
				<th>Type</th>
				<th>Language</th>
				<th>Company</th>
				<th>Order</th>
				<th>Edit</th>
				<th>Delete</th>
			</thead>
			<tbody>
				<?php if(!empty($companymenuList)): ?>
					<?php foreach($companymenuList as $company_menu):?>
						<tr>
							<td><?= $company_menu->id; ?></td>
							<td><?= ucfirst($company_menu->name); ?></td>
							<td><?= ucfirst($company_menu->menu_type); ?></td>
							<td><?= ucfirst($company_menu->language_name); ?></td>
							<td><?= ucfirst($company_menu->company_name); ?></td>
							<td><?= ucfirst($company_menu->order_menu); ?></td>
							<td><a href="<?= base_url(); ?>company-menu/edit/<?= $company_menu->id; ?>">Edit</a></td>
							<td><a href="javascript:void(0);" onclick="deleteCheck(<?= $company_menu->id ?>);">Delete</a></td>
						</tr>
						
					<?php endforeach;?>
				<?php else: ?>
						<tr>
							<td colspan="8">No results found</td>
						</tr>
				<?php endif;?>
			</tbody>
		</table>
	</div>
	<nav>
		<ul class="pagination">
			<?php echo $links; ?>
		</ul>
	</nav>
</div>