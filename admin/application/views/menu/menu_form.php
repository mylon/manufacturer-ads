<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!isset($this->session->userdata['manufacturer_staff_logged_in'])) {
    header("location: ".base_url()."login");
    exit;
}

$id 	= (!empty($menuData)) ? $menuData[0]->id : 0;
$menu  = (!empty($menuData)) ? $menuData[0]->menu : '';
$language_id  = (!empty($menuData)) ? $menuData[0]->language_id : '';
$url  = (!empty($menuData)) ? $menuData[0]->url : '';

?>

<div class="container-fluid fix-height-container">
	<div class="content-title">
		<h1><?= $page_title; ?></h1>
	</div>
	<?php
		if (isset($error_message)) :
			echo "<div class='error_msg bg-danger'>";
			if (isset($error_message)) {
				echo $error_message;
			}
			echo validation_errors();
			echo "</div>";
		endif;

		if (isset($message_display)) {
			echo "<div class='message bg-success'>";
			echo $message_display;
			echo "</div>";
		}
	?>
	<form class="form-horizontal" method="post" action="<?= base_url(); ?>menu/save-menu">
		<div class="form-group">
		    <label for="language_id" class="col-sm-1 control-label">Language</label>
		    <div class="col-sm-4">
		      <select class="form-control" name="language_id" id="language_id">
		      		<option value="">Please Select</option>
		      		<?php foreach($languageList as $k => $v):?>
		      		<?php $selected = ($language_id == $k) ? "selected" : "";?>
				  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
					<?php endforeach; ?>
				</select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="menu" class="col-sm-1 control-label">Name</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="menu" id="menu" value="<?= $menu; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="url" class="col-sm-1 control-label">URL</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="url" id="url" value="<?= $url; ?>">
		    </div>
		</div>
		<div class="form-group signup-button-center">
		    <div class="col-sm-offset-1 col-sm-4">
		      <a class="btn btn-default btn-custom-grey" href="<?= base_url();?>menu" role="button">Back</a>
		      <button type="submit" class="btn btn-primary btn-custom-blue"><?= $button_title; ?></button>
		    </div>
		</div>
		<input type="hidden" class="form-control" name="id" id="id" value="<?= $id; ?>">
	</form>
</div>
