<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!isset($this->session->userdata['manufacturer_staff_logged_in'])) {
    header("location: ".base_url()."login");
    exit;
}

?>
<div class="container-fluid content-wrapper fix-height-container">
	<div class="content-title center-title">
		<h1>Test</h1>
	</div>
</div>