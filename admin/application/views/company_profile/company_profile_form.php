<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!isset($this->session->userdata['manufacturer_staff_logged_in'])) {
    header("location: ".base_url()."login");
    exit;
}

$id 				= (!empty($companyData)) ? $companyData[0]->id : 0;
$name  				= (!empty($companyData)) ? $companyData[0]->name : '';
$title  			= (!empty($companyData)) ? $companyData[0]->title : '';
$product_line  		= (!empty($companyData)) ? $companyData[0]->product_line : '';
$subject  			= (!empty($companyData)) ? $companyData[0]->subject : '';
$description  		= (!empty($companyData)) ? $companyData[0]->description : '';
$ceo  				= (!empty($companyData)) ? $companyData[0]->ceo : '';
$category_id  		= (!empty($companyData)) ? $companyData[0]->category_id : '';
$sub_category_id  	= (!empty($companyData)) ? $companyData[0]->sub_category_id : '';
$company_id  		= (!empty($companyData)) ? $companyData[0]->company_id : '';
$language_id  		= (!empty($companyData)) ? $companyData[0]->language_id : '';
$address  			= (!empty($companyData)) ? $companyData[0]->address : '';
$country_id  		= (!empty($companyData)) ? $companyData[0]->country_id : '';
$prefecture_id  	= (!empty($companyData)) ? $companyData[0]->prefecture_id : '';
$industrial_park_id = (!empty($companyData)) ? $companyData[0]->industrial_park_id : '';
$tel  				= (!empty($companyData)) ? $companyData[0]->tel : '';
$tel1  				= (!empty($companyData)) ? $companyData[0]->tel1 : '';
$tel2  				= (!empty($companyData)) ? $companyData[0]->tel2 : '';
$tel3  				= (!empty($companyData)) ? $companyData[0]->tel3 : '';
$tel4  				= (!empty($companyData)) ? $companyData[0]->tel4 : '';
$web_url  			= (!empty($companyData)) ? $companyData[0]->web_url : '';
$founded_date 		= (!empty($companyData)) ? $companyData[0]->founded_date : '';
$email		 		= (!empty($companyData)) ? $companyData[0]->email : '';
$capital			= (!empty($companyData)) ? $companyData[0]->capital : '';
$head_company		= (!empty($companyData)) ? $companyData[0]->head_company : '';
$child_company		= (!empty($companyData)) ? $companyData[0]->child_company : '';
$shareholder    	= (!empty($companyData)) ? $companyData[0]->shareholder : '';
$payroll_no     	= (!empty($companyData)) ? $companyData[0]->payroll_no : '';
$accounting_period 	= (!empty($companyData)) ? $companyData[0]->accounting_period : '';
$line_bank		 	= (!empty($companyData)) ? $companyData[0]->line_bank : '';
$certification 		= (!empty($companyData)) ? $companyData[0]->certification : '';
$main_client 		= (!empty($companyData)) ? $companyData[0]->main_client : '';
$product_meta_title 			= (!empty($companyData)) ? $companyData[0]->product_meta_title : '';
$product_meta_description 		= (!empty($companyData)) ? $companyData[0]->product_meta_description : '';
$news_meta_title 				= (!empty($companyData)) ? $companyData[0]->news_meta_title : '';
$news_meta_description 			= (!empty($companyData)) ? $companyData[0]->news_meta_description : '';
$staff_company_id   = $this->session->userdata['manufacturer_staff_logged_in']['staff_company'];

?>

<div class="container-fluid fix-height-container">
	<div class="content-title">
		<h1><?= $page_title; ?></h1>
	</div>
	<?php
		if (isset($error_message)) :
			echo "<div class='error_msg bg-danger'>";
			if (isset($error_message)) {
				echo $error_message;
			}
			echo validation_errors();
			echo "</div>";
		endif;

		if (isset($message_display)) {
			echo "<div class='message bg-success'>";
			echo $message_display;
			echo "</div>";
		}
	?>
	<form class="form-horizontal" method="post" action="<?= base_url(); ?>company-profile/save-company">
		<div class="form-group">
		    <label for="language_id" class="col-sm-1 control-label">Language</label>
		    <div class="col-sm-4">
		      <select class="form-control" name="language_id">
		      		<option value="">Please Select</option>
		      		<?php foreach($languageList as $k => $v):?>
		      		<?php $selected = ($language_id == $k) ? "selected" : "";?>
				  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
					<?php endforeach; ?>
				</select>
		    </div>
		</div>
		<?php if($staff_company_id > 0): ?>
		<input type="hidden" class="form-control" name="company_id" id="company_id" value="<?= $staff_company_id; ?>">
		<?php else:?>
		<div class="form-group">
		    <label for="company_id" class="col-sm-1 control-label">Company</label>
		    <div class="col-sm-4">
		      <select class="form-control" name="company_id" id="company_id">
		      		<option value="">Please Select</option>
		      		<?php foreach($CompanyList as $k => $v):?>
		      		<?php $selected = ($company_id == $k) ? "selected" : "";?>
				  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
					<?php endforeach; ?>
				</select>
		    </div>
		</div>
		<?php endif;?>
		<div class="form-group">
		    <label for="name" class="col-sm-1 control-label">Name</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="name" id="name" value="<?= $name; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="title" class="col-sm-1 control-label">Title</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="title" id="title" value="<?= $title; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="product_line" class="col-sm-1 control-label">Product Line</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="product_line" id="product_line" value="<?= $product_line; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="subject" class="col-sm-1 control-label">Subject</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="subject" id="subject" value="<?= $subject; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="code" class="col-sm-1 control-label">Description</label>
		    <div class="col-sm-4">
		      <textarea class="form-control" rows="3" name="description" id="description"><?= htmlspecialchars_decode($description); ?></textarea>
		    </div>
		</div>
		<div class="form-group">
		    <label for="code" class="col-sm-1 control-label">CEO</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="ceo" id="ceo" value="<?= $ceo; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="category_id" class="col-sm-1 control-label">Category</label>
		    <div class="col-sm-4">
		      <select class="form-control" name="category_id" id="category_id">
		      		<option value="">Please Select</option>
		      		<?php foreach($companyCategory as $k => $v):?>
		      		<?php $selected = ($category_id == $k) ? "selected" : "";?>
				  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
					<?php endforeach; ?>
				</select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="sub_category_id" class="col-sm-1 control-label">Sub Category</label>
		    <div class="col-sm-4">
		      <select class="form-control" name="sub_category_id[]" id="sub_category_id" multiple>
		      		<option value="">Please Select</option>
		      		<?php foreach($subcompanyCategory as $k => $v):?>
		      		<?php
		      			 $selected = "";
		      			 if ($id > 0 && !empty($subCategoryList) && !is_null($subCategoryList)) {
		      			 	foreach ($subCategoryList as $scl_id) {
		      			 		if ($scl_id == $k) {
		      			 			$selected = "selected";
		      			 			break;
		      			 		}
		      			 	}
		      			 }
		      		?>
				  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
					<?php endforeach; ?>
				</select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="code" class="col-sm-1 control-label">Address</label>
		    <div class="col-sm-4">
		      <textarea class="form-control" rows="3" name="address" id="address"><?= $address; ?></textarea>
		    </div>
		</div>
		<div class="form-group">
		    <label for="country_id" class="col-sm-1 control-label">Country</label>
		    <div class="col-sm-4">
		      <select class="form-control" name="country_id" id="country_id">
		      		<option value="">Please Select</option>
		      		<?php foreach($country as $k => $v):?>
		      		<?php $selected = ($country_id == $k) ? "selected" : "";?>
				  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
					<?php endforeach; ?>
				</select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="prefecture_id" class="col-sm-1 control-label">Prefecture</label>
		    <div class="col-sm-4">
		      <select class="form-control" name="prefecture_id" id="prefecture_id">
		      		<option value="">Please Select</option>
		      		<?php if(!empty($prefectureList)): ?>
			      		<?php foreach($prefectureList as $k => $v):?>
			      		<?php $selected = ($prefecture_id == $k) ? "selected" : "";?>
					  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
						<?php endforeach; ?>
					<?php endif; ?>
				</select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="industrial_park_id" class="col-sm-1 control-label">Industrial Park</label>
		    <div class="col-sm-4">
		      <select class="form-control" name="industrial_park_id" id="industrial_park_id">
		      		<option value="">Please Select</option>
		      		<?php if(!empty($industrialParkList)): ?>
			      		<?php foreach($industrialParkList as $k => $v):?>
			      		<?php $selected = ($industrial_park_id == $k) ? "selected" : "";?>
					  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
						<?php endforeach; ?>
					<?php endif; ?>
				</select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="tel" class="col-sm-1 control-label">Telephone</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="tel" id="tel" value="<?= $tel; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="tel1" class="col-sm-1 control-label">Telephone</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="tel1" id="tel1" value="<?= $tel1; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="tel2" class="col-sm-1 control-label">Telephone</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="tel2" id="tel2" value="<?= $tel2; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="tel3" class="col-sm-1 control-label">Telephone</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="tel3" id="tel3" value="<?= $tel3; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="tel4" class="col-sm-1 control-label">Telephone</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="tel4" id="tel4" value="<?= $tel4; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="web_url" class="col-sm-1 control-label">Web URL</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="web_url" id="web_url" value="<?= $web_url; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="founded_date" class="col-sm-1 control-label">Founded Date</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="founded_date" id="founded_date" value="<?= $founded_date; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="email" class="col-sm-1 control-label">Email</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="email" id="email" value="<?= $email; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="capital" class="col-sm-1 control-label">Capital</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="capital" id="capital" value="<?= $capital; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="head_company" class="col-sm-1 control-label">Headquater</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="head_company" id="head_company" value="<?= $head_company; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="child_company" class="col-sm-1 control-label">Branch</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="child_company" id="child_company" value="<?= $child_company; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="shareholder" class="col-sm-1 control-label">Shareholder</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="shareholder" id="shareholder" value="<?= $shareholder; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="payroll_no" class="col-sm-1 control-label">Payroll No.</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="payroll_no" id="payroll_no" value="<?= $payroll_no; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="accounting_period" class="col-sm-1 control-label">Accounting Period</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="accounting_period" id="accounting_period" value="<?= $accounting_period; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="line_bank" class="col-sm-1 control-label">Line Bank</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="line_bank" id="line_bank" value="<?= $line_bank; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="certification" class="col-sm-1 control-label">Certification</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="certification" id="certification" value="<?= $certification; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="main_client" class="col-sm-1 control-label">Main Client</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="main_client" id="main_client" value="<?= $main_client; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="product_meta_title" class="col-sm-1 control-label">Product Meta Title</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="product_meta_title" id="product_meta_title" value="<?= $product_meta_title; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="product_meta_description" class="col-sm-1 control-label">Product Meta Description</label>
		    <div class="col-sm-4">
		    	<textarea class="form-control" rows="3" name="product_meta_description" id="product_meta_description"><?= htmlspecialchars_decode($product_meta_description); ?></textarea>
		    </div>
		</div>
		<div class="form-group">
		    <label for="news_meta_title" class="col-sm-1 control-label">News Meta Title</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="news_meta_title" id="news_meta_title" value="<?= $news_meta_title; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="news_meta_description" class="col-sm-1 control-label">News Meta Description</label>
		    <div class="col-sm-4">
		    	<textarea class="form-control" rows="3" name="news_meta_description" id="news_meta_description"><?= htmlspecialchars_decode($news_meta_description); ?></textarea>
		    </div>
		</div>
		<div class="form-group signup-button-center">
		    <div class="col-sm-offset-1 col-sm-4">
		      <a class="btn btn-default btn-custom-grey" href="<?= base_url();?>company-profile" role="button">Back</a>
		      <button type="submit" class="btn btn-primary btn-custom-blue"><?= $button_title; ?></button>
		    </div>
		</div>
		<input type="hidden" class="form-control" name="staff_id" id="staff_id" value="<?= $this->session->userdata['manufacturer_staff_logged_in']['staff_id']; ?>">
		<input type="hidden" class="form-control" name="id" id="id" value="<?= $id; ?>">
	</form>
</div>
<script type="text/javascript">
	function capitalize(string) {
		return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
	}

	$('#category_id').change(function(){
	    var company_category_id = $(this).val();
	    $("#sub_category_id > option").remove();
	    $.ajax({
	        type: "POST",
	        url: "<?php echo site_url('admincompanyprofile/sub_category_list'); ?>",
	        data: {company_category_id: company_category_id},
	        dataType: 'json',
	        success:function(data){
	        	$('#sub_category_id').html('<option value="0">Please Select</option>')
	            $.each(data,function(k, v){
	                var opt = $('<option />');
	                opt.val(k);
	                opt.text(capitalize(v));
	                $('#sub_category_id').append(opt);
	            });
	        }
	    });
	});

	$('#country_id').change(function(){
	    var country_id = $(this).val();
	    $("#prefecture_id > option").remove();
	    $.ajax({
	        type: "POST",
	        url: "<?php echo site_url('admincompanyprofile/prefecture_list'); ?>",
	        data: {country_id: country_id},
	        dataType: 'json',
	        success:function(data){
	        	$('#prefecture_id').html('<option value="0">Please Select</option>')
	            $.each(data,function(k, v){
	                var opt = $('<option />');
	                opt.val(k);
	                opt.text(capitalize(v));
	                $('#prefecture_id').append(opt);
	            });
	        }
	    });
	});

	$('#prefecture_id').change(function(){
	    var country_id = $('#country_id').val();
	    var prefecture_id = $(this).val();
	    $("#industrial_park_id > option").remove();
	    $.ajax({
	        type: "POST",
	        url: "<?php echo site_url('admincompanyprofile/industrial_park_list'); ?>",
	        data: {country_id: country_id, prefecture_id: prefecture_id},
	        dataType: 'json',
	        success:function(data){
	        	$('#industrial_park_id').html('<option value="0">Please Select</option>')
	            $.each(data,function(k, v){
	                var opt = $('<option />');
	                opt.val(k);
	                opt.text(capitalize(v));
	                $('#industrial_park_id').append(opt);
	            });
	        }
	    });
	});
</script>
