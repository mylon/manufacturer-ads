<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!isset($this->session->userdata['manufacturer_staff_logged_in'])) {
    header("location: ".base_url()."login");
    exit;
}

$company_name = isset($name) ? $name : "";
$company_id = isset($company_id) ? $company_id : "";
$category_id = isset($category_id) ? $category_id : "";
$sub_category_id = isset($sub_category_id) ? $sub_category_id : "";
$country_id = isset($country_id) ? $country_id : "";
$prefecture_id = isset($prefecture_id) ? $prefecture_id : "";
$industrial_park_id = isset($industrial_park_id) ? $industrial_park_id : "";
$language_id = isset($language_id) ? $language_id : "";
$staff_id = isset($created_by) ? $created_by : "";
$staff_company_id = $this->session->userdata['manufacturer_staff_logged_in']['staff_company'];

?>
<script type="text/javascript">
	var url="<?php echo base_url();?>";
    function deleteCheck(id) {
        var r=confirm("Do you want to delete this company?");
        if (r==true) {
        	window.location = url+"company-profile/delete/"+id;
        } 
        else {
        	 return false;
        }
    } 
</script>
<div class="container-fluid fix-height-container">
	<div class="content-title">
		<h1>Company Profile List</h1>
	</div>
	<?php
		if (isset($message_display)) {
			echo "<div class='message bg-success'>";
			echo $message_display;
			echo "</div>";
		}
	?>

	<div class="search-content">
		<form class="form-horizontal" method="post" action="<?= base_url(); ?>company-profile">
			<div class="form-group">
			    <label for="name" class="col-sm-1 control-label">Profile Name</label>
			    <div class="col-sm-2">
			      <input type="text" class="form-control" name="name" id="name" value="<?= $company_name; ?>">
			    </div>
			</div>
			<div class="form-group">
			    <label for="language_id" class="col-sm-1 control-label">Language</label>
			    <div class="col-sm-2">
			      <select class="form-control" name="language_id">
			      		<option value="">Please Select</option>
			      		<?php foreach($languageList as $k => $v):?>
			      		<?php $selected = ($language_id == $k) ? "selected" : "";?>
					  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
						<?php endforeach; ?>
					</select>
			    </div>
			</div>
			<?php if($staff_company_id > 0): ?>
				<input type="hidden" class="form-control" name="company_id" id="company_id" value="<?= $staff_company_id; ?>">
			<?php else:?>
			<div class="form-group">
			    <label for="company_id" class="col-sm-1 control-label">Company</label>
			    <div class="col-sm-2">
			      <select class="form-control" name="company_id" id="company_id">
			      		<option value="">Please Select</option>
			      		<?php foreach($CompanyList as $k => $v):?>
			      		<?php $selected = ($company_id == $k) ? "selected" : "";?>
					  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
						<?php endforeach; ?>
					</select>
			    </div>
			</div>
			<?php endif;?>
			<div class="form-group">
			    <label for="category_id" class="col-sm-1 control-label">Category</label>
			    <div class="col-sm-2">
			      <select class="form-control" name="category_id" id="category_id">
			      		<option value="">Please Select</option>
			      		<?php foreach($companyCategory as $k => $v):?>
			      		<?php $selected = ($category_id == $k) ? "selected" : "";?>
					  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
						<?php endforeach; ?>
					</select>
			    </div>
			</div>
			<div class="form-group">
			    <label for="sub_category_id" class="col-sm-1 control-label">Sub Category</label>
			    <div class="col-sm-2">
			      <select class="form-control" name="sub_category_id" id="sub_category_id">
			      		<option value="">Please Select</option>
			      		<?php foreach($subcompanyCategory as $k => $v):?>
			      		<?php $selected = ($sub_category_id == $k) ? "selected" : "";?>
					  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
						<?php endforeach; ?>
					</select>
			    </div>
			</div>
			<div class="form-group">
			    <label for="country_id" class="col-sm-1 control-label">Country</label>
			    <div class="col-sm-2">
			      <select class="form-control" name="country_id" id="country_id">
			      		<option value="">Please Select</option>
			      		<?php foreach($country as $k => $v):?>
			      		<?php $selected = ($country_id == $k) ? "selected" : "";?>
					  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
						<?php endforeach; ?>
					</select>
			    </div>
			</div>
			<div class="form-group">
			    <label for="prefecture_id" class="col-sm-1 control-label">Prefecture</label>
			    <div class="col-sm-2">
			      <select class="form-control" name="prefecture_id" id="prefecture_id">
			      		<option value="">Please Select</option>
			      		<?php if(!empty($prefectureList)): ?>
				      		<?php foreach($prefectureList as $k => $v):?>
				      		<?php $selected = ($prefecture_id == $k) ? "selected" : "";?>
						  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
							<?php endforeach; ?>
						<?php endif; ?>
					</select>
			    </div>
			</div>
			<div class="form-group">
			    <label for="industrial_park_id" class="col-sm-1 control-label">Industrial Park</label>
			    <div class="col-sm-2">
			      <select class="form-control" name="industrial_park_id" id="industrial_park_id">
			      		<option value="">Please Select</option>
			      		<?php if(!empty($industrialParkList)): ?>
				      		<?php foreach($industrialParkList as $k => $v):?>
				      		<?php $selected = ($industrial_park_id == $k) ? "selected" : "";?>
						  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
							<?php endforeach; ?>
						<?php endif; ?>
					</select>
			    </div>
			</div>
			<div class="form-group">
			    <label for="staff_id" class="col-sm-1 control-label">Created By</label>
			    <div class="col-sm-2">
			      <select class="form-control" name="staff_id">
			      		<option value="">Please Select</option>
			      		<?php foreach($staffList as $k => $v):?>
			      		<?php $selected = ($staff_id == $k) ? "selected" : "";?>
					  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
						<?php endforeach; ?>
					</select>
			    </div>
			</div>
			<div class="form-group signup-button-center">
			    <div class="col-sm-offset-1 col-sm-4">
			      <a class="btn btn-default btn-custom-grey" href="<?= base_url();?>company-profile" role="button">Reset</a>
			      <button type="submit" class="btn btn-primary btn-custom-blue">Search</button>
			    </div>
			</div>
		</form>
	</div>

	<div id="hospital-list" class="table-responsive">
		<div class="add-button add-button-right">
			<a class="btn btn-primary" role="button" href="<?= base_url(); ?>company-profile/add">Add Compnany</a>
		</div>
		<table class="table table-striped">
			<thead>
				<th>ID</th>
				<th>Company Name</th>
				<th>Profile Name</th>
				<th>Category</th>
				<th>Country</th>
				<th>Prefecture</th>
				<th>Industrial Park</th>
				<th>Language</th>
				<th>Created By</th>
				<th>Edited By</th>
				<th>Edit</th>
				<th>Delete</th>
			</thead>
			<tbody>
				<?php if(!empty($CompanyProfileList)): ?>
					<?php foreach($CompanyProfileList as $company_profile):?>
						<tr>
							<td><?= $company_profile->id; ?></td>
							<td><?= ucfirst($company_profile->company_name); ?></td>
							<td><?= ucfirst($company_profile->name); ?></td>
							<td><?= ucfirst($company_profile->category_name); ?></td>
							<td><?= ucfirst($company_profile->country_name); ?></td>
							<td><?= ucfirst($company_profile->prefecture_name); ?></td>
							<td><?= ucfirst($company_profile->industrial_park_name); ?></td>
							<td><?= ucfirst($company_profile->language_name); ?></td>
							<td><?= ucfirst($company_profile->created_by); ?></td>
							<td><?= ucfirst($company_profile->edited_by); ?></td>
							<td><a href="<?= base_url(); ?>company-profile/edit/<?= $company_profile->id; ?>">Edit</a></td>
							<td><a href="javascript:void(0);" onclick="deleteCheck(<?= $company_profile->id ?>);">Delete</a></td>
						</tr>
						
					<?php endforeach;?>
				<?php else: ?>
						<tr>
							<td colspan="9">No results found</td>
						</tr>
				<?php endif;?>
			</tbody>
		</table>
	</div>
	<nav>
		<ul class="pagination">
			<?php echo $links; ?>
		</ul>
	</nav>
</div>
<script type="text/javascript">
	function capitalize(string) {
		return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
	}
	
	$('#category_id').change(function(){
	    var company_category_id = $(this).val();
	    $("#sub_category_id > option").remove();
	    $.ajax({
	        type: "POST",
	        url: "<?php echo site_url('admincompanyprofile/sub_category_list'); ?>",
	        data: {company_category_id: company_category_id},
	        dataType: 'json',
	        success:function(data){
	        	$('#sub_category_id').html('<option value="0">Please Select</option>')
	            $.each(data,function(k, v){
	                var opt = $('<option />');
	                opt.val(k);
	                opt.text(capitalize(v));
	                $('#sub_category_id').append(opt);
	            });
	        }
	    });
	});

	$('#country_id').change(function(){
	    var country_id = $(this).val();
	    $("#prefecture_id > option").remove();
	    $.ajax({
	        type: "POST",
	        url: "<?php echo site_url('admincompanyprofile/prefecture_list'); ?>",
	        data: {country_id: country_id},
	        dataType: 'json',
	        success:function(data){
	        	$('#prefecture_id').html('<option value="0">Please Select</option>')
	            $.each(data,function(k, v){
	                var opt = $('<option />');
	                opt.val(k);
	                opt.text(capitalize(v));
	                $('#prefecture_id').append(opt);
	            });
	        }
	    });
	});

	$('#prefecture_id').change(function(){
	    var prefecture_id = $(this).val();
	    $("#industrial_park_id > option").remove();
	    $.ajax({
	        type: "POST",
	        url: "<?php echo site_url('admincompanyprofile/industrial_park_list'); ?>",
	        data: {prefecture_id: prefecture_id},
	        dataType: 'json',
	        success:function(data){
	        	$('#industrial_park_id').html('<option value="0">Please Select</option>')
	            $.each(data,function(k, v){
	                var opt = $('<option />');
	                opt.val(k);
	                opt.text(capitalize(v));
	                $('#industrial_park_id').append(opt);
	            });
	        }
	    });
	});
</script>
