<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Manufacturer Site</title>
    
    
    <link href="<?php echo base_url('assets/css/reset.css');?>" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Trirong" rel="stylesheet">

    <!-- Bootstrap -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/bootstrap-datetimepicker.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/jquery-ui.min.css');?>" rel="stylesheet">
    <?php $randCss = rand(10000, 99999); ?>
    <link href="<?php echo base_url('assets/css/admin.css').'?'.$randCss;?>" rel="stylesheet">

    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.1.0.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-ui.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>

	
	<?php
		if(isset($this->js))
		{
			foreach ($this->js as $js)
			{
				echo '<script type="text/javascript" src="'.base_url.'views/'.$js.'"></script>';
			}
		}
	?>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php 
        if(!empty($map['js'])){
            echo $map['js'];
        }
    ?>
    

  </head>
  <body>
  	<?php //Session::init(); ?>
    
    <div class="row">
       <?php if (isset($this->session->userdata['manufacturer_staff_logged_in'])) : ?>
      <!-- Header -->
      <div class="col-md-2 col-xs-2 left-menu">
         <header>
            <ul class="navbar nav-pills nav-stacked">
              <!--<li><a href="<?= base_url();?>dashboard">Your Profile</a></li>-->
              <!--<li><a href="<?= base_url(); ?>company-article">Articles</a></li>
              <li><a href="<?= base_url(); ?>article-category">Article Category</a></li>-->
              <li><a href="<?= base_url(); ?>company">Company</a></li>
              <li><a href="<?= base_url(); ?>company-profile">Company Profile</a></li>
              <?php if (isset($this->session->userdata['manufacturer_staff_logged_in']) 
                    && ($this->session->userdata['manufacturer_staff_logged_in']['staff_type'] == "superadmin") ) : ?>
              <li><a href="<?= base_url(); ?>company-category">Company Category</a></li>
              <li><a href="<?= base_url(); ?>company-category-profile">Company Category Profile</a></li>
              <hr/>
              <li><a href="<?= base_url(); ?>sub-company-category">Sub Company Category</a></li>
              <li><a href="<?= base_url(); ?>sub-company-category-profile">Sub Company Category Profile</a></li>
              <hr/>
              <li><a href="<?= base_url();?>menu">Menu</a></li>
              <?php endif;?>
              <li><a href="<?= base_url();?>company-menu">Company Menu</a></li>
              <hr/>
              <li><a href="<?= base_url();?>company-news">News</a></li>
              <hr/>
              <li><a href="<?= base_url(); ?>news-category">News Category</a></li>
              <li><a href="<?= base_url(); ?>news-category-profile">News Category Profile</a></li>
              <hr/>
              <li><a href="<?= base_url();?>company-products">Products</a></li>
              <li><a href="<?= base_url();?>company-products-profile">Products Profile</a></li>
              <hr/>
              <li><a href="<?= base_url();?>products-category">Products Category</a></li>
              <li><a href="<?= base_url();?>products-category-profile">Products Category Profile</a></li>
              <hr/>
              <?php if (isset($this->session->userdata['manufacturer_staff_logged_in']) 
                    && ($this->session->userdata['manufacturer_staff_logged_in']['staff_type'] == "superadmin") ) : ?>
              <li><a href="<?= base_url();?>prefecture">Prefecture</a></li>
              <li><a href="<?= base_url();?>prefecture-profile">Prefecture Profile</a></li>
              <hr/>
              <li><a href="<?= base_url();?>industrial-park">Industrial Park</a></li>
              <li><a href="<?= base_url();?>industrial-park-profile">Industrial Park Profile</a></li>
              <hr/>
              <li><a href="<?= base_url();?>language">Language</a></li>
              <li><a href="<?= base_url();?>country">Country</a></li>
              <li><a href="<?= base_url();?>user">User</a></li>
              <li><a href="<?= base_url();?>staff">Staff</a></li>
              <?php endif;?>
              <li><a href="<?= base_url(); ?>logout">Log out</a></li>
            </ul>
        </header>
      </div>
      <!-- Header -->
      <div class="col-md-10 col-xs-10">
      <?php else:?>
       <div class="col-md-12 col-xs-12">
      <?php endif;?>
      

	
