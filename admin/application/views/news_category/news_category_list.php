<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!isset($this->session->userdata['manufacturer_staff_logged_in'])) {
    header("location: ".base_url()."login");
    exit;
}

$category_name = isset($name) ? $name : "";
$company_id = isset($company_id) ? $company_id : "";
$staff_company_id = $this->session->userdata['manufacturer_staff_logged_in']['staff_company'];

?>
<script type="text/javascript">
	var url="<?php echo base_url();?>";
    function deleteCheck(id) {
        var r=confirm("Do you want to delete this category?");
        if (r==true) {
        	window.location = url+"news-category/delete/"+id;
        } 
        else {
        	 return false;
        }
    } 
</script>
<div class="container-fluid fix-height-container">
	<div class="content-title">
		<h1>News Category List</h1>
	</div>
	<?php
		if (isset($message_display)) {
			echo "<div class='message bg-success'>";
			echo $message_display;
			echo "</div>";
		}
	?>
	<div class="search-content">
		<form class="form-horizontal" method="post" action="<?= base_url(); ?>news-category">
			<div class="form-group">
			    <label for="name" class="col-sm-1 control-label">Name</label>
			    <div class="col-sm-2">
			      <input type="text" class="form-control" name="name" id="name" value="<?= $category_name; ?>">
			    </div>
			</div>
			<?php if($staff_company_id > 0): ?>
				<input type="hidden" class="form-control" name="company_id" id="company_id" value="<?= $staff_company_id; ?>">
			<?php else:?>
			<div class="form-group">
			    <label for="category_id" class="col-sm-1 control-label">Company</label>
			    <div class="col-sm-2">
			      <select class="form-control" name="company_id">
			      		<option value="">Please Select</option>
			      		<?php foreach($companyList as $k => $v):?>
			      		<?php $selected = ($company_id == $k) ? "selected" : "";?>
					  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
						<?php endforeach; ?>
					</select>
			    </div>
			</div>
			<?php endif;?>
			<div class="form-group signup-button-center">
			    <div class="col-sm-offset-1 col-sm-4">
			      <a class="btn btn-default btn-custom-grey" href="<?= base_url();?>news-category" role="button">Reset</a>
			      <button type="submit" class="btn btn-primary btn-custom-blue">Search</button>
			    </div>
			</div>
		</form>
	</div>
	<div id="hospital-list" class="table-responsive">
		<div class="add-button add-button-right">
			<a class="btn btn-primary" role="button" href="<?= base_url(); ?>news-category/add">Add Category</a>
		</div>
		<table class="table table-striped">
			<thead>
				<th>ID</th>
				<th>Name</th>
				<th>Company</th>
				<th>Edit</th>
				<th>Delete</th>
			</thead>
			<tbody>
				<?php if(!empty($CategoryList)): ?>
					<?php foreach($CategoryList as $category):?>
						<tr>
							<td><?= $category->id; ?></td>
							<td><?= ucfirst($category->name); ?></td>
							<td><?= ucfirst($category->company_name); ?></td>
							<td><a href="<?= base_url(); ?>news-category/edit/<?= $category->id; ?>">Edit</a></td>
							<td><a href="javascript:void(0);" onclick="deleteCheck(<?= $category->id ?>);">Delete</a></td>
						</tr>
						
					<?php endforeach;?>
				<?php else: ?>
						<tr>
							<td colspan="5">No results found</td>
						</tr>
				<?php endif;?>
			</tbody>
		</table>
	</div>
	<nav>
		<ul class="pagination">
			<?php echo $links; ?>
		</ul>
	</nav>
</div>