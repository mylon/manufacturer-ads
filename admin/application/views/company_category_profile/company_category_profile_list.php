<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!isset($this->session->userdata['manufacturer_staff_logged_in'])) {
    header("location: ".base_url()."login");
    exit;
}

$category_name = isset($name) ? $name : "";
$company_category_id = isset($company_category_id) ? $company_category_id : "";
$language_id = isset($language_id) ? $language_id : "";

?>
<script type="text/javascript">
	var url="<?php echo base_url();?>";
    function deleteCheck(id) {
        var r=confirm("Do you want to delete this company category?");
        if (r==true) {
        	window.location = url+"company-category-profile/delete/"+id;
        } 
        else {
        	 return false;
        }
    } 
</script>
<div class="container-fluid fix-height-container">
	<div class="content-title">
		<h1>Company Category Profile List</h1>
	</div>
	<?php
		if (isset($message_display)) {
			echo "<div class='message bg-success'>";
			echo $message_display;
			echo "</div>";
		}
	?>
	<div class="search-content">
		<form class="form-horizontal" method="post" action="<?= base_url(); ?>company-category-profile">
			<div class="form-group">
			    <label for="name" class="col-sm-1 control-label">Name</label>
			    <div class="col-sm-2">
			      <input type="text" class="form-control" name="name" id="name" value="<?= $category_name; ?>">
			    </div>
			</div>
			<div class="form-group">
			    <label for="company_category_id" class="col-sm-1 control-label">Category</label>
			    <div class="col-sm-2">
			      <select class="form-control" name="company_category_id" id="company_category_id">
			      		<option value="">Please Select</option>
			      		<?php foreach($companyCategoryList as $k => $v):?>
			      		<?php $selected = ($company_category_id == $k) ? "selected" : "";?>
					  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
						<?php endforeach; ?>
					</select>
			    </div>
			</div>
			<div class="form-group">
			    <label for="language_id" class="col-sm-1 control-label">Language</label>
			    <div class="col-sm-2">
			      <select class="form-control" name="language_id" id="language_id">
			      		<option value="">Please Select</option>
			      		<?php foreach($languageList as $k => $v):?>
			      		<?php $selected = ($language_id == $k) ? "selected" : "";?>
					  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
						<?php endforeach; ?>
					</select>
			    </div>
			</div>
			<div class="form-group signup-button-center">
			    <div class="col-sm-offset-1 col-sm-4">
			      <a class="btn btn-default btn-custom-grey" href="<?= base_url();?>company-category-profile" role="button">Reset</a>
			      <button type="submit" class="btn btn-primary btn-custom-blue">Search</button>
			    </div>
			</div>
		</form>
	</div>
	<div id="hospital-list" class="table-responsive">
		<div class="add-button add-button-right">
			<a class="btn btn-primary" role="button" href="<?= base_url(); ?>company-category-profile/add">Add Company Category</a>
		</div>
		<table class="table table-striped">
			<thead>
				<th>ID</th>
				<th>Name</th>
				<th>Category</th>
				<th>Lanugae</th>
				<th>Edit</th>
				<th>Delete</th>
			</thead>
			<tbody>
				<?php if(!empty($companyCategoryProfileList)): ?>
					<?php foreach($companyCategoryProfileList as $company_category_profile):?>
						<tr>
							<td><?= $company_category_profile->id; ?></td>
							<td><?= ucfirst($company_category_profile->name); ?></td>
							<td><?= ucfirst($company_category_profile->category_name); ?></td>
							<td><?= ucfirst($company_category_profile->language_name); ?></td>
							<td><a href="<?= base_url(); ?>company-category-profile/edit/<?= $company_category_profile->id; ?>">Edit</a></td>
							<td><a href="javascript:void(0);" onclick="deleteCheck(<?= $company_category_profile->id ?>);">Delete</a></td>
						</tr>
					<?php endforeach;?>
				<?php else: ?>
						<tr>
							<td colspan="7">No results found</td>
						</tr>
				<?php endif;?>
			</tbody>
		</table>
	</div>
	<nav>
		<ul class="pagination">
			<?php echo $links; ?>
		</ul>
	</nav>
</div>