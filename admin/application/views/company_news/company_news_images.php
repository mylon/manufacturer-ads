<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!isset($this->session->userdata['manufacturer_staff_logged_in'])) {
    header("location: ".base_url()."login");
    exit;
}

$logo_path = 'https://s3-ap-southeast-1.amazonaws.com/manufacturer-bucket/company-news/';

?>

<script type="text/javascript">
    var url="<?php echo base_url();?>";
    function deleteCheck(profile_id, imageUrl) {
        var r=confirm("Do you want to delete this image?");
        if (r==true) {
            window.location = url+"company-news/delete-image/"+profile_id+"/"+imageUrl;
        } 
        else {
             return false;
        }
    } 
</script>

<div class="container-fluid admin-container image-gallery">
	<h1>News Image</h1>
	<div class="row">
        <div class="col-lg-12">
            <div class="col-lg-12">
                <p><?php echo $this->session->flashdata('statusMsg'); ?></p>
            </div>
            <form class="form-horizontal" enctype="multipart/form-data" action="<?php echo base_url(); ?>company-news/save-image/<?php echo $id; ?>" method="post">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="uploadImage">Choose Imgaes</label>
                    <div class="col-sm-8">
                        <input type="file" id="uploadImage" class="form-control" name="userFiles"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="uploadImage"></label>
                    <div class="col-sm-2">
                         <input class="btn btn-primary" type="submit" name="fileSubmit" value="UPLOAD"/>
                    </div>
                </div>
            </div>
            </form>
        </div>
        <div class="col-lg-12">
            <div class="row">
                <ul class="gallery clearfix">
                    <?php if(!empty($files)): foreach($files as $file): if(!empty($file->images)):?>
                    <li class="item">
                        <img src="<?php echo $logo_path.$file->images; ?>" alt="" width="200">
                        <button type="button" class="btn btn-danger" onclick="deleteCheck('<?= $id ?>', '<?= $file->images ?>');">Delete</button>
                    </li>
                    <?php endif; endforeach; else: ?>
                    <p>File(s) not found.....</p>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
</div>