<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!isset($this->session->userdata['manufacturer_staff_logged_in'])) {
    header("location: ".base_url()."login");
    exit;
}

$news_title = isset($title) ? $title : "";
$start_date = isset($start_date) ? $start_date : "";
$end_date = isset($end_date) ? $end_date : "";
$staff_id = isset($created_by) ? $created_by : "";
$count_news = isset($NewsList) ? count($NewsList) : 0;
$company_id = isset($company_id) ? $company_id : "";
$category_id = isset($category_id) ? $category_id : "";
$language_id = isset($language_id) ? $language_id : "";
$staff_company_id = $this->session->userdata['manufacturer_staff_logged_in']['staff_company'];

?>
<script type="text/javascript">
	var url="<?php echo base_url();?>";
    function deleteCheck(id) {
        var r=confirm("Do you want to delete this product?");
        if (r==true) {
        	window.location = url+"company-news/delete/"+id;
        } 
        else {
        	 return false;
        }
    } 
</script>
<div class="container-fluid fix-height-container">
	<div class="content-title">
		<h1>News List (<?= $count_news;?>)</h1>
	</div>
	<?php
		if (isset($message_display)) {
			echo "<div class='message bg-success'>";
			echo $message_display;
			echo "</div>";
		}
	?>
	<div class="search-content">
		<form class="form-horizontal" method="post" action="<?= base_url(); ?>company-news">
			<div class="form-group">
			    <label for="title" class="col-sm-1 control-label">Title</label>
			    <div class="col-sm-2">
			      <input type="text" class="form-control" name="title" id="title" value="<?= $news_title; ?>">
			    </div>
			</div>
			<div class="form-group">
			    <label for="title" class="col-sm-1 control-label">Created</label>
			    <div class="col-sm-2">
			      <input type="text" class="form-control" name="start_date" id="start_date" value="<?= $start_date; ?>"> (YYYY-MM-DD)
			    </div>
			</div>
			<div class="form-group">
			    <label for="title" class="col-sm-1 control-label">Updated</label>
			    <div class="col-sm-2">
			      <input type="text" class="form-control" name="end_date" id="end_date" value="<?= $end_date; ?>"> (YYYY-MM-DD)
			    </div>
			</div>
			
			<div class="form-group">
			    <label for="staff_id" class="col-sm-1 control-label">Created By</label>
			    <div class="col-sm-2">
			      <select class="form-control" name="staff_id">
			      		<option value="">Please Select</option>
			      		<?php foreach($staffList as $k => $v):?>
			      		<?php $selected = ($staff_id == $k) ? "selected" : "";?>
					  		<option value="<?= $k; ?>" <?= $selected?>><?= $v; ?></option>
						<?php endforeach; ?>
					</select>
			    </div>
			</div>
			<?php if($staff_company_id > 0): ?>
				<input type="hidden" class="form-control" name="company_id" id="company_id" value="<?= $staff_company_id; ?>">
			<?php else:?>
			<div class="form-group">
		    	<label for="company_id" class="col-sm-1 control-label">Company</label>
			    <div class="col-sm-2">
			      <select class="form-control" name="company_id">
			      		<option value="">Please Select</option>
			      		<?php foreach($companyList as $k => $v):?>
			      		<?php $selected = ($company_id == $k) ? "selected" : "";?>
					  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
						<?php endforeach; ?>
					</select>
			    </div>
			</div>
			<?php endif;?>
			<div class="form-group">
			    <label for="category_id" class="col-sm-1 control-label">Category</label>
			    <div class="col-sm-2">
			      <select class="form-control" name="category_id">
			      		<option value="">Please Select</option>
			      		<?php foreach($categoryList as $k => $v):?>
			      		<?php $selected = ($category_id == $k) ? "selected" : "";?>
					  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
						<?php endforeach; ?>
					</select>
			    </div>
			</div>
			<div class="form-group">
			    <label for="language_id" class="col-sm-1 control-label">Langugae</label>
			    <div class="col-sm-2">
			      <select class="form-control" name="language_id">
			      		<option value="">Please Select</option>
			      		<?php foreach($languageList as $k => $v):?>
			      		<?php $selected = ($language_id == $k) ? "selected" : "";?>
					  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
						<?php endforeach; ?>
					</select>
			    </div>
			</div>
			<div class="form-group signup-button-center">
			    <div class="col-sm-offset-1 col-sm-4">
			      <a class="btn btn-default btn-custom-grey" href="<?= base_url();?>company-news" role="button">Reset</a>
			      <button type="submit" class="btn btn-primary btn-custom-blue">Search</button>
			    </div>
			</div>
		</form>
	</div>

	<div id="hospital-list" class="table-responsive" style="overflow-x:auto;">
		<div class="add-button add-button-right">
			<a class="btn btn-primary" role="button" href="<?= base_url(); ?>company-news/add">Add News</a>
		</div>
		<table class="table table-striped">
			<thead>
				<th>ID</th>
				<th>Title</th>
				<th>Author Images</th>
				<th>News Images</th>
				<th>Description Images</th>
				<th>Company</th>
				<th>Language</th>
				<th>Category</th>
				<th>Created By</th>
				<th>Edited By</th>
				<th>Created</th>
				<th>Updated</th>
				<th>Edit</th>
				<th>Delete</th>
			</thead>
			<tbody>
				<?php if(!empty($NewsList)): ?>
					<?php foreach($NewsList as $news):?>
						<tr>
							<td><?= $news->id; ?></td>
							<td><?= ucfirst($news->title); ?></td>
							<td><a href="<?= base_url(); ?>company-news/upload-author/<?= $news->id; ?>">Upload</a></td>
							<td><a href="<?= base_url(); ?>company-news/upload-image/<?= $news->id; ?>">Upload</a></td>
							<td><a href="<?= base_url(); ?>company-news/description-upload-image/<?= $news->id; ?>">Upload</a></td>
							<td><?= ucfirst($news->company_name); ?></td>
							<td><?= ucfirst($news->language_name); ?></td>
							<td><?= ucfirst($news->category_name); ?></td>
							<td><?= ucfirst($news->created_by); ?></td>
							<td><?= ucfirst($news->edited_by); ?></td>
							<td><?= date('Y-m-d', strtotime($news->created_date)); ?></td>
							<td><?php if(!empty($news->updated_date)) : echo date('Y-m-d', strtotime($news->updated_date)); else: echo '-'; endif; ?></td>
							<td><a href="<?= base_url(); ?>company-news/edit/<?= $news->id; ?>">Edit</a></td>
							<td><a href="javascript:void(0);" onclick="deleteCheck(<?= $news->id ?>);">Delete</a></td>
						</tr>
						
					<?php endforeach;?>
				<?php else: ?>
						<tr>
							<td colspan="12">No results found</td>
						</tr>
				<?php endif;?>
			</tbody>
		</table>
	</div>
	<nav>
		<ul class="pagination">
			<?php echo $links; ?>
		</ul>
	</nav>
</div>