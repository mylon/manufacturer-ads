<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!isset($this->session->userdata['manufacturer_staff_logged_in'])) {
    header("location: ".base_url()."login");
    exit;
}

$id 				= (!empty($newsData)) ? $newsData[0]->id : 0;
$title  			= (!empty($newsData)) ? $newsData[0]->title : '';
$description  		= (!empty($newsData)) ? $newsData[0]->description : '';
$author_name  		= (!empty($newsData)) ? $newsData[0]->author_name : '';
$author_job	  		= (!empty($newsData)) ? $newsData[0]->author_job : '';
$company_id  		= (!empty($newsData)) ? $newsData[0]->company_id : '';
$category_id  		= (!empty($newsData)) ? $newsData[0]->category_id : '';
$language_id  		= (!empty($newsData)) ? $newsData[0]->language_id : '';
$meta_title  		= (!empty($newsData)) ? $newsData[0]->meta_title : '';
$meta_description  	= (!empty($newsData)) ? $newsData[0]->meta_description : '';
$staff_company_id = $this->session->userdata['manufacturer_staff_logged_in']['staff_company'];

?>

<div class="container-fluid fix-height-container">
	<div class="content-title">
		<h1><?= $page_title; ?></h1>
	</div>
	<?php
		if (isset($error_message)) :
			echo "<div class='error_msg bg-danger'>";
			if (isset($error_message)) {
				echo $error_message;
			}
			echo validation_errors();
			echo "</div>";
		endif;

		if (isset($message_display)) {
			echo "<div class='message bg-success'>";
			echo $message_display;
			echo "</div>";
		}
	?>
	<form class="form-horizontal" method="post" action="<?= base_url(); ?>company-news/save-company-news">
		<div class="form-group">
		    <label for="name" class="col-sm-1 control-label">Author Name</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="author_name" id="author_name" value="<?= $author_name; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="name" class="col-sm-1 control-label">Author Job</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="author_job" id="author_job" value="<?= $author_job; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="name" class="col-sm-1 control-label">News Title</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="title" id="title" value="<?= $title; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="code" class="col-sm-1 control-label">Description</label>
		    <div class="col-sm-4">
		      <textarea class="form-control" rows="3" name="description" id="description"><?= htmlspecialchars_decode($description); ?></textarea>
		    </div>
		</div>
		<div class="form-group">
		    <label for="meta_title" class="col-sm-1 control-label">Meta Title</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="meta_title" id="meta_title" value="<?= $meta_title; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="meta_description" class="col-sm-1 control-label">Meta Description</label>
		    <div class="col-sm-4">
		      <textarea class="form-control" rows="3" name="meta_description" id="meta_description"><?= htmlspecialchars_decode($meta_description); ?></textarea>
		    </div>
		</div>
		<?php if($staff_company_id > 0): ?>
			<input type="hidden" class="form-control" name="company_id" id="company_id" value="<?= $staff_company_id; ?>">
		<?php else:?>
		<div class="form-group">
		    <label for="category_id" class="col-sm-1 control-label">Company</label>
		    <div class="col-sm-4">
		      <select class="form-control" name="company_id">
		      		<option value="">Please Select</option>
		      		<?php foreach($companyList as $k => $v):?>
		      		<?php $selected = ($company_id == $k) ? "selected" : "";?>
				  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
					<?php endforeach; ?>
				</select>
		    </div>
		</div>
		<?php endif;?>
		<div class="form-group">
		    <label for="category_id" class="col-sm-1 control-label">Category</label>
		    <div class="col-sm-4">
		      <select class="form-control" name="category_id">
		      		<option value="">Please Select</option>
		      		<?php foreach($categoryList as $k => $v):?>
		      		<?php $selected = ($category_id == $k) ? "selected" : "";?>
				  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
					<?php endforeach; ?>
				</select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="language_id" class="col-sm-1 control-label">Langugae</label>
		    <div class="col-sm-4">
		      <select class="form-control" name="language_id">
		      		<option value="">Please Select</option>
		      		<?php foreach($languageList as $k => $v):?>
		      		<?php $selected = ($language_id == $k) ? "selected" : "";?>
				  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
					<?php endforeach; ?>
				</select>
		    </div>
		</div>
		<div class="form-group signup-button-center">
		    <div class="col-sm-offset-1 col-sm-4">
		      <a class="btn btn-default btn-custom-grey" href="<?= base_url();?>company-news" role="button">Back</a>
		      <button type="submit" class="btn btn-primary btn-custom-blue"><?= $button_title; ?></button>
		    </div>
		</div>
		<input type="hidden" class="form-control" name="staff_id" id="staff_id" value="<?= $this->session->userdata['manufacturer_staff_logged_in']['staff_id']; ?>">
		<input type="hidden" class="form-control" name="id" id="id" value="<?= $id; ?>">
	</form>
</div>