<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!isset($this->session->userdata['manufacturer_staff_logged_in'])) {
    header("location: ".base_url()."login");
    exit;
}

$id 	= (!empty($userData)) ? $userData[0]->id : 0;
$name  = (!empty($userData)) ? $userData[0]->name : '';
$email  = (!empty($userData)) ? $userData[0]->email : '';
$company_name  = (!empty($userData)) ? $userData[0]->company_name : '';

?>

<div class="container-fluid fix-height-container">
	<div class="content-title">
		<h1><?= $page_title; ?></h1>
	</div>
	<?php
		if (isset($error_message)) :
			echo "<div class='error_msg bg-danger'>";
			if (isset($error_message)) {
				echo $error_message;
			}
			echo validation_errors();
			echo "</div>";
		endif;

		if (isset($message_display)) {
			echo "<div class='message bg-success'>";
			echo $message_display;
			echo "</div>";
		}
	?>
	<form class="form-horizontal" method="post" action="<?= base_url(); ?>user/save-user">
		<div class="form-group">
		    <label for="name" class="col-sm-1 control-label">Name</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="name" id="name" value="<?= $name; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="company_name" class="col-sm-1 control-label">Company Name</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="company_name" id="company_name" value="<?= $company_name; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="email" class="col-sm-1 control-label">Email</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="email" id="email" value="<?= $email; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="password" class="col-sm-1 control-label">Password</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="password" id="password" value="">
		    </div>
		</div>
		<div class="form-group">
		    <label for="confirm_password" class="col-sm-1 control-label">Confirm Password</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="confirm_password" id="confirm_password" value="">
		    </div>
		</div>
		<div class="form-group signup-button-center">
		    <div class="col-sm-offset-1 col-sm-4">
		      <a class="btn btn-default btn-custom-grey" href="<?= base_url();?>user" role="button">Back</a>
		      <button type="submit" class="btn btn-primary btn-custom-blue"><?= $button_title; ?></button>
		    </div>
		</div>
		<input type="hidden" class="form-control" name="id" id="id" value="<?= $id; ?>">
	</form>
</div>
