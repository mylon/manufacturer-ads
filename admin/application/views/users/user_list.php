<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!isset($this->session->userdata['manufacturer_staff_logged_in'])) {
    header("location: ".base_url()."login");
    exit;
}

?>
<script type="text/javascript">
	var url="<?php echo base_url();?>";
    function deleteCheck(id) {
        var r=confirm("Do you want to delete this staff?");
        if (r==true) {
        	window.location = url+"user/delete/"+id;
        } 
        else {
        	 return false;
        }
    } 
</script>
<div class="container-fluid fix-height-container">
	<div class="content-title">
		<h1>User List</h1>
	</div>
	<?php
		if (isset($message_display)) {
			echo "<div class='message bg-success'>";
			echo $message_display;
			echo "</div>";
		}
	?>
	<div id="hospital-list" class="table-responsive">
		<div class="add-button add-button-right">
			<a class="btn btn-primary" role="button" href="<?= base_url(); ?>user/add">Add User</a>
		</div>
		<table class="table table-striped">
			<thead>
				<th>ID</th>
				<th>Name</th>
				<th>Company Name</th>
				<th>Email</th>
				<th>Edit</th>
				<th>Delete</th>
			</thead>
			<tbody>
				<?php if(!empty($userList)): ?>
					<?php foreach($userList as $user):?>
						<tr>
							<td><?= $user->id; ?></td>
							<td><?= ucfirst($user->name); ?></td>
							<td><?= ucfirst($user->company_name); ?></td>
							<td><?= $user->email; ?></td>
							<td><a href="<?= base_url(); ?>user/edit/<?= $user->id; ?>">Edit</a></td>
							<td><a href="javascript:void(0);" onclick="deleteCheck(<?= $user->id ?>);">Delete</a></td>
						</tr>
						
					<?php endforeach;?>
				<?php else: ?>
						<tr>
							<td colspan="6">No results found</td>
						</tr>
				<?php endif;?>
			</tbody>
		</table>
	</div>
	<nav>
		<ul class="pagination">
			<?php echo $links; ?>
		</ul>
	</nav>
</div>