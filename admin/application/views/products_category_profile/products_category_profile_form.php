<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!isset($this->session->userdata['manufacturer_staff_logged_in'])) {
    header("location: ".base_url()."login");
    exit;
}

$id 	= (!empty($categoryData)) ? $categoryData[0]->id : 0;
$name  = (!empty($categoryData)) ? $categoryData[0]->name : '';
$description  = (!empty($categoryData)) ? $categoryData[0]->description : '';
$language_id  = (!empty($categoryData)) ? $categoryData[0]->language_id : '';
$products_category_id  = (!empty($categoryData)) ? $categoryData[0]->products_category_id : '';

?>

<div class="container-fluid fix-height-container">
	<div class="content-title">
		<h1><?= $page_title; ?></h1>
	</div>
	<?php
		if (isset($error_message)) :
			echo "<div class='error_msg bg-danger'>";
			if (isset($error_message)) {
				echo $error_message;
			}
			echo validation_errors();
			echo "</div>";
		endif;

		if (isset($message_display)) {
			echo "<div class='message bg-success'>";
			echo $message_display;
			echo "</div>";
		}
	?>
	<form class="form-horizontal" method="post" action="<?= base_url(); ?>products-category-profile/save-products-category">
		<div class="form-group">
		    <label for="language_id" class="col-sm-1 control-label">Language</label>
		    <div class="col-sm-4">
		      <select class="form-control" name="language_id">
		      		<option value="">Please Select</option>
		      		<?php foreach($languageList as $k => $v):?>
		      		<?php $selected = ($language_id == $k) ? "selected" : "";?>
				  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
					<?php endforeach; ?>
				</select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="products_category_id" class="col-sm-1 control-label">Category</label>
		    <div class="col-sm-4">
		      <select class="form-control" name="products_category_id">
		      		<option value="">Please Select</option>
		      		<?php foreach($categoryList as $k => $v):?>
		      		<?php $selected = ($products_category_id == $k) ? "selected" : "";?>
				  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
					<?php endforeach; ?>
				</select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="name" class="col-sm-1 control-label">Category Name</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="name" id="name" value="<?= $name; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="code" class="col-sm-1 control-label">Description</label>
		    <div class="col-sm-4">
		      <textarea class="form-control" rows="3" name="description" id="description"><?= htmlspecialchars_decode($description); ?></textarea>
		    </div>
		</div>
		<div class="form-group signup-button-center">
		    <div class="col-sm-offset-1 col-sm-4">
		      <a class="btn btn-default btn-custom-grey" href="<?= base_url();?>products-category-profile" role="button">Back</a>
		      <button type="submit" class="btn btn-primary btn-custom-blue"><?= $button_title; ?></button>
		    </div>
		</div>
		<input type="hidden" class="form-control" name="id" id="id" value="<?= $id; ?>">
	</form>
</div>
