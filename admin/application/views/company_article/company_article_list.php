<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!isset($this->session->userdata['manufacturer_staff_logged_in'])) {
    header("location: ".base_url()."login");
    exit;
}

?>
<script type="text/javascript">
	var url="<?php echo base_url();?>";
    function deleteCheck(id) {
        var r=confirm("Do you want to delete this company?");
        if (r==true) {
        	window.location = url+"company-article/delete/"+id;
        } 
        else {
        	 return false;
        }
    } 
</script>
<div class="container-fluid fix-height-container">
	<div class="content-title">
		<h1>Article List</h1>
	</div>
	<?php
		if (isset($message_display)) {
			echo "<div class='message bg-success'>";
			echo $message_display;
			echo "</div>";
		}
	?>
	<div id="hospital-list" class="table-responsive">
		<div class="add-button add-button-right">
			<a class="btn btn-primary" role="button" href="<?= base_url(); ?>company-article/add">Add Article</a>
		</div>
		<table class="table table-striped">
			<thead>
				<th>ID</th>
				<th>Title</th>
				<th>Images</th>
				<th>Created By</th>
				<th>Edited By</th>
				<th>Created Date</th>
				<th>Edit</th>
				<th>Delete</th>
			</thead>
			<tbody>
				<?php if(!empty($ArticleList)): ?>
					<?php foreach($ArticleList as $article):?>
						<tr>
							<td><?= $article->id; ?></td>
							<td><?= ucfirst($article->title); ?></td>
							<td><a href="<?= base_url(); ?>company-article/upload-image/<?= $article->id; ?>">Images</a></td>
							<td><?= date($article->created_date, 'd/m/Y'); ?></td>
							<td><?= ucfirst($article->created_by); ?></td>
							<td><?= ucfirst($article->edited_by); ?></td>
							<td><a href="<?= base_url(); ?>company-article/edit/<?= $article->id; ?>">Edit</a></td>
							<td><a href="javascript:void(0);" onclick="deleteCheck(<?= $article->id ?>);">Delete</a></td>
						</tr>
						
					<?php endforeach;?>
				<?php else: ?>
						<tr>
							<td colspan="8">No results found</td>
						</tr>
				<?php endif;?>
			</tbody>
		</table>
	</div>
	<nav>
		<ul class="pagination">
			<?php echo $links; ?>
		</ul>
	</nav>
</div>