<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!isset($this->session->userdata['manufacturer_staff_logged_in'])) {
    header("location: ".base_url()."login");
    exit;
}

$category_name = isset($name) ? $name : "";
$company_category_id = isset($company_category_id) ? $company_category_id : "";
$sub_company_category_id = isset($sub_company_category_id) ? $sub_company_category_id : "";
$language_id = isset($language_id) ? $language_id : "";

?>
<script type="text/javascript">
	var url="<?php echo base_url();?>";
    function deleteCheck(id) {
        var r=confirm("Do you want to delete this company category?");
        if (r==true) {
        	window.location = url+"sub-company-category-profile/delete/"+id;
        } 
        else {
        	 return false;
        }
    } 
</script>
<div class="container-fluid fix-height-container">
	<div class="content-title">
		<h1>Sub Company Category Profile List</h1>
	</div>
	<?php
		if (isset($message_display)) {
			echo "<div class='message bg-success'>";
			echo $message_display;
			echo "</div>";
		}
	?>
	<div class="search-content">
		<form class="form-horizontal" method="post" action="<?= base_url(); ?>sub-company-category-profile">
			<div class="form-group">
			    <label for="name" class="col-sm-1 control-label">Name</label>
			    <div class="col-sm-2">
			      <input type="text" class="form-control" name="name" id="name" value="<?= $category_name; ?>">
			    </div>
			</div>
			<div class="form-group">
			    <label for="company_category_id" class="col-sm-1 control-label">Main Category</label>
			    <div class="col-sm-2">
			      <select class="form-control" name="company_category_id" id="company_category_id">
			      		<option value="">Please Select</option>
			      		<?php foreach($maincategoryList as $k => $v):?>
			      		<?php $selected = ($company_category_id == $k) ? "selected" : "";?>
					  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
						<?php endforeach; ?>
					</select>
			    </div>
			</div>
			<div class="form-group">
			    <label for="sub_company_category_id" class="col-sm-1 control-label">Sub Category</label>
			    <div class="col-sm-2">
			      <select class="form-control" name="sub_company_category_id" id="sub_company_category_id">
			      		<option value="">Please Select</option>
			      		<?php foreach($subcategoryList as $k => $v):?>
			      		<?php $selected = ($sub_company_category_id == $k) ? "selected" : "";?>
					  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
						<?php endforeach; ?>
					</select>
			    </div>
			</div>
			<div class="form-group">
			    <label for="language_id" class="col-sm-1 control-label">Language</label>
			    <div class="col-sm-2">
			      <select class="form-control" name="language_id" id="language_id">
			      		<option value="">Please Select</option>
			      		<?php foreach($languageList as $k => $v):?>
			      		<?php $selected = ($language_id == $k) ? "selected" : "";?>
					  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
						<?php endforeach; ?>
					</select>
			    </div>
			</div>
			<div class="form-group signup-button-center">
			    <div class="col-sm-offset-1 col-sm-4">
			      <a class="btn btn-default btn-custom-grey" href="<?= base_url();?>sub-company-category-profile" role="button">Reset</a>
			      <button type="submit" class="btn btn-primary btn-custom-blue">Search</button>
			    </div>
			</div>
		</form>
	</div>
	<div id="hospital-list" class="table-responsive">
		<div class="add-button add-button-right">
			<a class="btn btn-primary" role="button" href="<?= base_url(); ?>sub-company-category-profile/add">Add Sub Company Category Profile</a>
		</div>
		<table class="table table-striped">
			<thead>
				<th>ID</th>
				<th>Name</th>
				<th>Main Category</th>
				<th>Sub Category</th>
				<th>Language</th>
				<th>Edit</th>
				<th>Delete</th>
			</thead>
			<tbody>
				<?php if(!empty($companyCategoryList)): ?>
					<?php foreach($companyCategoryList as $company_category):?>
						<tr>
							<td><?= $company_category->id; ?></td>
							<td><?= ucfirst($company_category->name); ?></td>
							<td><?= ucfirst($company_category->main_category_name); ?></td>
							<td><?= ucfirst($company_category->sub_main_category_name); ?></td>
							<td><?= ucfirst($company_category->language_name); ?></td>
							<td><a href="<?= base_url(); ?>sub-company-category-profile/edit/<?= $company_category->id; ?>">Edit</a></td>
							<td><a href="javascript:void(0);" onclick="deleteCheck(<?= $company_category->id ?>);">Delete</a></td>
						</tr>
					<?php endforeach;?>
				<?php else: ?>
						<tr>
							<td colspan="8">No results found</td>
						</tr>
				<?php endif;?>
			</tbody>
		</table>
	</div>
	<nav>
		<ul class="pagination">
			<?php echo $links; ?>
		</ul>
	</nav>
</div>
<script type="text/javascript">
	function capitalize(string) {
		return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
	}
	
	$('#company_category_id').change(function(){
	    var company_category_id = $(this).val();
	    $("#sub_company_category_id > option").remove();
	    $.ajax({
	        type: "POST",
	        url: "<?php echo site_url('adminsubcompanycategoryprofile/sub_category_list'); ?>",
	        data: {company_category_id: company_category_id},
	        dataType: 'json',
	        success:function(data){
	        	$('#sub_company_category_id').html('<option value="0">Please Select</option>')
	            $.each(data,function(k, v){
	                var opt = $('<option />');
	                opt.val(k);
	                opt.text(capitalize(v));
	                $('#sub_company_category_id').append(opt);
	            });
	        }
	    });
	});
</script>