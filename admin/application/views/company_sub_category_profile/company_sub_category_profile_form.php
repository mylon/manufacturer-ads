<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!isset($this->session->userdata['manufacturer_staff_logged_in'])) {
    header("location: ".base_url()."login");
    exit;
}

$id 	= (!empty($companyCategoryData)) ? $companyCategoryData[0]->id : 0;
$name  = (!empty($companyCategoryData)) ? $companyCategoryData[0]->name : '';
$language_id  = (!empty($companyCategoryData)) ? $companyCategoryData[0]->language_id : '';
$sub_company_category_id  = (!empty($companyCategoryData)) ? $companyCategoryData[0]->sub_company_category_id : '';

?>

<div class="container-fluid fix-height-container">
	<div class="content-title">
		<h1><?= $page_title; ?></h1>
	</div>
	<?php
		if (isset($error_message)) :
			echo "<div class='error_msg bg-danger'>";
			if (isset($error_message)) {
				echo $error_message;
			}
			echo validation_errors();
			echo "</div>";
		endif;

		if (isset($message_display)) {
			echo "<div class='message bg-success'>";
			echo $message_display;
			echo "</div>";
		}
	?>
	<form class="form-horizontal" method="post" action="<?= base_url(); ?>sub-company-category-profile/save-company-category">
		<div class="form-group">
		    <label for="name" class="col-sm-1 control-label">Category Name</label>
		    <div class="col-sm-4">
		      <input type="text" class="form-control" name="name" id="name" value="<?= $name; ?>">
		    </div>
		</div>
		<div class="form-group">
		    <label for="company_category_id" class="col-sm-1 control-label">Main Category</label>
		    <div class="col-sm-4">
		      <select class="form-control" name="company_category_id" id="company_category_id">
		      		<option value="">Please Select</option>
		      		<?php foreach($maincategoryList as $k => $v):?>
		      		<?php $selected = ($company_category_id == $k) ? "selected" : "";?>
				  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
					<?php endforeach; ?>
				</select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="sub_company_category_id" class="col-sm-1 control-label">Sub Category</label>
		    <div class="col-sm-4">
		      <select class="form-control" name="sub_company_category_id" id="sub_company_category_id">
		      		<option value="">Please Select</option>
		      		<?php foreach($subcategoryList as $k => $v):?>
		      		<?php $selected = ($sub_company_category_id == $k) ? "selected" : "";?>
				  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
					<?php endforeach; ?>
				</select>
		    </div>
		</div>
		<div class="form-group">
		    <label for="language_id" class="col-sm-1 control-label">Language</label>
		    <div class="col-sm-4">
		      <select class="form-control" name="language_id" id="language_id">
		      		<option value="">Please Select</option>
		      		<?php foreach($languageList as $k => $v):?>
		      		<?php $selected = ($language_id == $k) ? "selected" : "";?>
				  		<option value="<?= $k; ?>" <?= $selected?>><?= ucfirst($v); ?></option>
					<?php endforeach; ?>
				</select>
		    </div>
		</div>
		<div class="form-group signup-button-center">
		    <div class="col-sm-offset-1 col-sm-4">
		      <a class="btn btn-default btn-custom-grey" href="<?= base_url();?>sub-company-category-profile" role="button">Back</a>
		      <button type="submit" class="btn btn-primary btn-custom-blue"><?= $button_title; ?></button>
		    </div>
		</div>
		<input type="hidden" class="form-control" name="id" id="id" value="<?= $id; ?>">
	</form>
</div>
<script type="text/javascript">
	function capitalize(string) {
		return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
	}
	
	$('#company_category_id').change(function(){
	    var company_category_id = $(this).val();
	    $("#sub_company_category_id > option").remove();
	    $.ajax({
	        type: "POST",
	        url: "<?php echo site_url('adminsubcompanycategoryprofile/sub_category_list'); ?>",
	        data: {company_category_id: company_category_id},
	        dataType: 'json',
	        success:function(data){
	        	$('#sub_company_category_id').html('<option value="0">Please Select</option>')
	            $.each(data,function(k, v){
	                var opt = $('<option />');
	                opt.val(k);
	                opt.text(capitalize(v));
	                $('#sub_company_category_id').append(opt);
	            });
	        }
	    });
	});
</script>