<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'adminauthentication';

$route['login'] = 'adminauthentication/admin_login_process';
$route['logout'] = 'adminauthentication/logout';
$route['dashboard'] = 'admindashboard';

$route['country/(:num)'] = 'admincountry/index/$1';
$route['country/save-country'] = 'admincountry/save_country_process';
$route['country/add'] = 'admincountry/add';
$route['country/edit/(:num)'] = 'admincountry/edit/$1';
$route['country/delete/(:num)'] = 'admincountry/delete/$1';
$route['country'] = 'admincountry/index';

$route['menu/(:num)'] = 'adminmenu/index/$1';
$route['menu/save-menu'] = 'adminmenu/save_menu_process';
$route['menu/add'] = 'adminmenu/add';
$route['menu/edit/(:num)'] = 'adminmenu/edit/$1';
$route['menu/delete/(:num)'] = 'adminmenu/delete/$1';
$route['menu'] = 'adminmenu/index';

$route['company-menu/(:num)'] = 'admincompanymenu/index/$1';
$route['company-menu/save-menu'] = 'admincompanymenu/save_menu_process';
$route['company-menu/add'] = 'admincompanymenu/add';
$route['company-menu/edit/(:num)'] = 'admincompanymenu/edit/$1';
$route['company-menu/delete/(:num)'] = 'admincompanymenu/delete/$1';
$route['company-menu'] = 'admincompanymenu/index';

$route['prefecture/(:num)'] = 'adminprefecture/index/$1';
$route['prefecture/save-prefecture'] = 'adminprefecture/save_prefecture_process';
$route['prefecture/add'] = 'adminprefecture/add';
$route['prefecture/edit/(:num)'] = 'adminprefecture/edit/$1';
$route['prefecture/delete/(:num)'] = 'adminprefecture/delete/$1';
$route['prefecture'] = 'adminprefecture/index';

$route['prefecture-profile/(:num)'] = 'adminprefectureprofile/index/$1';
$route['prefecture-profile/save-prefecture'] = 'adminprefectureprofile/save_prefecture_process';
$route['prefecture-profile/add'] = 'adminprefectureprofile/add';
$route['prefecture-profile/edit/(:num)'] = 'adminprefectureprofile/edit/$1';
$route['prefecture-profile/delete/(:num)'] = 'adminprefectureprofile/delete/$1';
$route['prefecture-profile'] = 'adminprefectureprofile/index';

$route['company-profile/save-company'] = 'admincompanyprofile/save_company_process';
$route['company-profile/delete/(:num)'] = 'admincompanyprofile/delete/$1';
$route['company-profile/edit/(:num)'] = 'admincompanyprofile/edit/$1';
$route['company-profile/add'] = 'admincompanyprofile/add';
$route['company-profile'] = 'admincompanyprofile/index';
$route['company-profile/(:num)'] = 'admincompanyprofile/index/$1';

$route['company/delete-images/(:num)/(:any)/(:num)'] = 'admincompany/delete_images/$1/$2/$3';
$route['company/save-images/(:num)'] = 'admincompany/save_images/$1';
$route['company/upload-images/(:num)'] = 'admincompany/upload_images/$1';
$route['company/delete-logo/(:num)/(:any)'] = 'admincompany/delete_logo/$1/$2';
$route['company/save-logo/(:num)'] = 'admincompany/save_logo/$1';
$route['company/upload-logo/(:num)'] = 'admincompany/upload_logo/$1';
$route['company/save-company'] = 'admincompany/save_company_process';
$route['company/delete/(:num)'] = 'admincompany/delete/$1';
$route['company/edit/(:num)'] = 'admincompany/edit/$1';
$route['company/add'] = 'admincompany/add';
$route['company'] = 'admincompany/index';
$route['company/(:num)'] = 'admincompany/index/$1';

$route['company-article/delete-image/(:num)/(:any)'] = 'admincompanyarticle/delete_image/$1/$2';
$route['company-article/save-image/(:num)'] = 'admincompanyarticle/save_image/$1';
$route['company-article/upload-image/(:num)'] = 'admincompanyarticle/upload_image/$1';
$route['company-article/save-article'] = 'admincompanyarticle/save_company_article_process';
$route['company-article/delete/(:num)'] = 'admincompanyarticle/delete/$1';
$route['company-article/edit/(:num)'] = 'admincompanyarticle/edit/$1';
$route['company-article/add'] = 'admincompanyarticle/add';
$route['company-article'] = 'admincompanyarticle/index';

$route['industrial-park/(:num)'] = 'adminindustrialpark/index/$1';
$route['industrial-park/save-industrial-park'] = 'adminindustrialpark/save_industrial_park_process';
$route['industrial-park/add'] = 'adminindustrialpark/add';
$route['industrial-park/edit/(:num)'] = 'adminindustrialpark/edit/$1';
$route['industrial-park/delete/(:num)'] = 'adminindustrialpark/delete/$1';
$route['industrial-park'] = 'adminindustrialpark/index';

$route['industrial-park-profile/(:num)'] = 'adminindustrialparkprofile/index/$1';
$route['industrial-park-profile/save-industrial-park'] = 'adminindustrialparkprofile/save_industrial_park_process';
$route['industrial-park-profile/add'] = 'adminindustrialparkprofile/add';
$route['industrial-park-profile/edit/(:num)'] = 'adminindustrialparkprofile/edit/$1';
$route['industrial-park-profile/delete/(:num)'] = 'adminindustrialparkprofile/delete/$1';
$route['industrial-park-profile'] = 'adminindustrialparkprofile/index';

$route['company-category/(:num)'] = 'admincompanycategory/index/$1';
$route['company-category/save-company-category'] = 'admincompanycategory/save_company_category_process';
$route['company-category/add'] = 'admincompanycategory/add';
$route['company-category/edit/(:num)'] = 'admincompanycategory/edit/$1';
$route['company-category/delete/(:num)'] = 'admincompanycategory/delete/$1';
$route['company-category'] = 'admincompanycategory/index';

$route['company-category-profile/(:num)'] = 'admincompanycategoryprofile/index/$1';
$route['company-category-profile/save-company-category'] = 'admincompanycategoryprofile/save_company_category_process';
$route['company-category-profile/add'] = 'admincompanycategoryprofile/add';
$route['company-category-profile/edit/(:num)'] = 'admincompanycategoryprofile/edit/$1';
$route['company-category-profile/delete/(:num)'] = 'admincompanycategoryprofile/delete/$1';
$route['company-category-profile'] = 'admincompanycategoryprofile/index';


$route['sub-company-category/(:num)'] = 'adminsubcompanycategory/index/$1';
$route['sub-company-category/save-company-category'] = 'adminsubcompanycategory/save_company_category_process';
$route['sub-company-category/add'] = 'adminsubcompanycategory/add';
$route['sub-company-category/edit/(:num)'] = 'adminsubcompanycategory/edit/$1';
$route['sub-company-category/delete/(:num)'] = 'adminsubcompanycategory/delete/$1';
$route['sub-company-category'] = 'adminsubcompanycategory/index';

$route['sub-company-category-profile/(:num)'] = 'adminsubcompanycategoryprofile/index/$1';
$route['sub-company-category-profile/save-company-category'] = 'adminsubcompanycategoryprofile/save_company_category_process';
$route['sub-company-category-profile/add'] = 'adminsubcompanycategoryprofile/add';
$route['sub-company-category-profile/edit/(:num)'] = 'adminsubcompanycategoryprofile/edit/$1';
$route['sub-company-category-profile/delete/(:num)'] = 'adminsubcompanycategoryprofile/delete/$1';
$route['sub-company-category-profile'] = 'adminsubcompanycategoryprofile/index';

$route['genre/(:num)'] = 'admingenre/index/$1';
$route['genre/save-genre'] = 'admingenre/save_genre_process';
$route['genre/add'] = 'admingenre/add';
$route['genre/edit/(:num)'] = 'admingenre/edit/$1';
$route['genre/delete/(:num)'] = 'admingenre/delete/$1';
$route['genre'] = 'admingenre/index';


$route['company-products/(:num)'] = 'admincompanyproduct/index/$1';
$route['company-products/delete-image/(:num)/(:any)'] = 'admincompanyproduct/delete_image/$1/$2';
$route['company-products/save-image/(:num)'] = 'admincompanyproduct/save_image/$1';
$route['company-products/upload-image/(:num)'] = 'admincompanyproduct/upload_image/$1';
$route['company-products/description-delete-image/(:num)/(:any)/(:num)'] = 'admincompanyproduct/description_delete_image/$1/$2/$3';
$route['company-products/description-save-image/(:num)'] = 'admincompanyproduct/description_save_image/$1';
$route['company-products/description-upload-image/(:num)'] = 'admincompanyproduct/description_upload_image/$1';
$route['company-products/save-company-products'] = 'admincompanyproduct/save_products_process';
$route['company-products/add'] = 'admincompanyproduct/add';
$route['company-products/edit/(:num)'] = 'admincompanyproduct/edit/$1';
$route['company-products/delete/(:num)'] = 'admincompanyproduct/delete/$1';
$route['company-products'] = 'admincompanyproduct/index';


$route['company-products-profile/(:num)'] = 'admincompanyproductprofile/index/$1';
$route['company-products-profile/delete-pdf/(:num)/(:any)/(:num)'] = 'admincompanyproductprofile/delete_pdf/$1/$2/$3';
$route['company-products-profile/save-pdf/(:num)'] = 'admincompanyproductprofile/save_pdf/$1';
$route['company-products-profile/upload-pdf/(:num)'] = 'admincompanyproductprofile/upload_pdf/$1';
$route['company-products-profile/save-company-products'] = 'admincompanyproductprofile/save_products_process';
$route['company-products-profile/add'] = 'admincompanyproductprofile/add';
$route['company-products-profile/edit/(:num)'] = 'admincompanyproductprofile/edit/$1';
$route['company-products-profile/delete/(:num)'] = 'admincompanyproductprofile/delete/$1';
$route['company-products-profile'] = 'admincompanyproductprofile/index';

$route['company-news/delete-author/(:num)/(:any)'] = 'admincompanynews/delete_author/$1/$2';
$route['company-news/save-author/(:num)'] = 'admincompanynews/save_author/$1';
$route['company-news/upload-author/(:num)'] = 'admincompanynews/upload_author/$1';
$route['company-news/delete-image/(:num)/(:any)'] = 'admincompanynews/delete_image/$1/$2';
$route['company-news/save-image/(:num)'] = 'admincompanynews/save_image/$1';
$route['company-news/upload-image/(:num)'] = 'admincompanynews/upload_image/$1';
$route['company-news/description-delete-image/(:num)/(:any)/(:num)'] = 'admincompanynews/description_delete_image/$1/$2/$3';
$route['company-news/description-save-image/(:num)'] = 'admincompanynews/description_save_image/$1';
$route['company-news/description-upload-image/(:num)'] = 'admincompanynews/description_upload_image/$1';
$route['company-news/save-company-news'] = 'admincompanynews/save_news_process';
$route['company-news/add'] = 'admincompanynews/add';
$route['company-news/edit/(:num)'] = 'admincompanynews/edit/$1';
$route['company-news/delete/(:num)'] = 'admincompanynews/delete/$1';
$route['company-news'] = 'admincompanynews/index';
$route['company-news/(:num)'] = 'admincompanynews/index/$1';

$route['news-category/(:num)'] = 'adminnewscategory/index/$1';
$route['news-category/save-news-category'] = 'adminnewscategory/save_news_category_process';
$route['news-category/add'] = 'adminnewscategory/add';
$route['news-category/edit/(:num)'] = 'adminnewscategory/edit/$1';
$route['news-category/delete/(:num)'] = 'adminnewscategory/delete/$1';
$route['news-category'] = 'adminnewscategory/index';

$route['news-category-profile/(:num)'] = 'adminnewscategoryprofile/index/$1';
$route['news-category-profile/save-news-category'] = 'adminnewscategoryprofile/save_news_category_process';
$route['news-category-profile/add'] = 'adminnewscategoryprofile/add';
$route['news-category-profile/edit/(:num)'] = 'adminnewscategoryprofile/edit/$1';
$route['news-category-profile/delete/(:num)'] = 'adminnewscategoryprofile/delete/$1';
$route['news-category-profile'] = 'adminnewscategoryprofile/index';

$route['language/(:num)'] = 'adminlanguage/index/$1';
$route['language/save-language'] = 'adminlanguage/save_language_process';
$route['language/add'] = 'adminlanguage/add';
$route['language/edit/(:num)'] = 'adminlanguage/edit/$1';
$route['language/delete/(:num)'] = 'adminlanguage/delete/$1';
$route['language'] = 'adminlanguage/index';


$route['products/(:num)'] = 'adminproducts/index/$1';
$route['products/save-products'] = 'adminproducts/save_products_process';
$route['products/add'] = 'adminproducts/add';
$route['products/edit/(:num)'] = 'adminproducts/edit/$1';
$route['products/delete/(:num)'] = 'adminproducts/delete/$1';
$route['products'] = 'adminproducts/index';

$route['products-category/(:num)'] = 'adminproductscategory/index/$1';
$route['products-category/save-products-category'] = 'adminproductscategory/save_products_category_process';
$route['products-category/add'] = 'adminproductscategory/add';
$route['products-category/edit/(:num)'] = 'adminproductscategory/edit/$1';
$route['products-category/delete/(:num)'] = 'adminproductscategory/delete/$1';
$route['products-category'] = 'adminproductscategory/index';

$route['products-category-profile/(:num)'] = 'adminproductscategoryprofile/index/$1';
$route['products-category-profile/save-products-category'] = 'adminproductscategoryprofile/save_products_category_process';
$route['products-category-profile/add'] = 'adminproductscategoryprofile/add';
$route['products-category-profile/edit/(:num)'] = 'adminproductscategoryprofile/edit/$1';
$route['products-category-profile/delete/(:num)'] = 'adminproductscategoryprofile/delete/$1';
$route['products-category-profile'] = 'adminproductscategoryprofile/index';

$route['staff/(:num)'] = 'adminstaff/index/$1';
$route['staff/save-staff'] = 'adminstaff/save_staff_process';
$route['staff/add'] = 'adminstaff/add';
$route['staff/edit/(:num)'] = 'adminstaff/edit/$1';
$route['staff/delete/(:num)'] = 'adminstaff/delete/$1';
$route['staff'] = 'adminstaff/index';

$route['user/(:num)'] = 'adminuser/index/$1';
$route['user/save-user'] = 'adminuser/save_user_process';
$route['user/add'] = 'adminuser/add';
$route['user/edit/(:num)'] = 'adminuser/edit/$1';
$route['user/delete/(:num)'] = 'adminuser/delete/$1';
$route['user'] = 'adminuser/index';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
