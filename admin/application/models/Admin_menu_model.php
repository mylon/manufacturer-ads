<?php

Class Admin_menu_model extends CI_Model {


	public function getMenuList($limit, $start, $search) {

		$this->db->select('m.*, l.name as language_name');
		$this->db->from('menu m');
		$this->db->join('language l', 'l.id = m.language_id', 'left');

		/// search
		if(!empty($search['menu'])) :
			$this->db->where("m.menu LIKE '%".$search['menu']."%'");
		endif;

		if(!empty($search['language_id'])) :
			$this->db->where("m.language_id LIKE '%".$search['language_id']."%'");
		endif;
		/// endsearch


		$this->db->order_by("menu", "asc"); 
		$this->db->limit($limit, $start); 
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function countMenu($search) {
		
		$this->db->select('m.*, l.name as language_name');
		$this->db->from('menu m');
		$this->db->join('language l', 'l.id = m.language_id', 'left');

		/// search
		if(!empty($search['menu'])) :
			$this->db->where("m.menu LIKE '%".$search['menu']."%'");
		endif;

		if(!empty($search['language_id'])) :
			$this->db->where("m.language_id LIKE '%".$search['language_id']."%'");
		endif;
		/// endsearch

		$query = $this->db->get();

		return $query->num_rows();
	}

	public function checkMenubyName($menu, $language_id) {
		$condition = "LOWER(menu) =" . "'" . $menu . "' AND language_id=". "'" . $language_id . "'";
		$this->db->select('*');
		$this->db->from('menu');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  true;
		} else {
			return false;
		}
	}


	public function getMenuById($id){
		$condition = "id =" . "'" . $id . "'";
		$this->db->select('*');
		$this->db->from('menu');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function saveMenu($data) {

		$action = (isset($data['id']) && $data['id'] > 0) ? "edit" : "add";

		if ($action == "add") :
			$result = $this->db->insert('menu', $data); 
		else:
			$this->db->where('id', $data['id']);
			$result = $this->db->update('menu', $data); 
		endif;

		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function deleteMenu($id) {
		$sql = "DELETE FROM menu WHERE id =".$id;
		$result = $this->db->query($sql);
		if ($result) {
			$sql = "DELETE FROM company_menu WHERE menu_id =".$id;
			$result = $this->db->query($sql);
			if ($result) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function getLanugageList() {

		$this->db->select('*');
		$this->db->from('language');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$country = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$country[$r->id] = $r->name;
			}
			return $country;
		} else {
			return false;
		}
	}

	
}

?>
