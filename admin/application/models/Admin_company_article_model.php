<?php

Class Admin_company_article_model extends CI_Model {
	
	public function getArticleList($limit, $start) {
		$this->db->select('company_article.*, s1.username as created_by, s2.username as edited_by');
		$this->db->from('company_article');
		$this->db->join('staff s1', 's1.id = company_article.created_by', 'left');
		$this->db->join('staff s2', 's2.id = company_article.edited_by', 'left');
		$this->db->order_by("created_date", "desc"); 
		$this->db->limit($limit, $start); 
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function countArticle() {
		return $this->db->count_all_results('company_article');
	}

	public function checkCompanybyName($name) {
		$condition = "LOWER(name) =" . "'" . strtolower($name) . "'";
		$this->db->select('*');
		$this->db->from('company');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  true;
		} else {
			return false;
		}
	}

	public function getCompanyById($id){
		$condition = "id =" . "'" . $id . "'";
		$this->db->select('*');
		$this->db->from('company');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function getGenreList() {

		$this->db->select('*');
		$this->db->from('genre');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$category = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category[$r->id] = $r->name;
			}
			return $category;
		} else {
			return false;
		}
	}

	public function getArticleCategoryList() {

		$this->db->select('*');
		$this->db->from('article_category');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$category = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category[$r->id] = $r->name;
			}
			return $category;
		} else {
			return false;
		}
	}

	public function getCountryList() {

		$this->db->select('*');
		$this->db->from('country');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$country = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$country[$r->id] = $r->name;
			}
			return $country;
		} else {
			return false;
		}
	}

	public function prefecture_data($country_id) {
		$condition = "country_id =" . "'" . $country_id . "'";
		$this->db->select('*');
		$this->db->from('prefecture');
		$this->db->order_by("name", "asc");
		$this->db->where($condition);
		$query = $this->db->get();
		$prefecture = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$prefecture[$r->id] = $r->name;
			}
			return $prefecture;
		} else {
			return false;
		}
		
	}

	public function industrial_park_data($country_id, $prefecture_id) {
		$condition = "country_id =" . "'" . $country_id . "' AND prefecture_id = '". $prefecture_id . "'";
		$this->db->select('*');
		$this->db->from('industrial_park');
		$this->db->order_by("name", "asc");
		$this->db->where($condition);
		$query = $this->db->get();
		$industrial_park = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$industrial_park[$r->id] = $r->name;
			}
			return $industrial_park;
		} else {
			return false;
		}
		
	}

	public function saveCompany($data) {

		$action = (isset($data['id']) && $data['id'] > 0) ? "edit" : "add";

		if ($action == "add") :
			$result = $this->db->insert('company', $data); 
		else:
			$this->db->where('id', $data['id']);
			$result = $this->db->update('company', $data); 
		endif;

		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function deleteCompnay($id) {
		$sql = "DELETE FROM company WHERE id =".$id;
		$result = $this->db->query($sql);
		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function getLogoImage($id){
        $condition = "id =" . "'" . $id . "'";
		$this->db->select('logo, logo_link');
		$this->db->from('company');
		$this->db->where($condition);
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return $query->result();
		} else {
			return false;
		}
    }

    public function deleteCompnayLogo($id) {

		$this->db->where('id', $id);
		$data['logo'] = "";
		$data['logo_link'] = "";
		$result = $this->db->update('company', $data); 

		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function deleteCompnayImages($id) {

		$sql = "DELETE FROM company_images WHERE id =".$id;
		$result = $this->db->query($sql);
		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function getCompanyImages($company_id){
        $condition = "company_id =" . "'" . $company_id . "'";
		$this->db->select('*');
		$this->db->from('company_images');
		$this->db->where($condition);
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return $query->result();
		} else {
			return false;
		}
    }

	
}

?>
