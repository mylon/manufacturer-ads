<?php

Class Admin_industrial_park_model extends CI_Model {


	public function getIndustrialParkList($limit, $start, $search) {

		$this->db->select('i.*, p.name as prefecture_name, c.name as country_name');
		$this->db->from('industrial_park i');
		$this->db->join('prefecture p', 'p.id = i.prefecture_id', 'left');
		$this->db->join('country c', 'c.id = p.country_id', 'left');

		/// search
		if(!empty($search['name'])) :
			$this->db->where("i.name LIKE '%".$search['name']."%'");
		endif;
		if(!empty($search['prefecture_id'])) :
			$this->db->where("i.prefecture_id LIKE '%".$search['prefecture_id']."%'");
		endif;
		if(!empty($search['country_id'])) :
			$this->db->where("p.country_id LIKE '%".$search['country_id']."%'");
		endif;
		/// endsearch
		
		$this->db->order_by("i.name", "asc"); 
		$this->db->limit($limit, $start); 
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function countIndustialPark($search) {
		
		$this->db->select('i.*, p.name as prefecture_name, c.name as country_name');
		$this->db->from('industrial_park i');
		$this->db->join('prefecture p', 'p.id = i.prefecture_id', 'left');
		$this->db->join('country c', 'c.id = p.country_id', 'left');
		

		/// search
		if(!empty($search['name'])) :
			$this->db->where("i.name LIKE '%".$search['name']."%'");
		endif;
		if(!empty($search['prefecture_id'])) :
			$this->db->where("i.prefecture_id LIKE '%".$search['prefecture_id']."%'");
		endif;
		if(!empty($search['country_id'])) :
			$this->db->where("p.country_id LIKE '%".$search['country_id']."%'");
		endif;
		/// endsearch

		$query = $this->db->get();

		return $query->num_rows();
	}

	public function checkcDuplicateIndustrial($name) {
		$condition = "LOWER(name) =" . "'" . $name . "'";
		$this->db->select('*');
		$this->db->from('industrial_park');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  true;
		} else {
			return false;
		}
	}

	public function getIndustrialParkById($id){
		$condition = "id =" . "'" . $id . "'";
		$this->db->select('*');
		$this->db->from('industrial_park');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function saveIndustrialPark($data) {

		$action = (isset($data['id']) && $data['id'] > 0) ? "edit" : "add";

		if ($action == "add") :
			$result = $this->db->insert('industrial_park', $data); 
		else:
			$this->db->where('id', $data['id']);
			$result = $this->db->update('industrial_park', $data); 
		endif;

		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function deleteIndustrialPark($id) {
		$sql = "DELETE FROM industrial_park WHERE id =".$id;
		$result = $this->db->query($sql);
		if ($result) {
			$sql = "DELETE FROM industrial_park_profile WHERE industrial_park_id =".$id;
			$result = $this->db->query($sql);
			if ($result) {
				return true;
			} else {
				return false;
			}
		} else {
				return false;
		}
	}

	public function getPrefectureList() {

		$condition = "";
		$this->db->select('*');
		$this->db->from('prefecture');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$country = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$country[$r->id] = $r->name;
			}
			return $country;
		} else {
			return false;
		}
	}

	public function getPrefectureProfileList() {

		$condition = "";
		$this->db->select('*');
		$this->db->from('prefecture_profile');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$country = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$country[$r->id] = $r->name;
			}
			return $country;
		} else {
			return false;
		}
	}

	public function prefecture_data($country_id) {
		$condition = "country_id =" . "'" . $country_id . "'";
		$this->db->select('*');
		$this->db->from('prefecture');
		$this->db->order_by("name", "asc");
		$this->db->where($condition);
		$query = $this->db->get();
		$category_data = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category_data[$r->id] = $r->name;
			}
			return $category_data;
		} else {
			return false;
		}
		
	}

	public function prefecture_profile_data($prefecture_id) {
		$condition = "prefecture_id =" . "'" . $prefecture_id . "'";
		$this->db->select('*');
		$this->db->from('prefecture_profile');
		$this->db->order_by("name", "asc");
		$this->db->where($condition);
		$query = $this->db->get();
		$category_data = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category_data[$r->id] = $r->name;
			}
			return $category_data;
		} else {
			return false;
		}
		
	}


	public function getCountryList() {

		$condition = "";
		$this->db->select('*');
		$this->db->from('country');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$country = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$country[$r->id] = $r->name;
			}
			return $country;
		} else {
			return false;
		}
	}

	
}

?>
