<?php

Class Admin_article_category_model extends CI_Model {


	public function getArticleCategoryList($limit, $start) {

		$this->db->select('*');
		$this->db->from('article_category');
		$this->db->order_by("name", "asc"); 
		$this->db->limit($limit, $start); 
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function countArticleCategory() {
		return $this->db->count_all_results('article_category');
	}

	public function checkcCategorybyName($name) {
		$condition = "LOWER(name) =" . "'" . $name . "'";
		$this->db->select('*');
		$this->db->from('article_category');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  true;
		} else {
			return false;
		}
	}

	public function getCategoryById($id){
		$condition = "id =" . "'" . $id . "'";
		$this->db->select('*');
		$this->db->from('article_category');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function saveCategory($data) {

		$action = (isset($data['id']) && $data['id'] > 0) ? "edit" : "add";

		if ($action == "add") :
			$result = $this->db->insert('article_category', $data); 
		else:
			$this->db->where('id', $data['id']);
			$result = $this->db->update('article_category', $data); 
		endif;

		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function deleteCategory($id) {
		$sql = "DELETE FROM article_category WHERE id =".$id;
		$result = $this->db->query($sql);
		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	
}

?>
