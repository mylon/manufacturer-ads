<?php

Class Admin_prefecture_model extends CI_Model {


	public function getPrefectureList($limit, $start, $search) {

		$this->db->select('prefecture.*, c.name as country_name');
		$this->db->from('prefecture');
		$this->db->join('country c', 'prefecture.country_id = c.id', 'left');

		/// search
		if(!empty($search['name'])) :
			$this->db->where("prefecture.name LIKE '%".$search['name']."%'");
		endif;

		if(!empty($search['country_id'])) :
			$this->db->where("prefecture.country_id LIKE '%".$search['country_id']."%'");
		endif;
		/// endsearch

		$this->db->order_by("name", "asc"); 
		$this->db->limit($limit, $start); 
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function countPrefecture($search) {
		
		$this->db->select('*');
		$this->db->from('prefecture');

		/// search
		if(!empty($search['name'])) :
			$this->db->where("name LIKE '%".$search['name']."%'");
		endif;

		if(!empty($search['country_id'])) :
			$this->db->where("country_id LIKE '%".$search['country_id']."%'");
		endif;
		/// endsearch

		$query = $this->db->get();

		return $query->num_rows();
	}

	public function checkcPrefectureName($name) {
		$condition = "LOWER(name) =" . "'" . $name . "'";
		$this->db->select('*');
		$this->db->from('prefecture');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  true;
		} else {
			return false;
		}
	}

	public function getCountryList() {

		$condition = "";
		$this->db->select('*');
		$this->db->from('country');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$country = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$country[$r->id] = $r->name;
			}
			return $country;
		} else {
			return false;
		}
	}

	public function getPrefectureById($id){
		$condition = "id =" . "'" . $id . "'";
		$this->db->select('*');
		$this->db->from('prefecture');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function savePrefecture($data) {

		$action = (isset($data['id']) && $data['id'] > 0) ? "edit" : "add";

		if ($action == "add") :
			$result = $this->db->insert('prefecture', $data); 
		else:
			$this->db->where('id', $data['id']);
			$result = $this->db->update('prefecture', $data); 
		endif;

		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function deletePrefecture($id) {
		$sql = "DELETE FROM prefecture WHERE id =".$id;
		$result = $this->db->query($sql);
		if ($result) {
			$sql = "DELETE FROM prefecture_profile WHERE prefecture_id =".$id;
			$result = $this->db->query($sql);
			if ($result) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	
}

?>
