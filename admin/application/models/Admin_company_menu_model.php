<?php

Class Admin_company_menu_model extends CI_Model {


	public function getCompanyMenuList($limit, $start, $search) {

		$this->db->select('cm.*, l.name as language_name, m.menu as menu_type, c.name as company_name');
		$this->db->from('company_menu cm');
		$this->db->join('menu m', 'm.id = cm.menu_id', 'left');
		$this->db->join('language l', 'l.id = m.language_id', 'left');
		$this->db->join('company c', 'c.id = cm.company_id', 'left');
		if(($this->session->userdata['manufacturer_staff_logged_in']['staff_type'] != 'superadmin') && 
			$this->session->userdata['manufacturer_staff_logged_in']['staff_company'] > 0) {

			$this->db->where('c.id', $this->session->userdata['manufacturer_staff_logged_in']['staff_company']);
		}

		/// search
		if(!empty($search['name'])) :
			$this->db->where("cm.name LIKE '%".$search['name']."%'");
		endif;

		if(!empty($search['company_id'])) :
			$this->db->where("cm.company_id LIKE '%".$search['company_id']."%'");
		endif;

		if(!empty($search['menu_id'])) :
			$this->db->where("cm.menu_id LIKE '%".$search['menu_id']."%'");
		endif;

		if(!empty($search['language_id'])) :
			$this->db->where("m.language_id LIKE '%".$search['language_id']."%'");
		endif;
		/// endsearch

		$this->db->order_by("cm.company_id", "asc"); 
		$this->db->order_by("cm.order_menu", "asc"); 
		$this->db->limit($limit, $start); 
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function countCompanyMenu($search) {
		
		$this->db->select('cm.*, l.name as language_name, m.menu as menu_type, c.name as company_name');
		$this->db->from('company_menu cm');
		$this->db->join('menu m', 'm.id = cm.menu_id', 'left');
		$this->db->join('language l', 'l.id = m.language_id', 'left');
		$this->db->join('company c', 'c.id = cm.company_id', 'left');
		if(($this->session->userdata['manufacturer_staff_logged_in']['staff_type'] != 'superadmin') && 
			$this->session->userdata['manufacturer_staff_logged_in']['staff_company'] > 0) {

			$this->db->where('c.id', $this->session->userdata['manufacturer_staff_logged_in']['staff_company']);
		}

		/// search
		if(!empty($search['name'])) :
			$this->db->where("cm.name LIKE '%".$search['name']."%'");
		endif;

		if(!empty($search['company_id'])) :
			$this->db->where("cm.company_id LIKE '%".$search['company_id']."%'");
		endif;

		if(!empty($search['menu_id'])) :
			$this->db->where("cm.menu_id LIKE '%".$search['menu_id']."%'");
		endif;

		if(!empty($search['language_id'])) :
			$this->db->where("m.language_id LIKE '%".$search['language_id']."%'");
		endif;
		/// endsearch

		$query = $this->db->get();

		return $query->num_rows();
	}

	public function checkMenubyName($name, $menu_id, $company_id) {
		$condition = "LOWER(name) =" . "'" . $name . "' AND menu_id=". "'" . $menu_id . "' AND company_id=". "'" . $company_id . "'";
		$this->db->select('*');
		$this->db->from('company_menu');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  true;
		} else {
			return false;
		}
	}


	public function getMenuById($id){
		$condition = "id =" . "'" . $id . "'";
		$this->db->select('*');
		$this->db->from('company_menu');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function saveMenu($data) {

		$action = (isset($data['id']) && $data['id'] > 0) ? "edit" : "add";

		if ($action == "add") :
			$result = $this->db->insert('company_menu', $data); 
		else:
			$this->db->where('id', $data['id']);
			$result = $this->db->update('company_menu', $data); 
		endif;

		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function deleteMenu($id) {
		$sql = "DELETE FROM company_menu WHERE id =".$id;
		$result = $this->db->query($sql);
		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function getLanugageList() {

		$this->db->select('*');
		$this->db->from('language');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$country = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$country[$r->id] = $r->name;
			}
			return $country;
		} else {
			return false;
		}
	}

	public function getCompanyList() {

		$this->db->select('*');
		$this->db->from('company');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$country = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$country[$r->id] = $r->name;
			}
			return $country;
		} else {
			return false;
		}
	}

	public function getMenuList() {

		$this->db->select('*');
		$this->db->from('menu');
		$this->db->order_by("menu", "asc");
		$query = $this->db->get();
		$country = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$country[$r->id] = $r->menu;
			}
			return $country;
		} else {
			return false;
		}
	}

	
}

?>
