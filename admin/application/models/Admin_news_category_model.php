<?php

Class Admin_news_category_model extends CI_Model {


	public function getNewsCategoryList($limit, $start, $search) {

		$this->db->select('news_category.*, c.name as company_name');
		$this->db->from('news_category');
		$this->db->join('company c', 'c.id = news_category.company_id', 'left');
		if(($this->session->userdata['manufacturer_staff_logged_in']['staff_type'] != 'superadmin') && 
			$this->session->userdata['manufacturer_staff_logged_in']['staff_company'] > 0) {

			$this->db->where('news_category.company_id', $this->session->userdata['manufacturer_staff_logged_in']['staff_company']);
		}

		/// search
		if(!empty($search['name'])) :
			$this->db->where("news_category.name LIKE '%".$search['name']."%'");
		endif;

		if(!empty($search['company_id'])) :
			$this->db->where('news_category.company_id', $search['company_id']);
		endif;
		/// endsearch
		
		$this->db->order_by("name", "asc"); 
		$this->db->limit($limit, $start); 
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function countNewsCategory($search) {
		

		$this->db->select('*');
		$this->db->from('news_category');

		if(($this->session->userdata['manufacturer_staff_logged_in']['staff_type'] != 'superadmin') && 
			$this->session->userdata['manufacturer_staff_logged_in']['staff_company'] > 0) {

			$this->db->where('news_category.company_id', $this->session->userdata['manufacturer_staff_logged_in']['staff_company']);
		}

		/// search
		if(!empty($search['name'])) :
			$this->db->where("name LIKE '%".$search['name']."%'");
		endif;

		if(!empty($search['company_id'])) :
			$this->db->where('company_id', $search['company_id']);
		endif;
		/// endsearch

		$query = $this->db->get();

		return $query->num_rows();
	}

	public function checkcCategorybyName($name, $company_id) {
		$condition = "LOWER(name) =" . "'" . $name . "' AND company_id = "."'".$company_id."'";
		$this->db->select('*');
		$this->db->from('news_category');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  true;
		} else {
			return false;
		}
	}

	public function getCategoryById($id){
		$condition = "id =" . "'" . $id . "'";
		$this->db->select('*');
		$this->db->from('news_category');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function getCompanyList() {

		$this->db->select('id, name');
		$this->db->from('company');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$category = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category[$r->id] = $r->name;
			}
			return $category;
		} else {
			return false;
		}
	}

	public function saveCategory($data) {

		$action = (isset($data['id']) && $data['id'] > 0) ? "edit" : "add";

		if ($action == "add") :
			$result = $this->db->insert('news_category', $data); 
		else:
			$this->db->where('id', $data['id']);
			$result = $this->db->update('news_category', $data); 
		endif;

		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function deleteCategory($id) {
		$sql = "DELETE FROM news_category WHERE id =".$id;
		$result = $this->db->query($sql);
		if ($result) {
			$sql = "DELETE FROM news_category_profile WHERE news_category_id =".$id;
			$result = $this->db->query($sql);
			if ($result) {
				return true;
			} else {
				return false;
			}
		} else {
				return false;
		}
	}

	
}

?>
