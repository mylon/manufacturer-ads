<?php

Class Admin_company_profile_model extends CI_Model {


	public function getCompanyProfileList($limit, $start, $search) {

		$this->db->select('company_profile.*, l.name as language_name, cy.name as company_name, s1.username as created_by, s2.username as edited_by, cc.name as category_name, c.name as country_name, p.name as prefecture_name, i.name as industrial_park_name');
		$this->db->from('company_profile');
		$this->db->join('company cy', 'cy.id = company_profile.company_id', 'left');
		$this->db->join('staff s1', 's1.id = company_profile.created_by', 'left');
		$this->db->join('staff s2', 's2.id = company_profile.edited_by', 'left');
		$this->db->join('company_category cc', 'cc.id = company_profile.category_id', 'left');
		$this->db->join('country c', 'c.id = company_profile.country_id', 'left');
		$this->db->join('prefecture p', 'p.id = company_profile.prefecture_id', 'left');
		$this->db->join('industrial_park i', 'i.id = company_profile.industrial_park_id', 'left');
		$this->db->join('language l', 'l.id = company_profile.language_id', 'left');
		if(($this->session->userdata['manufacturer_staff_logged_in']['staff_type'] != 'superadmin') && 
			$this->session->userdata['manufacturer_staff_logged_in']['staff_company'] > 0) {

			$this->db->where('company_profile.company_id', $this->session->userdata['manufacturer_staff_logged_in']['staff_company']);
		}

		/// search
		if(!empty($search['name'])) :
			$this->db->where("company_profile.name LIKE '%".$search['name']."%'");
		endif;

		if(!empty($search['category_id'])) :
			$this->db->where('company_profile.category_id', $search['category_id']);
		endif;

		if(!empty($search['company_id'])) :
			$this->db->where('company_profile.company_id', $search['company_id']);
		endif;

		if(!empty($search['country_id'])) :
			$this->db->where('company_profile.country_id', $search['country_id']);
		endif;

		if(!empty($search['created_by'])) :
			$this->db->where('company_profile.created_by', $search['created_by']);
		endif;

		if(!empty($search['prefecture_id'])) :
			$this->db->where('company_profile.prefecture_id', $search['prefecture_id']);
		endif;

		if(!empty($search['industrial_park_id'])) :
			$this->db->where('company_profile.industrial_park_id', $search['industrial_park_id']);
		endif;

		if(!empty($search['language_id'])) :
			$this->db->where('company_profile.language_id', $search['language_id']);
		endif;
		/// endsearch

		$this->db->order_by("cy.name", "asc"); 
		$this->db->order_by("company_profile.name", "asc"); 
		$this->db->limit($limit, $start); 
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function countCompanyProfile($search) {

		$this->db->select('*');
		$this->db->from('company_profile');

		if(($this->session->userdata['manufacturer_staff_logged_in']['staff_type'] != 'superadmin') && 
			$this->session->userdata['manufacturer_staff_logged_in']['staff_company'] > 0) {

			$this->db->where('company_profile.company_id', $this->session->userdata['manufacturer_staff_logged_in']['staff_company']);
		}

		/// search
		if(!empty($search['name'])) :
			$this->db->where("name LIKE '%".$search['name']."%'");
		endif;

		if(!empty($search['company_id'])) :
			$this->db->where('company_profile.company_id', $search['company_id']);
		endif;


		if(!empty($search['category_id'])) :
			$this->db->where('category_id', $search['category_id']);
		endif;

		if(!empty($search['country_id'])) :
			$this->db->where('country_id', $search['country_id']);
		endif;

		if(!empty($search['created_by'])) :
			$this->db->where('created_by', $search['created_by']);
		endif;

		if(!empty($search['prefecture_id'])) :
			$this->db->where('prefecture_id', $search['prefecture_id']);
		endif;

		if(!empty($search['industrial_park_id'])) :
			$this->db->where('industrial_park_id', $search['industrial_park_id']);
		endif;

		if(!empty($search['language_id'])) :
			$this->db->where('language_id', $search['language_id']);
		endif;
		/// endsearch

		$query = $this->db->get();

		return $query->num_rows();
	}

	public function checkCompanyProfilebyName($name) {
		$condition = "LOWER(name) =" . "'" . strtolower($name) . "'";
		$this->db->select('*');
		$this->db->from('company_profile');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  true;
		} else {
			return false;
		}
	}

	public function getCompanyProfileById($id){
		$condition = "id =" . "'" . $id . "'";
		$this->db->select('*');
		$this->db->from('company_profile');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function getCompanyList(){

		$this->db->select('*');
		$this->db->from('company');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$category = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category[$r->id] = $r->name;
			}
			return $category;
		} else {
			return false;
		}
	}

	public function getCompanyCategoryList() {

		$this->db->select('*');
		$this->db->from('company_category');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$category = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category[$r->id] = $r->name;
			}
			return $category;
		} else {
			return false;
		}
	}

	public function getSubCompanyCategoryList() {

		$this->db->select('*');
		$this->db->from('sub_company_category');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$category = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category[$r->id] = $r->name;
			}
			return $category;
		} else {
			return false;
		}
	}

	public function getStaffList() {

		$this->db->select('id, username');
		$this->db->from('staff');
		$this->db->order_by("username", "asc");
		$query = $this->db->get();
		$category = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category[$r->id] = $r->username;
			}
			return $category;
		} else {
			return false;
		}
	}

	public function getCountryList() {

		$this->db->select('*');
		$this->db->from('country');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$country = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$country[$r->id] = $r->name;
			}
			return $country;
		} else {
			return false;
		}
	}

	public function getLanugageList() {

		$this->db->select('*');
		$this->db->from('language');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$country = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$country[$r->id] = $r->name;
			}
			return $country;
		} else {
			return false;
		}
	}

	public function getPrefectureList() {

		$this->db->select('*');
		$this->db->from('prefecture');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$country = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$country[$r->id] = $r->name;
			}
			return $country;
		} else {
			return false;
		}
	}

	public function getPrefectureProfileList() {

		$this->db->select('*');
		$this->db->from('prefecture_profile');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$country = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$country[$r->id] = $r->name;
			}
			return $country;
		} else {
			return false;
		}
	}

	public function getIndustrialParkList() {

		$this->db->select('*');
		$this->db->from('industrial_park');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$country = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$country[$r->id] = $r->name;
			}
			return $country;
		} else {
			return false;
		}
	}

	public function category_data($company_id) {
		$condition = "company_id =" . "'" . $company_id . "'";
		$this->db->select('*');
		$this->db->from('company_category');
		$this->db->order_by("name", "asc");
		$this->db->where($condition);
		$query = $this->db->get();
		$category_data = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category_data[$r->id] = $r->name;
			}
			return $category_data;
		} else {
			return false;
		}
		
	}

	public function sub_category_data($company_category_id) {
		$condition = "company_category_id =" . "'" . $company_category_id . "'";
		$this->db->select('*');
		$this->db->from('sub_company_category');
		$this->db->order_by("name", "asc");
		$this->db->where($condition);
		$query = $this->db->get();
		$prefecture = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$prefecture[$r->id] = $r->name;
			}
			return $prefecture;
		} else {
			return false;
		}
		
	}

	public function prefecture_data($country_id) {
		$condition = "country_id =" . "'" . $country_id . "'";
		$this->db->select('*');
		$this->db->from('prefecture');
		$this->db->order_by("name", "asc");
		$this->db->where($condition);
		$query = $this->db->get();
		$prefecture = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$prefecture[$r->id] = $r->name;
			}
			return $prefecture;
		} else {
			return false;
		}
		
	}


	public function getSubcategorylist($company_id) {
		$condition = "company_id =" . "'" . $company_id . "'";
		$this->db->select('*');
		$this->db->from('company_sub_category');
		$this->db->order_by("sub_category_id", "asc");
		$this->db->where($condition);
		$query = $this->db->get();
		$prefecture = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$prefecture[$r->id] = $r->sub_category_id;
			}
			return $prefecture;
		} else {
			return false;
		}
		
	}

	public function industrial_park_data($prefecture_id) {
		$condition = "prefecture_id = '". $prefecture_id . "'";
		$this->db->select('*');
		$this->db->from('industrial_park');
		$this->db->order_by("name", "asc");
		$this->db->where($condition);
		$query = $this->db->get();
		$industrial_park = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$industrial_park[$r->id] = $r->name;
			}
			return $industrial_park;
		} else {
			return false;
		}
		
	}

	public function saveCompany($data, $sub_category_id_list) {

		$action = (isset($data['id']) && $data['id'] > 0) ? "edit" : "add";

		if ($action == "add") :
			$result = $this->db->insert('company_profile', $data); 
			$id = $this->db->insert_id();
		else:
			$this->db->where('id', $data['id']);
			$result = $this->db->update('company_profile', $data); 
			$id = $data['id'];
		endif;

		if(!empty($sub_category_id_list) && !is_null($sub_category_id_list)) {
			$sql = "DELETE FROM company_sub_category WHERE company_id =".$id;
			$result = $this->db->query($sql);
			if ($result) {
				foreach ($sub_category_id_list as $scl_id) {
					$sub_data['company_id'] = $id;
					$sub_data['sub_category_id'] = $scl_id;
					$result = $this->db->insert('company_sub_category', $sub_data); 
				}
			} 
		}

		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	

	public function deleteCompany($id) {
		$sql = "DELETE FROM company_profile WHERE id =".$id;
		$result = $this->db->query($sql);
		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	
}

?>
