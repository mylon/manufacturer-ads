<?php

Class Admin_country_model extends CI_Model {


	public function getCountryList($limit, $start, $search) {

		$this->db->select('*');
		$this->db->from('country');

		/// search
		if(!empty($search['name'])) :
			$this->db->where("name LIKE '%".$search['name']."%'");
		endif;
		/// endsearch


		$this->db->order_by("name", "asc"); 
		$this->db->limit($limit, $start); 
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function countCountry($search) {
		$this->db->select('*');
		$this->db->from('country');

		/// search
		if(!empty($search['name'])) :
			$this->db->where("name LIKE '%".$search['name']."%'");
		endif;
		/// endsearch

		$query = $this->db->get();

		return $query->num_rows();
	}

	public function checkcCountrybyName($name) {
		$condition = "LOWER(name) =" . "'" . $name . "'";
		$this->db->select('*');
		$this->db->from('country');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  true;
		} else {
			return false;
		}
	}

	public function checkcCountrybyCode($code) {
		$condition = "LOWER(code) =" . "'" . $code . "'";
		$this->db->select('*');
		$this->db->from('country');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  true;
		} else {
			return false;
		}
	}

	public function getCountryById($id){
		$condition = "id =" . "'" . $id . "'";
		$this->db->select('*');
		$this->db->from('country');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function saveCountry($data) {

		$action = (isset($data['id']) && $data['id'] > 0) ? "edit" : "add";

		if ($action == "add") :
			$result = $this->db->insert('country', $data); 
		else:
			$this->db->where('id', $data['id']);
			$result = $this->db->update('country', $data); 
		endif;

		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function deleteCountry($id) {
		$sql = "DELETE FROM country WHERE id =".$id;
		$result = $this->db->query($sql);
		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	
}

?>
