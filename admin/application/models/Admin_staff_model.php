<?php

Class Admin_staff_model extends CI_Model {


	public function getStaffList($limit, $start) {

		$this->db->select('*');
		$this->db->from('staff');
		$this->db->order_by("username", "asc"); 
		$this->db->limit($limit, $start); 
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function countStaff() {
		return $this->db->count_all_results('staff');
	}

	public function checkStaffbyUserName($username) {
		$condition = "LOWER(username) =" . "'" . $username . "'";
		$this->db->select('*');
		$this->db->from('staff');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  true;
		} else {
			return false;
		}
	}

	public function getStaffById($id){
		$condition = "id =" . "'" . $id . "'";
		$this->db->select('*');
		$this->db->from('staff');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function getCompanyList(){
		$this->db->select('*');
		$this->db->from('company');
		$this->db->order_by("name", "asc"); 
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}


	public function saveStaff($data) {

		$action = (isset($data['id']) && $data['id'] > 0) ? "edit" : "add";

		if ($action == "add") :
			$result = $this->db->insert('staff', $data); 
		else:
			$this->db->where('id', $data['id']);
			$result = $this->db->update('staff', $data); 
		endif;

		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function deleteStaff($id) {
		$sql = "DELETE FROM staff WHERE id =".$id;
		$result = $this->db->query($sql);
		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	
}

?>
