<?php

Class Admin_company_products_model extends CI_Model {
	
	public function getProductList($limit, $start, $search) {
		$this->db->select('company_products.*, pc.name as category_name, c.name as company_name, s1.username as created_by, s2.username as edited_by');
		$this->db->from('company_products');
		$this->db->join('staff s1', 's1.id = company_products.created_by', 'left');
		$this->db->join('staff s2', 's2.id = company_products.edited_by', 'left');
		$this->db->join('company c', 'c.id = company_products.company_id', 'left');
		$this->db->join('products_category pc', 'pc.id = company_products.category_id', 'left');
		if(($this->session->userdata['manufacturer_staff_logged_in']['staff_type'] != 'superadmin') && 
			$this->session->userdata['manufacturer_staff_logged_in']['staff_company'] > 0) {

			$this->db->where('company_products.company_id', $this->session->userdata['manufacturer_staff_logged_in']['staff_company']);
		}

		/// search
		if(!empty($search['name'])) :
			$this->db->where("company_products.name LIKE '%".$search['name']."%'");
		endif;

		if(!empty($search['start_date'])) :
			$this->db->where("created_date >= '".$search['start_date']. ' 00:00:00' ."'");
		endif;

		if(!empty($search['end_date'])) :
			$this->db->where("created_date < '".$search['end_date']. ' 23:59:59' ."'");
		endif;

		if(!empty($search['created_by'])) :
			$this->db->where('created_by', $search['created_by']);
		endif;

		if(!empty($search['company_id'])) :
			$this->db->where('company_products.company_id', $search['company_id']);
		endif;

		if(!empty($search['category_id'])) :
			$this->db->where('company_products.category_id', $search['category_id']);
		endif;
		/// endsearch

		$this->db->order_by("created_date", "desc"); 
		$this->db->limit($limit, $start); 
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function countProducts($search) {

		$this->db->select('*');
		$this->db->from('company_products');

		if(($this->session->userdata['manufacturer_staff_logged_in']['staff_type'] != 'superadmin') && 
			$this->session->userdata['manufacturer_staff_logged_in']['staff_company'] > 0) {

			$this->db->where('company_products.company_id', $this->session->userdata['manufacturer_staff_logged_in']['staff_company']);
		}

		/// search
		if(!empty($search['name'])) :
			$this->db->where("name LIKE '%".$search['name']."%'");
		endif;

		if(!empty($search['start_date'])) :
			$this->db->where("created_date >= '".$search['start_date']. ' 00:00:00' ."'");
		endif;

		if(!empty($search['end_date'])) :
			$this->db->where("created_date < '".$search['end_date']. ' 23:59:59' ."'");
		endif;

		if(!empty($search['created_by'])) :
			$this->db->where('created_by', $search['created_by']);
		endif;

		if(!empty($search['company_id'])) :
			$this->db->where('company_id', $search['company_id']);
		endif;

		if(!empty($search['category_id'])) :
			$this->db->where('category_id', $search['category_id']);
		endif;
		/// endsearch

		$query = $this->db->get();

		return $query->num_rows();
	}

	public function category_data($company_id) {
		$condition = "company_id =" . "'" . $company_id . "'";
		$this->db->select('*');
		$this->db->from('products_category');
		$this->db->order_by("name", "asc");
		$this->db->where($condition);
		$query = $this->db->get();
		$category_data = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category_data[$r->id] = $r->name;
			}
			return $category_data;
		} else {
			return false;
		}
		
	}


	public function getProductById($id){
		$condition = "id =" . "'" . $id . "'";
		$this->db->select('*');
		$this->db->from('company_products');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function getCategoryList() {

		$this->db->select('id, name');
		$this->db->from('products_category');
		if(($this->session->userdata['manufacturer_staff_logged_in']['staff_type'] != 'superadmin') && 
			$this->session->userdata['manufacturer_staff_logged_in']['staff_company'] > 0) {

			$this->db->where('products_category.company_id', $this->session->userdata['manufacturer_staff_logged_in']['staff_company']);
		}
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$category = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category[$r->id] = $r->name;
			}
			return $category;
		} else {
			return false;
		}
	}

	public function getCompanyList() {

		$this->db->select('id, name');
		$this->db->from('company');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$category = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category[$r->id] = $r->name;
			}
			return $category;
		} else {
			return false;
		}
	}

	public function getStaffList() {

		$this->db->select('id, username');
		$this->db->from('staff');
		$this->db->order_by("username", "asc");
		$query = $this->db->get();
		$category = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category[$r->id] = $r->username;
			}
			return $category;
		} else {
			return false;
		}
	}


	public function saveProduct($data) {

		$action = (isset($data['id']) && $data['id'] > 0) ? "edit" : "add";

		if ($action == "add") :
			$result = $this->db->insert('company_products', $data); 
		else:
			$this->db->where('id', $data['id']);
			$result = $this->db->update('company_products', $data); 
		endif;

		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function deleteProduct($id) {
		$sql = "DELETE FROM company_products WHERE id =".$id;
		$result = $this->db->query($sql);
		if ($result) {
			$sql = "DELETE FROM company_products_profile WHERE company_products_id =".$id;
			$result = $this->db->query($sql);
			if ($result) {
				return true;
			} else {
				return false;
			}
		} else {
				return false;
		}
	}

	public function getProductImage($id){
        $condition = "id =" . "'" . $id . "'";
		$this->db->select('images');
		$this->db->from('company_products');
		$this->db->where($condition);
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return $query->result();
		} else {
			return false;
		}
    }

    public function deleteProductImage($id) {

		$this->db->where('id', $id);
		$data['images'] = NULL;
		$result = $this->db->update('company_products', $data); 

		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function getProductDescriptionImages($product_id){
        $condition = "product_id =" . "'" . $product_id . "'";
		$this->db->select('*');
		$this->db->from('products_description_images');
		$this->db->where($condition);
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return $query->result();
		} else {
			return false;
		}
    }

    public function deleteProductDescriptionImage($id) {

		$sql = "DELETE FROM products_description_images WHERE id =".$id;
		$result = $this->db->query($sql);
		if ($result) {
			return true;
		} else {
			return false;
		}
	}
	
}

?>
