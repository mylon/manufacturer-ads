<?php

Class Admin_company_products_profile_model extends CI_Model {
	
	public function getProductProfileList($limit, $start, $search) {
		$this->db->select('cpp.*, pc.name as category_name, cp.name as product_name, c.name as company_name, l.name as language_name, s1.username as created_by, s2.username as edited_by, pc.name as category_name');
		$this->db->from('company_products_profile cpp');
		$this->db->join('staff s1', 's1.id = cpp.created_by', 'left');
		$this->db->join('staff s2', 's2.id = cpp.edited_by', 'left');
		$this->db->join('language l', 'l.id = cpp.language_id', 'left');
		$this->db->join('company_products cp', 'cp.id = cpp.company_products_id', 'left');
		$this->db->join('company c', 'c.id = cp.company_id', 'left');
		$this->db->join('products_category pc', 'pc.id = cp.category_id', 'left');
		if(($this->session->userdata['manufacturer_staff_logged_in']['staff_type'] != 'superadmin') && 
			$this->session->userdata['manufacturer_staff_logged_in']['staff_company'] > 0) {

			$this->db->where('c.id', $this->session->userdata['manufacturer_staff_logged_in']['staff_company']);
		}

		/// search
		if(!empty($search['name'])) :
			$this->db->where("cpp.name LIKE '%".$search['name']."%'");
		endif;

		if(!empty($search['start_date'])) :
			$this->db->where("cpp.created_date >= '".$search['start_date']. ' 00:00:00' ."'");
		endif;

		if(!empty($search['end_date'])) :
			$this->db->where("cpp.created_date < '".$search['end_date']. ' 23:59:59' ."'");
		endif;

		if(!empty($search['created_by'])) :
			$this->db->where('cpp.created_by', $search['created_by']);
		endif;

		if(!empty($search['company_id'])) :
			$this->db->where('cp.company_id', $search['company_id']);
		endif;

		if(!empty($search['category_id'])) :
			$this->db->where('cp.category_id', $search['category_id']);
		endif;

		if(!empty($search['language_id'])) :
			$this->db->where('cpp.language_id', $search['language_id']);
		endif;
		/// endsearch

		$this->db->order_by("cpp.created_date", "desc"); 
		$this->db->limit($limit, $start); 
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function countProductsProfile($search) {

		$this->db->select('*');
		$this->db->from('company_products_profile');
		$this->db->join('company_products cp', 'cp.id = company_products_profile.company_products_id', 'left');
		$this->db->join('company c', 'c.id = cp.company_id', 'left');
		$this->db->join('products_category pc', 'pc.id = cp.category_id', 'left');	
		if(($this->session->userdata['manufacturer_staff_logged_in']['staff_type'] != 'superadmin') && 
			$this->session->userdata['manufacturer_staff_logged_in']['staff_company'] > 0) {

			$this->db->where('c.id', $this->session->userdata['manufacturer_staff_logged_in']['staff_company']);
		}

		/// search
		if(!empty($search['name'])) :
			$this->db->where("company_products_profile.name LIKE '%".$search['name']."%'");
		endif;

		if(!empty($search['start_date'])) :
			$this->db->where("company_products_profile.created_date >= '".$search['start_date']. ' 00:00:00' ."'");
		endif;

		if(!empty($search['end_date'])) :
			$this->db->where("company_products_profile.created_date < '".$search['end_date']. ' 23:59:59' ."'");
		endif;

		if(!empty($search['created_by'])) :
			$this->db->where('company_products_profile.created_by', $search['created_by']);
		endif;

		if(!empty($search['company_id'])) :
			$this->db->where('cp.company_id', $search['company_id']);
		endif;

		if(!empty($search['category_id'])) :
			$this->db->where('cp.category_id', $search['category_id']);
		endif;

		if(!empty($search['language_id'])) :
			$this->db->where('company_products_profile.language_id', $search['language_id']);
		endif;
		/// endsearch

		$query = $this->db->get();

		return $query->num_rows();
	}

	public function getLanguageList() {

		$this->db->select('id, name');
		$this->db->from('language');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$category = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category[$r->id] = $r->name;
			}
			return $category;
		} else {
			return false;
		}
	}
	

	public function getProductProfileById($id){
		$condition = "id =" . "'" . $id . "'";
		$this->db->select('*');
		$this->db->from('company_products_profile');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function getCompanyList() {

		$this->db->select('id, name');
		$this->db->from('company');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$category = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category[$r->id] = $r->name;
			}
			return $category;
		} else {
			return false;
		}
	}

	public function getProductList() {

		$this->db->select('id, name');
		$this->db->from('company_products');
		if(($this->session->userdata['manufacturer_staff_logged_in']['staff_type'] != 'superadmin') && 
			$this->session->userdata['manufacturer_staff_logged_in']['staff_company'] > 0) {

			$this->db->where('company_products.company_id', $this->session->userdata['manufacturer_staff_logged_in']['staff_company']);
		}
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$category = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category[$r->id] = $r->name;
			}
			return $category;
		} else {
			return false;
		}
	}

	public function getCategoryList() {

		$this->db->select('id, name');
		$this->db->from('products_category');
		if(($this->session->userdata['manufacturer_staff_logged_in']['staff_type'] != 'superadmin') && 
			$this->session->userdata['manufacturer_staff_logged_in']['staff_company'] > 0) {

			$this->db->where('products_category.company_id', $this->session->userdata['manufacturer_staff_logged_in']['staff_company']);
		}
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$category = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category[$r->id] = $r->name;
			}
			return $category;
		} else {
			return false;
		}
	}

	public function category_data($company_id) {
		$condition = "company_id =" . "'" . $company_id . "'";
		$this->db->select('*');
		$this->db->from('products_category');
		$this->db->order_by("name", "asc");
		$this->db->where($condition);
		$query = $this->db->get();
		$category_data = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category_data[$r->id] = $r->name;
			}
			return $category_data;
		} else {
			return false;
		}
		
	}

	public function getProductCategoryList() {

		$this->db->select('id, name');
		$this->db->from('products_category');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$category = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category[$r->id] = $r->name;
			}
			return $category;
		} else {
			return false;
		}
	}

	public function getStaffList() {

		$this->db->select('id, username');
		$this->db->from('staff');
		$this->db->order_by("username", "asc");
		$query = $this->db->get();
		$category = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category[$r->id] = $r->username;
			}
			return $category;
		} else {
			return false;
		}
	}


	public function saveProduct($data) {

		$action = (isset($data['id']) && $data['id'] > 0) ? "edit" : "add";

		if ($action == "add") :
			$result = $this->db->insert('company_products_profile', $data); 
		else:
			$this->db->where('id', $data['id']);
			$result = $this->db->update('company_products_profile', $data); 
		endif;

		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function deleteProduct($id) {
		$sql = "DELETE FROM company_products_profile WHERE id =".$id;
		$result = $this->db->query($sql);
		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function getProductProfilePDF($product_profile_id){
        $condition = "product_profile_id =" . "'" . $product_profile_id . "'";
		$this->db->select('*');
		$this->db->from('products_profile_pdf');
		$this->db->where($condition);
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return $query->result();
		} else {
			return false;
		}
    }

     public function deleteProductProfileBrochure($id) {

		$sql = "DELETE FROM products_profile_pdf WHERE id =".$id;
		$result = $this->db->query($sql);
		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	
}

?>
