<?php

Class Admin_company_model extends CI_Model {


	public function getCompanyList($limit, $start, $search) {

		$this->db->select('company.*, ct.type as type_name');
		$this->db->from('company');
		$this->db->join('company_type ct', 'ct.id = company.type', 'left');
		if(($this->session->userdata['manufacturer_staff_logged_in']['staff_type'] != 'superadmin') && 
			$this->session->userdata['manufacturer_staff_logged_in']['staff_company'] > 0) {

			$this->db->where('company.id', $this->session->userdata['manufacturer_staff_logged_in']['staff_company']);
		}

		/// search
		if(!empty($search['name'])) :
			$this->db->where("company.name LIKE '%".$search['name']."%'");
		endif;

		if(!empty($search['type_id'])) :
			$this->db->where("company.type LIKE '%".$search['type']."%'");
		endif;
		/// endsearch

		$this->db->order_by("company.name", "asc"); 
		$this->db->limit($limit, $start); 
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function countCompany($search) {

		$this->db->select('company.*, ct.type as type_name');
		$this->db->from('company');
		$this->db->join('company_type ct', 'ct.id = company.type', 'left');
		if(($this->session->userdata['manufacturer_staff_logged_in']['staff_type'] != 'superadmin') && 
			$this->session->userdata['manufacturer_staff_logged_in']['staff_company'] > 0) {
			$this->db->where('company.id', $this->session->userdata['manufacturer_staff_logged_in']['staff_company'] );
		}

		/// search
		if(!empty($search['name'])) :
			$this->db->where("company.name LIKE '%".$search['name']."%'");
		endif;

		if(!empty($search['type_id'])) :
			$this->db->where("company.type LIKE '%".$search['type']."%'");
		endif;
		/// endsearch

		$query = $this->db->get();

		return $query->num_rows();
	}

	public function checkCompanybyName($name) {
		$condition = "LOWER(name) =" . "'" . strtolower($name) . "'";
		$this->db->select('*');
		$this->db->from('company');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  true;
		} else {
			return false;
		}
	}

	public function getCompanyById($id){
		$condition = "id =" . "'" . $id . "'";
		$this->db->select('*');
		$this->db->from('company');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  $query->result();
		} else {
			return false;
		}
	}


	public function saveCompany($data) {

		$action = (isset($data['id']) && $data['id'] > 0) ? "edit" : "add";

		if ($action == "add") :
			$result = $this->db->insert('company', $data); 
		else:
			$this->db->where('id', $data['id']);
			$result = $this->db->update('company', $data); 
		endif;

		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function deleteCompany($id) {
		$sql = "DELETE FROM company WHERE id =".$id;
		$result = $this->db->query($sql);
		if ($result) {
			$sql = "DELETE FROM company_profile WHERE company_id =".$id;
			$result = $this->db->query($sql);
			if ($result) {
				$sql = "DELETE FROM company_images WHERE company_id =".$id;
				$result = $this->db->query($sql);
				if ($result) {
					$sql = "DELETE FROM company_article WHERE company_id =".$id;
					$result = $this->db->query($sql);
					if ($result) {
						return true;
					} else {
						return false;
					}
				} else {
					return false;
				}
			} else {
					return false;
			}
		} else {
			return false;
		}
	}

	public function getLogoImage($id){
        $condition = "id =" . "'" . $id . "'";
		$this->db->select('logo, logo_link');
		$this->db->from('company');
		$this->db->where($condition);
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return $query->result();
		} else {
			return false;
		}
    }

    public function deleteCompnayLogo($id) {

		$this->db->where('id', $id);
		$data['logo'] = "";
		$data['logo_link'] = "";
		$result = $this->db->update('company', $data); 

		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function deleteCompnayImages($id) {

		$sql = "DELETE FROM company_images WHERE id =".$id;
		$result = $this->db->query($sql);
		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function getCompanyImages($company_id){
        $condition = "company_id =" . "'" . $company_id . "'";
		$this->db->select('*');
		$this->db->from('company_images');
		$this->db->where($condition);
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return $query->result();
		} else {
			return false;
		}
    }

    public function getCompanyType() {

		$condition = "";
		$this->db->select('*');
		$this->db->from('company_type');
		$this->db->order_by("type", "asc");
		$query = $this->db->get();
		$country = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$country[$r->id] = $r->type;
			}
			return $country;
		} else {
			return false;
		}
	}
	

	
}

?>
