<?php

Class Admin_company_category_profile_model extends CI_Model {


	public function getCompanyCategoryProfileList($limit, $start, $search) {

		$this->db->select('ccp.*, cc.name as category_name, l.name as language_name');
		$this->db->from('company_category_profile ccp');
		$this->db->join('language l', 'l.id = ccp.language_id', 'left');
		$this->db->join('company_category cc', 'cc.id = ccp.company_category_id', 'left');


		/// search
		if(!empty($search['name'])) :
			$this->db->where("ccp.name LIKE '%".$search['name']."%'");
		endif;

		if(!empty($search['language_id'])) :
			$this->db->where("ccp.language_id LIKE '%".$search['language_id']."%'");
		endif;

		if(!empty($search['company_category_id'])) :
			$this->db->where("ccp.company_category_id LIKE '%".$search['company_category_id']."%'");
		endif;
		/// endsearch

		$this->db->order_by("name", "asc"); 
		$this->db->limit($limit, $start); 
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function countCompanyCategoryProfile($search) {

		$this->db->select('ccp.*, cc.name as category_name, l.name as language_name');
		$this->db->from('company_category_profile ccp');
		$this->db->join('language l', 'l.id = ccp.language_id', 'left');
		$this->db->join('company_category cc', 'cc.id = ccp.company_category_id', 'left');

		/// search
		if(!empty($search['name'])) :
			$this->db->where("ccp.name LIKE '%".$search['name']."%'");
		endif;

		if(!empty($search['language_id'])) :
			$this->db->where("ccp.language_id LIKE '%".$search['language_id']."%'");
		endif;

		if(!empty($search['company_category_id'])) :
			$this->db->where("ccp.company_category_id LIKE '%".$search['company_category_id']."%'");
		endif;
		/// endsearch

		$query = $this->db->get();

		return $query->num_rows();
	}

	public function checkCompanyCategorybyName($name, $company_category_id, $language_id) {
		$condition = "LOWER(name) =" . "'" . $name . "' AND company_category_id =" . "'" . $company_category_id . "' AND language_id =" . "'" . $language_id . "'";
		$this->db->select('*');
		$this->db->from('company_category_profile');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  true;
		} else {
			return false;
		}
	}

	public function getCompanyList() {

		$this->db->select('*');
		$this->db->from('company');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$country = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$country[$r->id] = $r->name;
			}
			return $country;
		} else {
			return false;
		}
	}

	public function category_data($company_id) {
		$condition = "company_id =" . "'" . $company_id . "'";
		$this->db->select('*');
		$this->db->from('company_category');
		$this->db->order_by("name", "asc");
		$this->db->where($condition);
		$query = $this->db->get();
		$category_data = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category_data[$r->id] = $r->name;
			}
			return $category_data;
		} else {
			return false;
		}
		
	}

	public function getLanguageList() {

		$this->db->select('*');
		$this->db->from('language');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$country = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$country[$r->id] = $r->name;
			}
			return $country;
		} else {
			return false;
		}
	}

	public function getCompanyCategoryList() {

		$this->db->select('*');
		$this->db->from('company_category');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$country = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$country[$r->id] = $r->name;
			}
			return $country;
		} else {
			return false;
		}
	}

	public function getCompanyCategoryProfileById($id){
		$condition = "id =" . "'" . $id . "'";
		$this->db->select('*');
		$this->db->from('company_category_profile');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function saveCompanyCategory($data) {

		$action = (isset($data['id']) && $data['id'] > 0) ? "edit" : "add";

		if ($action == "add") :
			$result = $this->db->insert('company_category_profile', $data); 
		else:
			$this->db->where('id', $data['id']);
			$result = $this->db->update('company_category_profile', $data); 
		endif;

		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function deleteCompanyCategory($id) {
		$sql = "DELETE FROM company_category_profile WHERE id =".$id;
		$result = $this->db->query($sql);
		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	
}

?>
