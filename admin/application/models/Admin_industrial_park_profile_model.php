<?php

Class Admin_industrial_park_profile_model extends CI_Model {


	public function getIndustrialParkProfileList($limit, $start, $search) {

		$this->db->select('industrial_park_profile.*, l.name as language_name , i.name as industrial_park_name ,c.name as country_name, p.name as prefecture_name');
		$this->db->from('industrial_park_profile');
		$this->db->join('industrial_park i', 'industrial_park_profile.industrial_park_id = i.id', 'left');
		$this->db->join('prefecture p', 'i.prefecture_id = p.id', 'left');
		$this->db->join('country c', 'p.country_id = c.id', 'left');
		$this->db->join('language l', 'industrial_park_profile.language_id = l.id', 'left');


		/// search
		if(!empty($search['name'])) :
			$this->db->where("industrial_park_profile.name LIKE '%".$search['name']."%'");
		endif;

		if(!empty($search['language_id'])) :
			$this->db->where('industrial_park_profile.language_id', $search['language_id']);
		endif;

		if(!empty($search['country_id'])) :
			$this->db->where('p.country_id', $search['country_id']);
		endif;

		if(!empty($search['industrial_park_id'])) :
			$this->db->where('industrial_park_profile.industrial_park_id', $search['industrial_park_id']);
		endif;

		if(!empty($search['prefecture_id'])) :
			$this->db->where('i.prefecture_id', $search['prefecture_id']);
		endif;
		/// endsearch

		$this->db->order_by("country_name", "asc"); 
		$this->db->order_by("prefecture_name", "asc"); 
		$this->db->order_by("name", "asc"); 
		$this->db->limit($limit, $start); 
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function countIndustialProfilePark($search) {
		
		
		$this->db->select('industrial_park_profile.*, l.name as language_name , i.name as industrial_park_name ,c.name as country_name, p.name as prefecture_name');
		$this->db->from('industrial_park_profile');
		$this->db->join('industrial_park i', 'industrial_park_profile.industrial_park_id = i.id', 'left');
		$this->db->join('prefecture p', 'i.prefecture_id = p.id', 'left');
		$this->db->join('country c', 'p.country_id = c.id', 'left');
		$this->db->join('language l', 'industrial_park_profile.language_id = l.id', 'left');

		/// search
		if(!empty($search['name'])) :
			$this->db->where("industrial_park_profile.name LIKE '%".$search['name']."%'");
		endif;

		if(!empty($search['country_id'])) :
			$this->db->where('p.country_id', $search['country_id']);
		endif;

		if(!empty($search['language_id'])) :
			$this->db->where('industrial_park_profile.language_id', $search['language_id']);
		endif;

		if(!empty($search['industrial_park_id'])) :
			$this->db->where('industrial_park_profile.industrial_park_id', $search['industrial_park_id']);
		endif;

		if(!empty($search['prefecture_id'])) :
			$this->db->where('i.prefecture_id', $search['prefecture_id']);
		endif;
		/// endsearch

		$query = $this->db->get();

		return $query->num_rows();
	}

	public function checkcDuplicateIndustrial($name, $language_id, $industrial_park_id) {
		$condition = "LOWER(name) =" . "'" . $name . "' AND language_id =". "'" . $language_id . "' AND industrial_park_id =". "'" . $industrial_park_id . "'";
		$this->db->select('*');
		$this->db->from('industrial_park_profile');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  true;
		} else {
			return false;
		}
	}

	public function getIndustrialParkProfileById($id){
		$condition = "id =" . "'" . $id . "'";
		$this->db->select('*');
		$this->db->from('industrial_park_profile');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function saveIndustrialPark($data) {

		$action = (isset($data['id']) && $data['id'] > 0) ? "edit" : "add";

		if ($action == "add") :
			$result = $this->db->insert('industrial_park_profile', $data); 
		else:
			$this->db->where('id', $data['id']);
			$result = $this->db->update('industrial_park_profile', $data); 
		endif;

		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function deleteIndustrialPark($id) {
		$sql = "DELETE FROM industrial_park_profile WHERE id =".$id;
		$result = $this->db->query($sql);
		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function getIndustrialParkList() {

		$condition = "";
		$this->db->select('*');
		$this->db->from('industrial_park');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$country = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$country[$r->id] = $r->name;
			}
			return $country;
		} else {
			return false;
		}
	}

	public function getLanguageList() {

		$condition = "";
		$this->db->select('*');
		$this->db->from('language');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$country = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$country[$r->id] = $r->name;
			}
			return $country;
		} else {
			return false;
		}
	}

	public function getCountryList() {

		$condition = "";
		$this->db->select('*');
		$this->db->from('country');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$country = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$country[$r->id] = $r->name;
			}
			return $country;
		} else {
			return false;
		}
	}

	public function getPrefectureList() {

		$condition = "";
		$this->db->select('*');
		$this->db->from('prefecture');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$country = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$country[$r->id] = $r->name;
			}
			return $country;
		} else {
			return false;
		}
	}

	public function getPrefectureProfileList() {

		$condition = "";
		$this->db->select('*');
		$this->db->from('prefecture_profile');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$country = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$country[$r->id] = $r->name;
			}
			return $country;
		} else {
			return false;
		}
	}

	public function getPrefectureListByCountryId($country_id) {

		$condition = "country_id =" . "'" . $country_id . "'";
		$this->db->select('*');
		$this->db->from('prefecture');
		$this->db->order_by("name", "asc");
		$this->db->where($condition);
		$query = $this->db->get();
		$prefecture = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$prefecture[$r->id] = $r->name;
			}
			return $prefecture;
		} else {
			return false;
		}
	}

	public function getIndustrialParkByPrefectureID($prefecture_id) {

		$condition = "prefecture_id =" . "'" . $prefecture_id . "'";
		$this->db->select('*');
		$this->db->from('industrial_park');
		$this->db->order_by("name", "asc");
		$this->db->where($condition);
		$query = $this->db->get();
		$prefecture = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$prefecture[$r->id] = $r->name;
			}
			return $prefecture;
		} else {
			return false;
		}
	}

	
}

?>
