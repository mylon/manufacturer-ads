<?php

Class Admin_company_news_model extends CI_Model {
	
	public function getNewsList($limit, $start, $search) {
		$this->db->select('company_news.*, c.name as company_name, l.name as language_name, s1.username as created_by, s2.username as edited_by, nc.name as category_name');
		$this->db->from('company_news');
		$this->db->join('staff s1', 's1.id = company_news.created_by', 'left');
		$this->db->join('staff s2', 's2.id = company_news.edited_by', 'left');
		$this->db->join('company c', 'c.id = company_news.company_id', 'left');
		$this->db->join('language l', 'l.id = company_news.language_id', 'left');
		$this->db->join('news_category nc', 'nc.id = company_news.category_id', 'left');
		if(($this->session->userdata['manufacturer_staff_logged_in']['staff_type'] != 'superadmin') && 
			$this->session->userdata['manufacturer_staff_logged_in']['staff_company'] > 0) {

			$this->db->where('company_news.company_id', $this->session->userdata['manufacturer_staff_logged_in']['staff_company']);
		}

		/// search
		if(!empty($search['title'])) :
			$this->db->where("company_news.title LIKE '%".$search['title']."%'");
		endif;

		if(!empty($search['start_date'])) :
			$this->db->where("company_news.created_date >= '".$search['start_date']. ' 00:00:00' ."'");
		endif;

		if(!empty($search['end_date'])) :
			$this->db->where("company_news.created_date < '".$search['end_date']. ' 23:59:59' ."'");
		endif;

		if(!empty($search['created_by'])) :
			$this->db->where('company_news.created_by', $search['created_by']);
		endif;

		if(!empty($search['company_id'])) :
			$this->db->where('company_news.company_id', $search['company_id']);
		endif;

		if(!empty($search['category_id'])) :
			$this->db->where('company_news.category_id', $search['category_id']);
		endif;

		if(!empty($search['language_id'])) :
			$this->db->where('company_news.language_id', $search['language_id']);
		endif;
		/// endsearch

		$this->db->order_by("updated_date", "desc"); 
		$this->db->limit($limit, $start); 


		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function countNews($search) {

		$this->db->select('*');
		$this->db->from('company_news');

		if(($this->session->userdata['manufacturer_staff_logged_in']['staff_type'] != 'superadmin') && 
			$this->session->userdata['manufacturer_staff_logged_in']['staff_company'] > 0) {

			$this->db->where('company_news.company_id', $this->session->userdata['manufacturer_staff_logged_in']['staff_company']);
		}

		/// search
		if(!empty($search['title'])) :
			$this->db->where("title LIKE '%".$search['title']."%'");
		endif;

		if(!empty($search['start_date'])) :
			$this->db->where("created_date >= '".$search['start_date']. ' 00:00:00' ."'");
		endif;

		if(!empty($search['end_date'])) :
			$this->db->where("created_date < '".$search['end_date']. ' 23:59:59' ."'");
		endif;

		if(!empty($search['created_by'])) :
			$this->db->where('created_by', $search['created_by']);
		endif;

		if(!empty($search['company_id'])) :
			$this->db->where('company_id', $search['company_id']);
		endif;

		if(!empty($search['category_id'])) :
			$this->db->where('category_id', $search['category_id']);
		endif;

		if(!empty($search['language_id'])) :
			$this->db->where('language_id', $search['language_id']);
		endif;
		/// endsearch

		$query = $this->db->get();

		return $query->num_rows();
	}

	

	public function getNewsById($id){
		$condition = "id =" . "'" . $id . "'";
		$this->db->select('*');
		$this->db->from('company_news');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function getCompanyList() {

		$this->db->select('id, name');
		$this->db->from('company');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$category = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category[$r->id] = $r->name;
			}
			return $category;
		} else {
			return false;
		}
	}

	public function getNewsCategoryList() {

		$this->db->select('id, name');
		$this->db->from('news_category');
		if(($this->session->userdata['manufacturer_staff_logged_in']['staff_type'] != 'superadmin') && 
			$this->session->userdata['manufacturer_staff_logged_in']['staff_company'] > 0) {

			$this->db->where('news_category.company_id', $this->session->userdata['manufacturer_staff_logged_in']['staff_company']);
		}
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$category = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category[$r->id] = $r->name;
			}
			return $category;
		} else {
			return false;
		}
	}

	public function getLanguageList() {

		$this->db->select('id, name');
		$this->db->from('language');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$category = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category[$r->id] = $r->name;
			}
			return $category;
		} else {
			return false;
		}
	}

	public function getStaffList() {

		$this->db->select('id, username');
		$this->db->from('staff');
		$this->db->order_by("username", "asc");
		$query = $this->db->get();
		$category = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category[$r->id] = $r->username;
			}
			return $category;
		} else {
			return false;
		}
	}


	public function saveNews($data) {

		$action = (isset($data['id']) && $data['id'] > 0) ? "edit" : "add";

		if ($action == "add") :
			$data['updated_date'] = date('Y-m-d H:i:s');
			$result = $this->db->insert('company_news', $data); 
		else:
			$this->db->where('id', $data['id']);
			$data['updated_date'] = date('Y-m-d H:i:s');
			$result = $this->db->update('company_news', $data); 
		endif;

		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function deleteNews($id) {
		$sql = "DELETE FROM company_news WHERE id =".$id;
		$result = $this->db->query($sql);
		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function getNewsImage($id){
        $condition = "id =" . "'" . $id . "'";
		$this->db->select('images');
		$this->db->from('company_news');
		$this->db->where($condition);
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return $query->result();
		} else {
			return false;
		}
    }

    public function getAuthorImage($id){
        $condition = "id =" . "'" . $id . "'";
		$this->db->select('author_photo');
		$this->db->from('company_news');
		$this->db->where($condition);
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return $query->result();
		} else {
			return false;
		}
    }

    public function deleteNewsImage($id) {

		$this->db->where('id', $id);
		$data['images'] = NULL;
		$data['edited_by'] = $this->session->userdata['manufacturer_staff_logged_in']['staff_id'];
		$data['updated_date'] = date('Y-m-d H:i:s');
		$result = $this->db->update('company_news', $data); 

		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	 public function deleteAuthorImage($id) {

		$this->db->where('id', $id);
		$data['author_photo'] = NULL;
		$data['edited_by'] = $this->session->userdata['manufacturer_staff_logged_in']['staff_id'];
		$data['updated_date'] = date('Y-m-d H:i:s');
		$result = $this->db->update('company_news', $data); 

		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function getNewsDescriptionImages($news_id){
        $condition = "news_id =" . "'" . $news_id . "'";
		$this->db->select('*');
		$this->db->from('news_description_images');
		$this->db->where($condition);
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return $query->result();
		} else {
			return false;
		}
    }

    public function deleteNewsDescriptionImage($id) {

		$sql = "DELETE FROM news_description_images WHERE id =".$id;
		$result = $this->db->query($sql);
		if ($result) {
			return true;
		} else {
			return false;
		}
	}
	
}

?>
