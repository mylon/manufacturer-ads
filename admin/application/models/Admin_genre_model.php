<?php

Class Admin_genre_model extends CI_Model {


	public function getGenreList($limit, $start, $search) {

		$this->db->select('*');
		$this->db->from('genre');

		/// search
		if(!empty($search['name'])) :
			$this->db->where("name LIKE '%".$search['name']."%'");
		endif;
		/// endsearch

		$this->db->order_by("name", "asc"); 
		$this->db->limit($limit, $start); 
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function countGenre($search) {

		$this->db->select('*');
		$this->db->from('genre');

		/// search
		if(!empty($search['name'])) :
			$this->db->where("name LIKE '%".$search['name']."%'");
		endif;
		/// endsearch

		$query = $this->db->get();

		return $query->num_rows();
	}

	public function checkcGenrebyName($name) {
		$condition = "LOWER(name) =" . "'" . $name . "'";
		$this->db->select('*');
		$this->db->from('genre');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  true;
		} else {
			return false;
		}
	}

	public function getGenreById($id){
		$condition = "id =" . "'" . $id . "'";
		$this->db->select('*');
		$this->db->from('genre');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function saveGenre($data) {

		$action = (isset($data['id']) && $data['id'] > 0) ? "edit" : "add";

		if ($action == "add") :
			$result = $this->db->insert('genre', $data); 
		else:
			$this->db->where('id', $data['id']);
			$result = $this->db->update('genre', $data); 
		endif;

		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function deleteGenre($id) {
		$sql = "DELETE FROM genre WHERE id =".$id;
		$result = $this->db->query($sql);
		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	
}

?>
