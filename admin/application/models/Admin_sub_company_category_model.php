<?php

Class Admin_sub_company_category_model extends CI_Model {


	public function getSubCompanyCategoryList($limit, $start, $search) {

		$this->db->select('scc.*, cc.name as main_category_name');
		$this->db->from('sub_company_category scc');
		$this->db->join('company_category cc', 'cc.id = scc.company_category_id', 'left');

		/// search
		if(!empty($search['name'])) :
			$this->db->where("scc.name LIKE '%".$search['name']."%'");
		endif;

		if(!empty($search['company_category_id'])) :
			$this->db->where("scc.company_category_id LIKE '%".$search['company_category_id']."%'");
		endif;
		/// endsearch

		$this->db->order_by("scc.name", "asc"); 
		$this->db->limit($limit, $start); 
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function countSubCompanyCategory($search) {

		$this->db->select('scc.*');
		$this->db->from('sub_company_category scc');
		$this->db->join('company_category cc', 'cc.id = scc.company_category_id', 'left');

		/// search
		if(!empty($search['name'])) :
			$this->db->where("scc.name LIKE '%".$search['name']."%'");
		endif;

		if(!empty($search['company_category_id'])) :
			$this->db->where("scc.company_category_id LIKE '%".$search['company_category_id']."%'");
		endif;
		/// endsearch

		$query = $this->db->get();

		return $query->num_rows();
	}

	public function checkCompanyCategorybyName($name, $company_category_id) {
		$condition = "LOWER(name) =" . "'" . $name . "' AND company_category_id = " . "'" . $company_category_id . "'";
		$this->db->select('*');
		$this->db->from('sub_company_category');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  true;
		} else {
			return false;
		}
	}

	public function getSubCompanyCategoryById($id){
		$condition = "id =" . "'" . $id . "'";
		$this->db->select('*');
		$this->db->from('sub_company_category');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function getCompanyList() {

		$this->db->select('*');
		$this->db->from('company');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$country = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$country[$r->id] = $r->name;
			}
			return $country;
		} else {
			return false;
		}
	}

	public function getMainCompanyCategoryList() {

		$this->db->select('*');
		$this->db->from('company_category');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$country = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$country[$r->id] = $r->name;
			}
			return $country;
		} else {
			return false;
		}
	}

	public function category_data($company_id) {
		$condition = "company_id =" . "'" . $company_id . "'";
		$this->db->select('*');
		$this->db->from('company_category');
		$this->db->order_by("name", "asc");
		$this->db->where($condition);
		$query = $this->db->get();
		$category_data = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category_data[$r->id] = $r->name;
			}
			return $category_data;
		} else {
			return false;
		}
		
	}

	public function saveCompanyCategory($data) {

		$action = (isset($data['id']) && $data['id'] > 0) ? "edit" : "add";

		if ($action == "add") :
			$result = $this->db->insert('sub_company_category', $data); 
		else:
			$this->db->where('id', $data['id']);
			$result = $this->db->update('sub_company_category', $data); 
		endif;

		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function deleteCompanyCategory($id) {
		$sql = "DELETE FROM sub_company_category WHERE id =".$id;
		$result = $this->db->query($sql);
		if ($result) {
			$sql = "DELETE FROM sub_company_category_profile WHERE sub_company_category_id =".$id;
			$result = $this->db->query($sql);
			if ($result) {
				return true;
			} else {
				return false;
			}
		} else {
				return false;
		}
	}

	
}

?>
