<?php

Class Admin_news_category_profile_model extends CI_Model {


	public function getNewsCategoryProfileList($limit, $start, $search) {

		$this->db->select('ncp.*, c.name as company_name, nc.name as category_name, l.name as language_name');
		$this->db->from('news_category_profile ncp');
		$this->db->join('news_category nc', 'nc.id = ncp.news_category_id', 'left');
		$this->db->join('company c', 'c.id = nc.company_id', 'left');
		$this->db->join('language l', 'l.id = ncp.language_id', 'left');
		if(($this->session->userdata['manufacturer_staff_logged_in']['staff_type'] != 'superadmin') && 
			$this->session->userdata['manufacturer_staff_logged_in']['staff_company'] > 0) {

			$this->db->where('c.id', $this->session->userdata['manufacturer_staff_logged_in']['staff_company']);
		}

		/// search
		if(!empty($search['name'])) :
			$this->db->where("ncp.name LIKE '%".$search['name']."%'");
		endif;

		if(!empty($search['company_id'])) :
			$this->db->where('nc.company_id', $search['company_id']);
		endif;

		if(!empty($search['news_category_id'])) :
			$this->db->where('ncp.news_category_id', $search['news_category_id']);
		endif;

		if(!empty($search['language_id'])) :
			$this->db->where('ncp.language_id', $search['language_id']);
		endif;
		/// endsearch
		
		$this->db->order_by("name", "asc"); 
		$this->db->limit($limit, $start); 
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function countNewsCategoryProfile($search) {
		

		$this->db->select('ncp.*');
		$this->db->from('news_category_profile ncp');
		$this->db->join('news_category nc', 'nc.id = ncp.news_category_id', 'left');
		$this->db->join('company c', 'c.id = nc.company_id', 'left');
		$this->db->join('language l', 'l.id = ncp.language_id', 'left');
		if(($this->session->userdata['manufacturer_staff_logged_in']['staff_type'] != 'superadmin') && 
			$this->session->userdata['manufacturer_staff_logged_in']['staff_company'] > 0) {

			$this->db->where('c.id', $this->session->userdata['manufacturer_staff_logged_in']['staff_company']);
		}

		/// search
		if(!empty($search['name'])) :
			$this->db->where("ncp.name LIKE '%".$search['name']."%'");
		endif;

		if(!empty($search['company_id'])) :
			$this->db->where('nc.company_id', $search['company_id']);
		endif;

		if(!empty($search['news_category_id'])) :
			$this->db->where('ncp.news_category_id', $search['news_category_id']);
		endif;

		if(!empty($search['language_id'])) :
			$this->db->where('ncp.language_id', $search['language_id']);
		endif;
		/// endsearch

		$query = $this->db->get();

		return $query->num_rows();
	}

	public function checkcCategorybyName($name, $news_category_id, $language_id) {
		$condition = "LOWER(name) =" . "'" . $name . "' AND news_category_id = ". "'". $news_category_id ."' AND language_id = ". "'". $language_id ."'";
		$this->db->select('*');
		$this->db->from('news_category_profile');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  true;
		} else {
			return false;
		}
	}

	public function getCategoryProfileById($id){
		$condition = "id =" . "'" . $id . "'";
		$this->db->select('*');
		$this->db->from('news_category_profile');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function category_data($company_id) {
		$condition = "company_id =" . "'" . $company_id . "'";
		$this->db->select('*');
		$this->db->from('news_category');
		$this->db->order_by("name", "asc");
		$this->db->where($condition);
		$query = $this->db->get();
		$category_data = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category_data[$r->id] = $r->name;
			}
			return $category_data;
		} else {
			return false;
		}
		
	}

	public function getCompanyList() {

		$this->db->select('id, name');
		$this->db->from('company');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$category = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category[$r->id] = $r->name;
			}
			return $category;
		} else {
			return false;
		}
	}

	public function getCategoryList() {

		$this->db->select('id, name');
		$this->db->from('news_category');
		if(($this->session->userdata['manufacturer_staff_logged_in']['staff_type'] != 'superadmin') && 
			$this->session->userdata['manufacturer_staff_logged_in']['staff_company'] > 0) {

			$this->db->where('news_category.company_id', $this->session->userdata['manufacturer_staff_logged_in']['staff_company']);
		}
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$category = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category[$r->id] = $r->name;
			}
			return $category;
		} else {
			return false;
		}
	}

	public function getLanguageList() {

		$this->db->select('id, name');
		$this->db->from('language');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$category = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category[$r->id] = $r->name;
			}
			return $category;
		} else {
			return false;
		}
	}

	public function saveCategory($data) {

		$action = (isset($data['id']) && $data['id'] > 0) ? "edit" : "add";

		if ($action == "add") :
			$result = $this->db->insert('news_category_profile', $data); 
		else:
			$this->db->where('id', $data['id']);
			$result = $this->db->update('news_category_profile', $data); 
		endif;

		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function deleteCategory($id) {
		$sql = "DELETE FROM news_category_profile WHERE id =".$id;
		$result = $this->db->query($sql);
		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	
}

?>
