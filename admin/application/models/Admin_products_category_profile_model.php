<?php

Class Admin_products_category_profile_model extends CI_Model {


	public function getNewsCategoryProfileList($limit, $start, $search) {

		$this->db->select('pp.*, c.name as company_name, p.name as category_name, l.name as language_name');
		$this->db->from('products_category_profile pp');
		$this->db->join('products_category p', 'p.id = pp.products_category_id', 'left');
		$this->db->join('company c', 'c.id = p.company_id', 'left');
		$this->db->join('language l', 'l.id = pp.language_id', 'left');
		if(($this->session->userdata['manufacturer_staff_logged_in']['staff_type'] != 'superadmin') && 
			$this->session->userdata['manufacturer_staff_logged_in']['staff_company'] > 0) {

			$this->db->where('p.company_id', $this->session->userdata['manufacturer_staff_logged_in']['staff_company']);
		}

		/// search
		if(!empty($search['name'])) :
			$this->db->where("pp.name LIKE '%".$search['name']."%'");
		endif;

		if(!empty($search['company_id'])) :
			$this->db->where('p.company_id', $search['company_id']);
		endif;

		if(!empty($search['products_category_id'])) :
			$this->db->where('pp.products_category_id', $search['products_category_id']);
		endif;

		if(!empty($search['language_id'])) :
			$this->db->where('pp.language_id', $search['language_id']);
		endif;
		/// endsearch
		
		$this->db->order_by("id", "desc"); 
		$this->db->limit($limit, $start); 
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function countNewsCategoryProfile($search) {
		

		$this->db->select('*');
		$this->db->from('products_category_profile');
		$this->db->join('products_category', 'products_category.id = products_category_profile.products_category_id', 'left');
		$this->db->join('company', 'company.id = products_category.company_id', 'left');

		if(($this->session->userdata['manufacturer_staff_logged_in']['staff_type'] != 'superadmin') && 
			$this->session->userdata['manufacturer_staff_logged_in']['staff_company'] > 0) {

			$this->db->where('products_category.company_id', $this->session->userdata['manufacturer_staff_logged_in']['staff_company']);
		}


		/// search
		if(!empty($search['name'])) :
			$this->db->where("name LIKE '%".$search['name']."%'");
		endif;

		if(!empty($search['products_category_id'])) :
			$this->db->where('products_category_id', $search['products_category_id']);
		endif;

		if(!empty($search['language_id'])) :
			$this->db->where('language_id', $search['language_id']);
		endif;
		/// endsearch

		$query = $this->db->get();

		return $query->num_rows();
	}

	public function checkcCategorybyName($name, $language_id, $products_category_id) {
		$condition = "LOWER(name) =" . "'" . $name . "' AND language_id = " . "'" . $language_id . "' AND products_category_id = " . "'" . $products_category_id . "'";
		$this->db->select('*');
		$this->db->from('products_category_profile');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  true;
		} else {
			return false;
		}
	}

	public function getCategoryProfileById($id){
		$condition = "id =" . "'" . $id . "'";
		$this->db->select('*');
		$this->db->from('products_category_profile');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function getCompanyList() {

		$this->db->select('id, name');
		$this->db->from('company');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$category = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category[$r->id] = $r->name;
			}
			return $category;
		} else {
			return false;
		}
	}

	public function getCategoryList() {

		$this->db->select('id, name');
		$this->db->from('products_category');
		if(($this->session->userdata['manufacturer_staff_logged_in']['staff_type'] != 'superadmin') && 
			$this->session->userdata['manufacturer_staff_logged_in']['staff_company'] > 0) {
			$this->db->where('products_category.company_id', $this->session->userdata['manufacturer_staff_logged_in']['staff_company']);
		}
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$category = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category[$r->id] = $r->name;
			}
			return $category;
		} else {
			return false;
		}
	}

	public function getLanguageList() {

		$this->db->select('id, name');
		$this->db->from('language');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$category = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category[$r->id] = $r->name;
			}
			return $category;
		} else {
			return false;
		}
	}

	public function saveCategory($data) {

		$action = (isset($data['id']) && $data['id'] > 0) ? "edit" : "add";

		if ($action == "add") :
			$result = $this->db->insert('products_category_profile', $data); 
		else:
			$this->db->where('id', $data['id']);
			$result = $this->db->update('products_category_profile', $data); 
		endif;

		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function deleteCategory($id) {
		$sql = "DELETE FROM products_category_profile WHERE id =".$id;
		$result = $this->db->query($sql);
		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	
}

?>
