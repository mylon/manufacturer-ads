<?php

Class Admin_prefecture_profile_model extends CI_Model {


	public function getPrefectureProfileList($limit, $start, $search) {

		$this->db->select('prefecture_profile.*, c.name as country_name, p.name as prefecture_name, l.name as language_name');
		$this->db->from('prefecture_profile');
		$this->db->join('prefecture p', 'prefecture_profile.prefecture_id = p.id', 'left');
		$this->db->join('language l', 'prefecture_profile.language_id = l.id', 'left');
		$this->db->join('country c', 'p.country_id = c.id', 'left');

		/// search
		if(!empty($search['name'])) :
			$this->db->where("prefecture_profile.name LIKE '%".$search['name']."%'");
		endif;

		if(!empty($search['country_id'])) :
			$this->db->where('p.country_id', $search['country_id']);
		endif;

		if(!empty($search['prefecture_id'])) :
			$this->db->where('prefecture_profile.prefecture_id', $search['prefecture_id']);
		endif;

		if(!empty($search['language_id'])) :
			$this->db->where('prefecture_profile.language_id', $search['language_id']);
		endif;
		/// endsearch

		$this->db->order_by("name", "asc"); 
		$this->db->limit($limit, $start); 
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function countPrefectureProfile($search) {
		
		$this->db->select('prefecture_profile.*, c.name as country_name, p.name as prefecture_name, l.name as language_name');
		$this->db->from('prefecture_profile');
		$this->db->join('prefecture p', 'prefecture_profile.prefecture_id = p.id', 'left');
		$this->db->join('language l', 'prefecture_profile.language_id = l.id', 'left');
		$this->db->join('country c', 'p.country_id = c.id', 'left');

		/// search
		if(!empty($search['name'])) :
			$this->db->where("prefecture_profile.name LIKE '%".$search['name']."%'");
		endif;

		if(!empty($search['country_id'])) :
			$this->db->where('p.country_id', $search['country_id']);
		endif;

		if(!empty($search['prefecture_id'])) :
			$this->db->where('prefecture_profile.prefecture_id', $search['prefecture_id']);
		endif;

		if(!empty($search['language_id'])) :
			$this->db->where('prefecture_profile.language_id', $search['language_id']);
		endif;
		/// endsearch

		$query = $this->db->get();

		return $query->num_rows();
	}

	public function checkcPrefectureName($name, $prefecture_id, $language_id) {
		$condition = "LOWER(name) =" . "'" . $name . "' AND prefecture_id="."'".$prefecture_id."' AND language_id="."'".$language_id."'";
		$this->db->select('*');
		$this->db->from('prefecture_profile');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  true;
		} else {
			return false;
		}
	}

	public function getPrefectureProfileById($id){
		$condition = "id =" . "'" . $id . "'";
		$this->db->select('*');
		$this->db->from('prefecture_profile');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function getCountryList() {

		$condition = "";
		$this->db->select('*');
		$this->db->from('country');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$country = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$country[$r->id] = $r->name;
			}
			return $country;
		} else {
			return false;
		}
	}

	public function getPrefectureList() {

		$condition = "";
		$this->db->select('*');
		$this->db->from('prefecture');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$country = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$country[$r->id] = $r->name;
			}
			return $country;
		} else {
			return false;
		}
	}

	public function country_data($country_id) {
		$condition = "country_id =" . "'" . $country_id . "'";
		$this->db->select('*');
		$this->db->from('prefecture');
		$this->db->order_by("name", "asc");
		$this->db->where($condition);
		$query = $this->db->get();
		$category_data = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$category_data[$r->id] = $r->name;
			}
			return $category_data;
		} else {
			return false;
		}
		
	}

	public function getLanguageList() {

		$condition = "";
		$this->db->select('*');
		$this->db->from('language');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		$country = array();

		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $r) {
				$country[$r->id] = $r->name;
			}
			return $country;
		} else {
			return false;
		}
	}

	public function savePrefecture($data) {

		$action = (isset($data['id']) && $data['id'] > 0) ? "edit" : "add";

		if ($action == "add") :
			$result = $this->db->insert('prefecture_profile', $data); 
		else:
			$this->db->where('id', $data['id']);
			$result = $this->db->update('prefecture_profile', $data); 
		endif;

		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function deletePrefecture($id) {
		$sql = "DELETE FROM prefecture_profile WHERE id =".$id;
		$result = $this->db->query($sql);
		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	
}

?>
