<?php

$lang['custom_company_name'] = "บริษัท";
$lang['custom_representative'] = "ผู้บริหาร";
$lang['custom_address'] = "ที่อยู่";
$lang['custom_product'] = "สินค้า";
$lang['custom_est_date'] = "วันที่ก่อตั้ง";
$lang['custom_capital'] = "ทุนจดทะเบียน";
$lang['custom_parent_comp'] = "บริษัทหลัก";
$lang['custom_group_comp'] = "บริษัทสาขา";
$lang['custom_shareholder'] = "ผู้ถือหุ้น";
$lang['custom_employee'] = "พนักงาน";
$lang['custom_account_period'] = "รอบบัญชี";
$lang['custom_bank'] = "ธนาคาร";
$lang['custom_certificate'] = "ใบรับรอง";
$lang['custom_major_customer'] = "ลูกค้า";
$lang['custom_url'] = "URL";
$lang['custom_pay_roll'] = "พนักงาน";
$lang['custom_menu_home'] = "หน้าแรก";
$lang['custom_menu_products'] = "สินค้า";
$lang['custom_menu_news'] = "ข่าว";


/* End of file custom_lang.php */
/* Location: ./system/language/english/custom_lang.php */