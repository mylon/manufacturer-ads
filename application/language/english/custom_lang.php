<?php

$lang['custom_company_name'] = "Company Name";
$lang['custom_representative'] = "Representative Person";
$lang['custom_address'] = "Address";
$lang['custom_product'] = "Product";
$lang['custom_est_date'] = "Establishment Date";
$lang['custom_capital'] = "Capital";
$lang['custom_parent_comp'] = "Parent Company";
$lang['custom_group_comp'] = "Group company";
$lang['custom_shareholder'] = "Shareholder";
$lang['custom_employee'] = "Employee";
$lang['custom_account_period'] = "Account Period";
$lang['custom_bank'] = "Bank";
$lang['custom_certificate'] = "Certificate";
$lang['custom_major_customer'] = "Major Customer";
$lang['custom_url'] = "URL";
$lang['custom_pay_roll'] = "Employee";
$lang['custom_menu_home'] = "Home";
$lang['custom_menu_products'] = "Products";
$lang['custom_menu_news'] = "News";
$lang['custom_product_line'] = "สินค้า";
/* End of file custom_lang.php */
/* Location: ./system/language/english/custom_lang.php */