<?php

$lang['custom_company_name'] = "会社名";
$lang['custom_representative'] = "代表者";
$lang['custom_address'] = "住所";
$lang['custom_product'] = "取扱商品";
$lang['custom_est_date'] = "設立日";
$lang['custom_capital'] = "資本金";
$lang['custom_parent_comp'] = "親会社";
$lang['custom_group_comp'] = "グループ会社";
$lang['custom_shareholder'] = "株主";
$lang['custom_employee'] = "従業員数";
$lang['custom_account_period'] = "決算期";
$lang['custom_bank'] = "取引銀行";
$lang['custom_certificate'] = "証明書";
$lang['custom_major_customer'] = "主要取引先";
$lang['custom_url'] = "URL";
$lang['custom_pay_roll'] = "従業員数";
$lang['custom_menu_home'] = "ホーム";
$lang['custom_menu_products'] = "製品";
$lang['custom_menu_news'] = "ニュース";


/* End of file custom_lang.php */
/* Location: ./system/language/english/custom_lang.php */