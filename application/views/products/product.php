<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$company = $companyData[0];
$company_profile = $companyProfile[0];
$company_category = $companyCategory[0];
$company_products = $companyProduct[0];
$company_url = base_url().$languageCode.'/'.$company->web_url;
$logo_url = 'https://s3-ap-southeast-1.amazonaws.com/manufacturer-bucket/logo/'.$company->logo;
$image_url = 'https://s3-ap-southeast-1.amazonaws.com/manufacturer-bucket/company-products/';
$news_url = 'https://s3-ap-southeast-1.amazonaws.com/manufacturer-bucket/company-news/';

$colour_menu = ($company->colour_menu != "") ? $company->colour_menu : "";
$colour_link = ($company->colour_link != "") ? $company->colour_link : "";

?>

<div class="container" id="company-content">
	<div class="company-content-wrapper">
		<div id="company-news">
			<div id="company-breadcrumb">
				<ul>
					<li><a href="<?php echo $company_url;?>"><?= lang('custom_menu_home'); ?></a></li>
					<li>&gt;<li/>
					<li><a href="<?php echo $company_url;?>/products" style="padding-left:5px;"><?= lang('custom_menu_products'); ?></a></li>
					<li>&gt;<li/>
					<li class="active" style="padding-left:5px; color: <?php echo $colour_link;?>"><?php echo $company_products->name;?></li>
				</ul>
			</div>
			<div class="row" id="company-profile-content">
				<div id="company-profile-content" class="col-md-8 col-xs-12">
					<div class="row" id="company-product-details">
						<div id="company-product-image" class="col-md-4 col-xs-4">
							<?php $productImg = ($company_products->images != "") ? $image_url.$company_products->images : $logo_url; ?>
							<img src="<?php echo $productImg;?>" />
						</div>
						<div id="company-product-left-detail" class="col-md-8 col-xs-8">
							<div class="products-category-name-product">
								<?php echo ucfirst($company_products->lang_cat_name); ?>
							</div>
							<div class="products-name-product" style="color: <?php echo $colour_link;?>">
								<?php echo ucfirst($company_products->name); ?>
							</div>
							<h1 class="products-name-product">
								<?php echo htmlspecialchars_decode(ucfirst($company_products->title)); ?><br/>
							</h1>
						</div>
					</div>
					<div class="products-description-product">
						<?php echo htmlspecialchars_decode($company_products->description); ?>
					</div>
					<div class="products-specs-product">
						<?php echo htmlspecialchars_decode($company_products->specs); ?>
					</div>
					
				</div><!-- company-profile-content -->
				<div id="company-right-content" class="col-md-4 col-xs-12">
					