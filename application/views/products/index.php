<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$company = $companyData[0];
$company_profile = $companyProfile[0];
$company_category = $companyCategory[0];
$company_url = base_url().$languageCode.'/'.$company->web_url;
$logo_url = 'https://s3-ap-southeast-1.amazonaws.com/manufacturer-bucket/logo/'.$company->logo;
$image_url = 'https://s3-ap-southeast-1.amazonaws.com/manufacturer-bucket/company-products/';
$news_url = 'https://s3-ap-southeast-1.amazonaws.com/manufacturer-bucket/company-news/';

$colour_menu = ($company->colour_menu != "") ? $company->colour_menu : "";
$colour_link = ($company->colour_link != "") ? $company->colour_link : "";

?>

<div class="container" id="company-content">
	<div class="company-content-wrapper">
		<div id="company-news">
			<div id="company-breadcrumb">
				<ul>
					<li><a href="<?php echo $company_url;?>"><?= lang('custom_menu_home'); ?></a></li>
					<li>&gt;<li/>
					<li class="active" style="padding-left:5px; color: <?php echo $colour_link;?>"><?= lang('custom_menu_products'); ?></li>
				</ul>
			</div>
			<!--<div id="company-header">
				<h1><?php echo lang('custom_menu_products').' : '.$company_profile->name; ?></h1>
			</div>-->
			<div class="row" id="company-profile-content">
				<div id="company-profile-content" class="col-md-8 col-xs-12">
					<div id="subject-title" style="background: <?php echo $colour_menu;?>">Products</div>
					<div id="company-products-lists">
					<?php if(!empty($productCategory)):?>
							<?php foreach($productCategory as $pc):?>
								<div class="products-category-list">
									<div class="products-category-name-header">
										<?php echo ucfirst($pc->name); ?>
									</div>
									<div class="products-category-description">
										<?php echo ucfirst($pc->description); ?>
									</div>
									<?php if(!empty($companyProducts)):?>
										<div class="products-list">
											<?php for($i=0; $i<count($companyProducts); $i++):?>
												<?php if($companyProducts[$i]->category_id == $pc->category_id):?>
													<div class="row product-details">
														<div class="col-md-2 col-xs-2 products-image">
															<?php $productImg = ($companyProducts[$i]->images != "") ? $image_url.$companyProducts[$i]->images : $logo_url; ?>
															<img src="<?php echo $productImg; ?>" />
														</div>
														<div class="col-md-10 col-xs-10 products-description">
															<a class="product-link" 
															   href="<?php echo $company_url.'/products/'.$companyProducts[$i]->id; ?>"
															   style="color: <?php echo $colour_link;?>">
																<?php echo strtoupper($companyProducts[$i]->name); ?>
															</a>
															<div class="products-category-name">
																<?php echo ucfirst($pc->name); ?>
															</div>
															<span><?php echo $companyProducts[$i]->title; ?></span>
														</div>
													</div>
												<?php endif;?>
											<?php endfor;?>
										</div>
									<?php endif;?>
								</div>
							<?php endforeach;?>
						<?php endif;?>
					</div>
				</div><!-- company-profile -->
				<div id="company-right-content" class="col-md-4 col-xs-12">
					