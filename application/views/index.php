<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<div class="content-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-3 col-xs-12 left-panel">
				<div class="panel panel-default panel-by-category panel-violet">
					<div class="panel-heading">Search by Category</div>
					<ul class="list-group">
						<?php if(!empty($comapanyCategoryList)): ?>
							<?php $i=0; foreach($comapanyCategoryList as $ccl): ?>
								<?php if($i <= 4 ): ?>
							    	<li class="list-group-item">
							    		<a href=""><?php echo ucfirst($ccl->name); ?></a>
							    	</li>
							    <?php endif;?>
					    	<?php $i++; endforeach; ?>
					    	<div class="collapse" id="category-collapse">
					    		<?php $i=0; foreach($comapanyCategoryList as $ccl): ?>
					    			<?php if($i > 4 ): ?>
							    		<li class="list-group-item">
								    		<a href=""><?php echo ucfirst($ccl->name); ?></a>
								    	</li>
								    <?php endif;?>
						    	<?php $i++; endforeach; ?>
					    	</div>
						<?php endif;?>
						<?php if(count($comapanyCategoryList) >= 5): ?>
							<li class="list-group-item more-button">
								<a class="collapsed" 
								   role="button" 
								   data-toggle="collapse" 
								   href="#category-collapse" 
								   aria-expanded="false" 
								   aria-controls="category-collapse">
						          More
						        </a>
							</li>
						<?php endif;?>
					</ul>
				</div>
				<div class="panel panel-default panel-by-ie panel-grey">
					<div class="panel-heading">Search by I.E.</div>
					<ul class="list-group">
					    <li class="list-group-item">Cras justo odio</li>
					    <li class="list-group-item">Dapibus ac facilisis in</li>
					    <li class="list-group-item">Morbi leo risus</li>
					    <li class="list-group-item">Porta ac consectetur ac</li>
					    <li class="list-group-item">Vestibulum at eros</li>
					  </ul>
				</div>
				<div class="panel panel-default panel-by-province panel-grey">
					<div class="panel-heading">Search by Province</div>
					<ul class="list-group">
					    <li class="list-group-item">Cras justo odio</li>
					    <li class="list-group-item">Dapibus ac facilisis in</li>
					    <li class="list-group-item">Morbi leo risus</li>
					    <li class="list-group-item">Porta ac consectetur ac</li>
					    <li class="list-group-item">Vestibulum at eros</li>
					 </ul>
				</div>
				<div class="panel panel-default panel-by-country panel-grey">
					<div class="panel-heading">Search by Country</div>
					<ul class="list-group">
					    <li class="list-group-item">Cras justo odio</li>
					    <li class="list-group-item">Dapibus ac facilisis in</li>
					    <li class="list-group-item">Morbi leo risus</li>
					    <li class="list-group-item">Porta ac consectetur ac</li>
					    <li class="list-group-item">Vestibulum at eros</li>
					 </ul>
				</div>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="panel panel-default panel-by-news panel-violet">
					<div class="panel-heading">News</div>
					<ul class="list-group">
					    <li class="list-group-item news-list">
					    	<div class="row">
					    		<div class="col-md-2 col-sm-4 col-xs-12 news-image">
					    			<img class="img-responsive" src="<?php echo base_url('assets/img/news-test.png'); ?>" />
					    		</div>
					    		<div class="col-md-10 col-sm-8 col-xs-12 news-content-wrapp">
					    			<div class="news-title">
					    				Dear Trump: Please save my GM factory job in Lordstown, Ohio
					    			</div>
					    			<div class="news-date">
					    				09:43 AM, Jan 18, 2017
					    			</div>
					    			<div class="news-content">
					    				2,000 GM workers are losing their job on January 20, 2017, the day
					    			</div>
					    		</div>
					    	</div>
					    </li>
					    <li class="list-group-item news-list">
					    	<div class="row">
					    		<div class="col-md-2 col-sm-4 col-xs-12 news-image">
					    			<img class="img-responsive" src="<?php echo base_url('assets/img/news-test.png'); ?>" />
					    		</div>
					    		<div class="col-md-10 col-sm-8 col-xs-12 news-content-wrapp">
					    			<div class="news-title">
					    				Dear Trump: Please save my GM factory job in Lordstown, Ohio
					    			</div>
					    			<div class="news-date">
					    				09:43 AM, Jan 18, 2017
					    			</div>
					    			<div class="news-content">
					    				2,000 GM workers are losing their job on January 20, 2017, the day
					    			</div>
					    		</div>
					    	</div>
					    </li>
					    <li class="list-group-item news-list">
					    	<div class="row">
					    		<div class="col-md-2 col-sm-4 col-xs-12 news-image">
					    			<img class="img-responsive" src="<?php echo base_url('assets/img/news-test.png'); ?>" />
					    		</div>
					    		<div class="col-md-10 col-sm-8 col-xs-12 news-content-wrapp">
					    			<div class="news-title">
					    				Dear Trump: Please save my GM factory job in Lordstown, Ohio
					    			</div>
					    			<div class="news-date">
					    				09:43 AM, Jan 18, 2017
					    			</div>
					    			<div class="news-content">
					    				2,000 GM workers are losing their job on January 20, 2017, the day
					    			</div>
					    		</div>
					    	</div>
					    </li>
					    <li class="list-group-item news-list">
					    	<div class="row">
					    		<div class="col-md-2 col-sm-4 col-xs-12 news-image">
					    			<img class="img-responsive" src="<?php echo base_url('assets/img/news-test.png'); ?>" />
					    		</div>
					    		<div class="col-md-10 col-sm-8 col-xs-12 news-content-wrapp">
					    			<div class="news-title">
					    				Dear Trump: Please save my GM factory job in Lordstown, Ohio
					    			</div>
					    			<div class="news-date">
					    				09:43 AM, Jan 18, 2017
					    			</div>
					    			<div class="news-content">
					    				2,000 GM workers are losing their job on January 20, 2017, the day
					    			</div>
					    		</div>
					    	</div>
					    </li>
					</ul>
				</div>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12 right-panel">
				<div class="right-index-box">
					<div class="right-index-title">
						ＮＴＴの作業服納入で談合の疑い
					</div>
					<div class="right-index-img">
					   <img class="img-responsive text-center" src="<?php echo base_url('assets/img/right-test.png'); ?>" />
					</div>
				</div>
				<div class="right-index-box">
					<div class="right-index-title">
						ＮＴＴの作業服納入で談合の疑い
					</div>
					<div class="right-index-img">
					   <img class="img-responsive text-center" src="<?php echo base_url('assets/img/right-test.png'); ?>" />
					</div>
				</div>
				<div class="right-index-box">
					<div class="right-index-title">
						ＮＴＴの作業服納入で談合の疑い
					</div>
					<div class="right-index-img">
					   <img class="img-responsive text-center" src="<?php echo base_url('assets/img/right-test.png'); ?>" />
					</div>
				</div>
			</div>
		</div>
	</div>
</div>