<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$company = $companyData[0];
$company_profile = $companyProfile[0];
$company_category = $companyCategory[0];
$company_url = base_url().$languageCode.'/'.$company->web_url;
$logo_url = 'https://s3-ap-southeast-1.amazonaws.com/manufacturer-bucket/logo/'.$company->logo;
$news_url = 'https://s3-ap-southeast-1.amazonaws.com/manufacturer-bucket/company-news/';


$colour_menu = ($company->colour_menu != "") ? $company->colour_menu : "";
$colour_link = ($company->colour_link != "") ? $company->colour_link : "";

?>

<div class="container" id="company-content">
	<div class="company-content-wrapper">
		<div id="company-profile" class="home-page">
			<div id="company-header">
				<h1><?php echo $company_profile->subject; ?></h1>
			</div>
			<div id="company-description">
				<?php echo str_replace("/", "", $company_profile->description); ?>
			</div>
			<div class="row" id="company-profile-content">
				<div id="company-profile" class="col-md-8 col-xs-12">
					<div id="subject-title" style="background: <?php echo $colour_menu;?>">Company Profile</div>
					<div id="company-profile-row-list">
						<?php if(!empty($company_profile->name)): ?>
						<div class="company-profile-row row">
							<div class="col-md-3 col-xs-12 company-profile-subject"><?= lang('custom_company_name'); ?></div>
							<div class="col-md-9 col-xs-12 company-profile-detail"><?php echo $company_profile->name;?></div>
						</div>
						<?php endif;?>
						<?php if(!empty($company_profile->ceo)): ?>
						<div class="company-profile-row row">
							<div class="col-md-3 col-xs-12 company-profile-subject"><?= lang('custom_representative'); ?></div>
							<div class="col-md-9 col-xs-12 company-profile-detail"><?php echo $company_profile->ceo;?></div>
						</div>
						<?php endif;?>
						<?php if(!empty($company_profile->address)): ?>
						<div class="company-profile-row row">
							<div class="col-md-3 col-xs-12 company-profile-subject"><?= lang('custom_address'); ?></div>
							<div class="col-md-9 col-xs-12 company-profile-detail"><?php echo $company_profile->address;?></div>
						</div>
						<?php endif;?>
						<?php if(!empty($company_profile->web_url)): ?>
						<div class="company-profile-row row">
							<div class="col-md-3 col-xs-12 company-profile-subject"><?= lang('custom_url'); ?></div>
							<div class="col-md-9 col-xs-12 company-profile-detail">
								<a href="<?php echo $company_profile->web_url;?>" target='_blank' style="color: <?php echo $colour_link;?>">
									<?php echo $company_profile->web_url;?>
								</a>
							</div>
						</div>
						<?php endif;?>
						<?php if(!empty($company_profile->product_line)): ?>
						<div class="company-profile-row row">
							<div class="col-md-3 col-xs-12 company-profile-subject"><?= lang('custom_product'); ?></div>
							<div class="col-md-9 col-xs-12 company-profile-detail"><?php echo $company_profile->product_line;?></div>
						</div>
						<?php endif;?>
						<?php if(!empty($company_profile->founded_date)): ?>
						<div class="company-profile-row row">
							<div class="col-md-3 col-xs-12 company-profile-subject"><?= lang('custom_est_date'); ?></div>
							<div class="col-md-9 col-xs-12 company-profile-detail"><?php echo $company_profile->founded_date;?></div>
						</div>
						<?php endif;?>
						<?php if(!empty($company_profile->capital)): ?>
						<div class="company-profile-row row">
							<div class="col-md-3 col-xs-12 company-profile-subject"><?= lang('custom_capital'); ?></div>
							<div class="col-md-9 col-xs-12 company-profile-detail"><?php echo $company_profile->capital;?></div>
						</div>
						<?php endif;?>
						<?php if(!empty($company_profile->head_company)): ?>
						<div class="company-profile-row row">
							<div class="col-md-3 col-xs-12 company-profile-subject"><?= lang('custom_parent_comp'); ?></div>
							<div class="col-md-9 col-xs-12 company-profile-detail"><?php echo $company_profile->head_company;?></div>
						</div>
						<?php endif;?>
						<?php if(!empty($company_profile->child_company)): ?>
						<div class="company-profile-row row">
							<div class="col-md-3 col-xs-12 company-profile-subject"><?= lang('custom_group_comp'); ?></div>
							<div class="col-md-9 col-xs-12 company-profile-detail"><?php echo $company_profile->child_company;?></div>
						</div>
						<?php endif;?>
						<?php if(!empty($company_profile->shareholder)): ?>
						<div class="company-profile-row row">
							<div class="col-md-3 col-xs-12 company-profile-subject"><?= lang('custom_shareholder'); ?></div>
							<div class="col-md-9 col-xs-12 company-profile-detail"><?php echo $company_profile->shareholder;?></div>
						</div>
						<?php endif;?>
						<?php if(!empty($company_profile->payroll_no)): ?>
						<div class="company-profile-row row">
							<div class="col-md-3 col-xs-12 company-profile-subject"><?= lang('custom_pay_roll'); ?></div>
							<div class="col-md-9 col-xs-12 company-profile-detail"><?php echo $company_profile->payroll_no;?></div>
						</div>
						<?php endif;?>
						<?php if(!empty($company_profile->accounting_period)): ?>
						<div class="company-profile-row row">
							<div class="col-md-3 col-xs-12 company-profile-subject"><?= lang('custom_account_period'); ?></div>
							<div class="col-md-9 col-xs-12 company-profile-detail"><?php echo $company_profile->accounting_period;?></div>
						</div>
						<?php endif;?>
						<?php if(!empty($company_profile->line_bank)): ?>
						<div class="company-profile-row row">
							<div class="col-md-3 col-xs-12 company-profile-subject"><?= lang('custom_bank'); ?></div>
							<div class="col-md-9 col-xs-12 company-profile-detail"><?php echo $company_profile->line_bank;?></div>
						</div>
						<?php endif;?>
						<?php if(!empty($company_profile->certification)): ?>
						<div class="company-profile-row row">
							<div class="col-md-3 col-xs-12 company-profile-subject"><?= lang('custom_certificate'); ?></div>
							<div class="col-md-9 col-xs-12 company-profile-detail"><?php echo $company_profile->certification;?></div>
						</div>
						<?php endif;?>
						<?php if(!empty($company_profile->main_client)): ?>
						<div class="company-profile-row row">
							<div class="col-md-3 col-xs-12 company-profile-subject"><?= lang('custom_major_customer'); ?></div>
							<div class="col-md-9 col-xs-12 company-profile-detail"><?php echo $company_profile->main_client;?></div>
						</div>
						<?php endif;?>
					</div><!-- company-profile-row-list -->
				</div><!-- company-profile -->
				<div id="company-right-content" class="col-md-4 col-xs-12">

				
