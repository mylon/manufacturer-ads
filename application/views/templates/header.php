<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Samurai Factory</title>
    
    
    <link href="<?php echo base_url('assets/css/reset.css');?>" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Trirong" rel="stylesheet">

    <!-- Bootstrap -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/bootstrap-datetimepicker.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/jquery-ui.min.css');?>" rel="stylesheet">
    <?php $randCss = rand(10000, 99999); ?>
    <link href="<?php echo base_url('assets/css/manufacturer.css').'?'.$randCss;?>" rel="stylesheet">

    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.1.0.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-ui.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>

	
	<?php
		if(isset($this->js))
		{
			foreach ($this->js as $js)
			{
				echo '<script type="text/javascript" src="'.base_url.'views/'.$js.'"></script>';
			}
		}
	?>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php 
        if(!empty($map['js'])){
            echo $map['js'];
        }
    ?>
    

  </head>
  <body>
    <?php
      $quick_search = isset($quick_search) ? $quick_search : "";
      $country_id = isset($country_id) ? $country_id : "";
      $language_id = isset($language_id) ? $language_id : "";

    ?>
    <div class="container">
      <div class="logo-img-content">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 header-logo-image">
            <img class="img-responsive" src="<?php echo base_url('assets/img/1_header_logo.png'); ?>" />
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <img class="img-responsive pull-right" src="<?php echo base_url('assets/img/1_header_banner.png'); ?>" />
          </div>
        </div>
      </div>
      <div class="search-form">
        <form class="form-inline" method="post" action="">
          <div class="row">
            <div class="col-md-6 col-sm-4 col-xs-12">
                <div class="input-group quick-search-input">
                  <input type="text" class="form-control" name="quick_search" id="quick_search" placeholder="Quick Search…" value="<?php echo $quick_search; ?>">
                  <span class="input-group-btn">
                    <button class="btn btn-default" id="quick-search-button" type="button">Go!</button>
                  </span>
                </div>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-12">
              <div class="select-language">
                <label for="language-search">Language</label>
                <select id="language-search" name="language" class="form-control">
                  <?php if(!empty($languageList)): ?>
                    <?php foreach($languageList as $l):?>
                    <?php $selected = ($language_id == $l->id) ? "selected" : "";?>
                    <option value="<?php echo $l->id; ?>" <?= $selected?>><?php echo ucfirst($l->name) ?></option>
                  <?php endforeach; endif; ?>
                </select>
              </div>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-12">
              <div class="select-country">
                <label for="country-search">Country</label>
                <select id="country-search" name="country" class="form-control">
                   <?php if(!empty($countryList)): ?>
                    <?php foreach($countryList as $c):?>
                    <?php $selected = ($country_id == $c->id) ? "selected" : "";?>
                    <option value="<?php echo $c->id; ?>" <?= $selected?>><?php echo ucfirst($c->name) ?></option>
                  <?php endforeach; endif; ?>
                </select>
              </div>
            </div>
          </div>
        </for>
      </div>
    </div>
      

	
