<?php

$company = $companyData[0];
$company_profile = $companyProfile[0];
$company_category = $companyCategory[0];
$company_url = base_url().$languageCode.'/'.$company->web_url;
$logo_url = 'https://s3-ap-southeast-1.amazonaws.com/manufacturer-bucket/logo/'.$company->logo;
$news_url = 'https://s3-ap-southeast-1.amazonaws.com/manufacturer-bucket/company-news/';


$colour_menu = ($company->colour_menu != "") ? $company->colour_menu : "";
$colour_link = ($company->colour_link != "") ? $company->colour_link : "";

?>

  <div class="company-right-box">
    <div class="right-subject-title" style="background: <?php echo $colour_menu;?>">Contact Us</div>
    <div id="contact-right-content" class="company-right-box-content">
      <?php if(!empty($company_profile->tel)): ?>
      <p>Tel: <a style="color: <?php echo $colour_link;?>" href="tel:<?php echo str_replace("(0)", "0", $company_profile->tel); ?>"><?php echo $company_profile->tel;?></a></p>
      <?php endif;?>
      <?php /*if(!empty($company_profile->tel1)): ?>
      <p>Tel: <a style="color: <?php echo $colour_link;?>" href="tel:<?php echo str_replace("(0)", "0", $company_profile->tel1); ?>"><?php echo $company_profile->tel1;?></a></p>
      <?php endif;?>
      <?php if(!empty($company_profile->tel2)): ?>
      <p>Tel: <a style="color: <?php echo $colour_link;?>" href="tel:<?php echo str_replace("(0)", "0", $company_profile->tel2); ?>"><?php echo $company_profile->tel2;?></a></p>
      <?php endif;?>
      <?php if(!empty($company_profile->tel3)): ?>
      <p>Tel: <a style="color: <?php echo $colour_link;?>" href="tel:<?php echo str_replace("(0)", "0", $company_profile->tel3); ?>"><?php echo $company_profile->tel3;?></a></p>
      <?php endif;?>
      <?php if(!empty($company_profile->tel4)): ?>
      <p>Tel: <a style="color: <?php echo $colour_link;?>" href="tel:<?php echo str_replace("(0)", "0", $company_profile->tel4); ?>"><?php echo $company_profile->tel4;?></a></p>
      <?php endif;*/ ?>
      <p>Email: <a style="color: <?php echo $colour_link;?>" href="mailto:<?php echo $company_profile->email; ?>"><?php echo $company_profile->email;?></a></p>
    </div>
  </div>
  <div class="company-right-box">
    <div class="right-subject-title" style="background: <?php echo $colour_menu;?>">Categories</div>
    <div id="category-right-content" class="company-right-box-content">
      <?php if(!empty($productCategory)):?>
        <?php $i=0; foreach($productCategory as $pc): $i++?>
          <div id="collapse-category-<?echo $i?>" class="collapse-category">
             <a role="button" 
                data-toggle="collapse" 
                data-parent="#collapse-category-<?echo $i?>" 
                href="#collapse<?php echo $i;?>" 
                aria-expanded="true" 
                aria-controls="collapse<?php echo $i;?>"
                style="color: <?php echo $colour_link;?>"
                class="collapse-link">
                  <?php echo ucfirst($pc->name); ?>
                   <span class="caret"></span>
                </a>
            <?php if(!empty($companyProducts)):?>
            <ul id="collapse<?php echo $i;?>" class="collapse collapse-list">
              <?php for($x=0; $x<count($companyProducts); $x++):?>
                <?php if($companyProducts[$x]->category_id == $pc->category_id):?>
                  <li>
                    <a  href="<?php echo $company_url.'/products/'.$companyProducts[$x]->id; ?>">
                      <?php echo strtoupper($companyProducts[$x]->name); ?>
                    </a>
                  </li>
                <?php endif;?>
              <?php endfor;?>
            </ul>
            <?php endif;?>
          </div>
        <?php endforeach;?>
      <?php endif;?>
      <?php if(!empty($newsCategory)):?>
        <?php $i=0; foreach($newsCategory as $nc): $i++?>
          <div id="news-collapse-category-<?echo $i?>" class="collapse-category">
             <a role="button" 
                data-toggle="collapse" 
                data-parent="#news-collapse-category-<?echo $i?>" 
                href="#news-collapse<?php echo $i;?>" 
                aria-expanded="true" 
                aria-controls="collapse<?php echo $i;?>"
                style="color: <?php echo $colour_link;?>"
                class="collapse-link">
                  <?php echo ucfirst($nc->name); ?>
                   <span class="caret"></span>
                </a>
            <?php if(!empty($companyNews)):?>
            <ul id="news-collapse<?php echo $i;?>" class="collapse collapse-list">
              <?php for($x=0; $x<count($companyNews); $x++):?>
                <?php if($companyNews[$x]->category_id == $nc->category_id):?>
                  <li>
                    <a href="<?php echo $company_url.'/news/'.$companyNews[$x]->id; ?>">
                      <?php echo strtoupper($companyNews[$x]->title); ?>
                    </a>
                  </li>
                <?php endif;?>
              <?php endfor;?>
            </ul>
            <?php endif;?>
          </div>
        <?php endforeach;?>
      <?php endif;?>
    </div>
  </div>
  <div class="company-right-box">
    <div class="right-subject-title" style="background: <?php echo $colour_menu;?>">News</div>
    <div id="news-right-content" class="company-right-box-content">
      <?php if(!empty($companyNews)):?>
        <?php foreach($companyNews as $cn):?>
          <div class="news-right-row">
            <div class="news-image">
              <?php $newsImg = ($cn->images != "") ? $news_url.$cn->images : $logo_url; ?>
              <img src="<?php echo $newsImg; ?>" alt="<?php echo $cn->title; ?>" />
            </div>
            <div class="news-right-title">
              <a href="<?php echo $company_url.'/news/'.$cn->id; ?>" 
                 style="color: <?php echo $colour_link;?>">
                <?php echo htmlspecialchars_decode($cn->title); ?>
              </a>
            </div>
          </div>
        <?php endforeach;?>
      <?php endif;?>
      <div id="more-right-news">
        <a href="<?php echo $company_url.'/news'?>" style="color: <?php echo $colour_link;?>">More News</a>
      </div>
    </div>
  </div>
  <div class="company-right-box">
    <div class="right-subject-title" style="background: <?php echo $colour_menu;?>">Members</div>
    <div id="member-right-content" class="company-right-box-content">
      <form action="" id="member-login" method="post" name="member-login">
        <div class="form-group">
          <label for="email">Email</label>
          <input type="email" class="form-control" id="email" placeholder="Email" name="email">
        </div>
        <div class="form-group">
          <label for="password">Password</label>
          <input type="password" class="form-control" id="password" placeholder="Password" name="password">
        </div>
        <div class="form-group clearfix">
          <button type="submit" class="col-md-6 col-xs-12 btn btn-primary btn-block" style="background: <?php echo $colour_menu;?>">Login</button>
          <a href="" class="col-md-6 col-xs-12 btn btn-default btn-block">Sign Up</a>
        </div>
        <div class="form-group">
          <a href="" id="forget-pass-link">Forget Password?</a>
        </div>
      </form>
    </div>
  </div>
