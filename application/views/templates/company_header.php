<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $meta_title; ?></title>
    <meta property="og:title" content="<?php echo $meta_title; ?>" />
    <meta property="og:type" content="<?php echo $meta_type;?>" /> 
    <meta property="og:image" content="<?php echo $meta_image; ?>" />
    <meta property="og:url" content="<?php echo $meta_url; ?>" />
    <meta property="og:site_name" content="<?php echo $meta_name; ?>" />
    <meta property="og:description" content="<?php echo $meta_description;?>" />
    <meta name="description" content="<?php echo $meta_description;?>" />
    
    
    <link href="<?php echo base_url('assets/css/reset.css');?>" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Trirong" rel="stylesheet">

    <!-- Bootstrap -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/bootstrap-datetimepicker.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/jquery-ui.min.css');?>" rel="stylesheet">
    <?php $randCss = rand(10000, 99999); ?>
    <link href="<?php echo base_url('assets/css/manufacturer.css').'?'.$randCss;?>" rel="stylesheet">

    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.1.0.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-ui.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>

    <!-- Google Analytics -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-100471675-1', 'auto');
      ga('send', 'pageview');

    </script>

	
	<?php
		if(isset($this->js))
		{
			foreach ($this->js as $js)
			{
				echo '<script type="text/javascript" src="'.base_url.'views/'.$js.'"></script>';
			}
		}
	?>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php 
        if(!empty($map['js'])){
            echo $map['js'];
        }

        $slide_url = 'https://s3-ap-southeast-1.amazonaws.com/manufacturer-bucket/company-images/';
        $company = $companyData[0];
        $logo_url = 'https://s3-ap-southeast-1.amazonaws.com/manufacturer-bucket/logo/'.$company->logo;
        $company_url = base_url().$languageCode.'/'.$company->web_url;
        $colour_menu = ($company->colour_menu != "") ? $company->colour_menu : "";
        $colour_link = ($company->colour_link != "") ? $company->colour_link : "";
        $bgcolor = ($company->bgcolor != "") ? $company->bgcolor : "#FFFFFF";

    ?>
    

  </head>
  <body style="background-color: <?php echo $bgcolor; ?>">
    <div class="container">
      <div class="company-content-wrapper">
        <?php if(isset($companySlideImage) && !empty($companySlideImage)):?>
          <div id="main-carousel-slide" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
              <?php $i=0; foreach($companySlideImage as $csi): ?>
                  <div class="item <?php echo ($i == 0) ? "active" : ""; ?>">
                    <img src="<?php echo $slide_url.$csi->image_url; ?>" />
                  </div>
              <?php $i++; endforeach;?>
            </div>
          </div>
        <?php endif;?>

        <div id="logo-menu" class="row">
          <div id="logo" class="col-md-2 col-xs-12">
            <img src="<?php echo $logo_url; ?>" />
          </div>
          <div id="menu" class="col-md-7 col-xs-12">
            <?php if(!empty($companyMenu)):?>
              <ul class="nav nav-pills nav-justified">
                <?php foreach($companyMenu as $cm):?>
                  <li role="presentation">
                    <? $urlCode = ($cm->url_code == "/") ? "" : "/".$cm->url_code; ?>
                    <? $urlLink = ($cm->url_code == "fb") ? $cm->link : $company_url.$urlCode; ?>
                    <? $target  = ($cm->url_code == "fb") ? "target='_blank'" : "";?>
                    <? $active  = ($cm->url_code == $currentPage) ? "active" : "";?>
                    <? $colorM  = ($active == "active") ?  $colour_link : "#808184"; ?>
                    <a href="<?php echo $urlLink; ?>" 
                       style="color: <?php echo $colorM; ?>" 
                       class="<?php echo $active; ?>"
                       <?php echo $target?>>
                      <?php echo strtoupper(trim($cm->name)); ?>
                    </a>
                  </li>
                <?php endforeach;?>
              </ul>
            <?php endif;?>
          </div>
          <div id="language" class="col-md-3 col-xs-12">
            <div class="row">
              <?php if(!empty($companyLanguage)):?>
                <ul class="nav nav-pills nav-justified">
                  <? if(count($companyLanguage) > 1): ?>
                    <?php foreach($companyLanguage as $cl):?>
                      <li role="presentation">
                        <? $active  = ($cl->code == $languageCode) ? "active" : "";?>
                        <? $colorM  = ($active == "active") ?  $colour_link : "#808184"; ?>
                        <a href="<?php echo base_url().$cl->code.'/'.$company->web_url; ?>" 
                           style="color:  <?php echo $colorM; ?>" 
                           class="<?php echo $active; ?>">
                          <?php echo strtoupper(trim($cl->code));?>
                        </a>
                      </li> 
                    <?php endforeach;?>
                  <?php endif;?>
                </ul>
              <?php endif;?>
            </div>
          </div>
        </div><!-- logo-menu -->  

      </div>
    </div>
    <script type="text/javascript">
      $('.carousel').carousel({
        interval: 5000
      });
    </script> 

	
