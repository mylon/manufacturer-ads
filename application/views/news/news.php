<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$company = $companyData[0];
$company_profile = $companyProfile[0];
$company_category = $companyCategory[0];
$company_news = $companyaNews[0];
$company_url = base_url().$languageCode.'/'.$company->web_url;
$logo_url = 'https://s3-ap-southeast-1.amazonaws.com/manufacturer-bucket/logo/'.$company->logo;
$news_url = 'https://s3-ap-southeast-1.amazonaws.com/manufacturer-bucket/company-news/';
$author_url = 'https://s3-ap-southeast-1.amazonaws.com/manufacturer-bucket/company-author/';

$colour_menu = ($company->colour_menu != "") ? $company->colour_menu : "";
$colour_link = ($company->colour_link != "") ? $company->colour_link : "";
$authorImg = ($company_news->author_photo != "") ? $author_url.$company_news->author_photo : base_url('assets/img/default-profile.png') ;

?>

<div class="container" id="company-content">
	<div class="company-content-wrapper">
		<div id="company-news">
			<div id="company-breadcrumb">
				<ul>
					<li><a href="<?php echo $company_url;?>"><?= lang('custom_menu_home'); ?></a></li>
					<li>&gt;<li/>
					<li><a href="<?php echo $company_url;?>/news" style="padding-left:5px;"><?= lang('custom_menu_news'); ?></a></li>
					<li>&gt;<li/>
					<li class="active" style="padding-left:5px; color: <?php echo $colour_link;?>"><?php echo substr(ucfirst($company_news->title),0,50); ?></li>
				</ul>
			</div>
			<div class="row" id="company-profile-content">
				<div id="company-profile-content" class="col-md-8 col-xs-12">
					<div id="company-news-content">
						<div id="company-news-title">
							<h1><?php echo strtoupper(trim($company_news->title)); ?></h1>
						</div>
						<div id="company-news-author">
							<div class="row">
								<div class="col-md-2 col-xs-12">
									<img src="<?php echo $authorImg?>" class="img-circle"/>
								</div>
								<div class="col-md-10 col-xs-12 author-details">
									<div id="company-news-date">
										<?php echo date('M d, Y @ h:iA', strtotime($company_news->created_date)); ?>
									</div>
									<div id="author-name"><?php echo ucfirst(trim($company_news->author_name)); ?></div>
									<div id="author-job"><?php echo strtoupper(trim($company_news->author_job)); ?></div>
								</div>  
							</div>
						</div>
						<div id="company-news-description">
							<?php echo htmlspecialchars_decode($company_news->description); ?>
						</div>
					</div>
				</div><!-- company-profile -->
				<div id="company-right-content" class="col-md-4 col-xs-12">
					