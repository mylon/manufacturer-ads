<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$company = $companyData[0];
$company_profile = $companyProfile[0];
$company_category = $companyCategory[0];
$company_url = base_url().$languageCode.'/'.$company->web_url;
$logo_url = 'https://s3-ap-southeast-1.amazonaws.com/manufacturer-bucket/logo/'.$company->logo;
$news_url = 'https://s3-ap-southeast-1.amazonaws.com/manufacturer-bucket/company-news/';


$colour_menu = ($company->colour_menu != "") ? $company->colour_menu : "";
$colour_link = ($company->colour_link != "") ? $company->colour_link : "";

?>

<div class="container" id="company-content">
	<div class="company-content-wrapper">
		<div id="company-news">
			<div id="company-breadcrumb">
				<ul>
					<li><a href="<?php echo $company_url;?>"><?= lang('custom_menu_home'); ?></a></li>
					<li>&gt;<li/>
					<li class="active" style="padding-left:5px; color: <?php echo $colour_link;?>"><?= lang('custom_menu_news'); ?></li>
				</ul>
			</div>
			<!--<div id="company-header">
				<h1><?php echo lang('custom_menu_news').' : '.$company_profile->name; ?></h1>
			</div>-->
			<div class="row" id="company-profile-content">
				<div id="company-profile-content" class="col-md-8 col-xs-12">
					<div id="subject-title" style="background: <?php echo $colour_menu;?>">News</div>
					<div id="company-news-lists">
					<?php if(!empty($companyNewsList)):?>
							<?php foreach($companyNewsList as $cn):?>
								<div class="company-news-list">
									<div class="row">
										<div class="company-news-image col-md-2 col-xs-12">
											<?php $newsImg = ($cn->images != "") ? $news_url.$cn->images : $logo_url; ?>
											<img src="<?php echo $newsImg; ?>" alt="<?php echo $cn->title; ?>" />
										</div>
										<div class="company-news-title col-md-10 col-xs-12">
											<a href="<?php echo $company_url.'/news/'.$cn->id; ?>" 
											   style="color: <?php echo $colour_link;?>">
											   <?php echo $cn->title." [ ". date('d/m/y', strtotime($cn->created_date))." ] " ; ?>
											</a>
										</div>
									</div>
								</div>
							<?php endforeach;?>
						<?php endif;?>
					</div>
				</div><!-- company-profile -->
				<div id="company-right-content" class="col-md-4 col-xs-12">
					