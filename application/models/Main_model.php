<?php

Class Main_model extends CI_Model {
	
	public function checkLanguagePath($uri) {

		$uri = explode("/", $uri);
		$lang = $uri[0];

		$condition = "code =" . "'" . $lang . "'";
		$this->db->select('*');
		$this->db->from('language');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if($query->num_rows() == 1) {
			return $lang;
		} else {
			return false;
		}

	}

	public function checkCurrentPage($uri) {
		$uri = explode("/", $uri);
		if (isset($uri[2]) && !empty($uri[2])) {
			return $uri[2];
		}else {
			return "/";
		}
	}

	public function getLanguageID($lang) {
		
		$condition = "code =" . "'" . $lang . "'";
		$this->db->select('id');
		$this->db->from('language');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			$result = $query->result();
			return  $result[0]->id;
		} else {
			return false;
		}
	}

	public function checkCompanyPath($uri) {

		$uri = explode("/", $uri);
		$company_path = $uri[1];

		$condition = "web_url =" . "'" . $company_path . "'";
		$this->db->select('*');
		$this->db->from('company');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function getCompanySlideImage($company_id) {

		$condition = "company_id =" . "'" . $company_id . "'";
		$this->db->select('*');
		$this->db->from('company_images');
		$this->db->where($condition);
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function getLanguageListfromCompanyID($company_id) {
		$condition = "cp.company_id =" . "'" . $company_id . "'";
		$this->db->select('l.*');
		$this->db->from('language l');
		$this->db->join('company_profile cp', 'cp.language_id = l.id', 'left');
		$this->db->where($condition);
		$this->db->order_by("l.code", "asc"); 

		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function getCompanyProfile($company_id, $language_id) {
		$condition = "cp.company_id =" . "'" . $company_id . "' AND cp.language_id = '" . $language_id . "'";
		$this->db->select('cp.*');
		$this->db->from('company_profile cp');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function getCompanyMenu($company_id, $language_id) {

		$condition = "cm.company_id =" . "'" . $company_id . "' AND m.language_id = '" . $language_id . "'";
		$this->db->select('cm.*, m.url as url_code');
		$this->db->from('company_menu cm');
		$this->db->join('menu m', 'cm.menu_id = m.id', 'left');
		$this->db->where($condition);
		$this->db->order_by("cm.order_menu", "asc"); 

		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function getCompanyNews($company_id, $language_id) {
		$condition = "cn.company_id =" . "'" . $company_id . "' AND cn.language_id = '" . $language_id . "'";
		$this->db->select('cn.*');
		$this->db->from('company_news cn');
		$this->db->where($condition);
		$this->db->limit(10);
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	//Have to fix just for demo
	public function getCategoryList($id) {
		$condition = "id =" . "'" . $id . "'";
		$this->db->select('*');
		$this->db->from('sub_company_category_profile');
		$this->db->where($condition);
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function getProductCategoryList($company_id, $language_id) {
		$condition = "pc.company_id =" . "'" . $company_id . "' AND pcp.language_id = '" . $language_id . "'";
		$this->db->select('pcp.*, pc.name as main_category_name, pc.id as category_id');
		$this->db->from('products_category_profile pcp');
		$this->db->join('products_category pc', 'pc.id = pcp.products_category_id', 'left');
		$this->db->where($condition);
		$this->db->order_by("pcp.products_category_id", "asc"); 
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function getCompanyProudcts($company_id, $language_id) {
		$condition = "cp.company_id =" . "'" . $company_id . "' AND cpp.language_id = '" . $language_id . "'";
		$this->db->select('cpp.*, cp.images as images, cp.category_id as category_id');
		$this->db->from('company_products_profile cpp');
		$this->db->join('company_products cp', 'cp.id = cpp.company_products_id', 'left');
		$this->db->where($condition);
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function getNewsCategoryList($company_id, $language_id) {
		$condition = "nc.company_id =" . "'" . $company_id . "' AND ncp.language_id = '" . $language_id . "'";
		$this->db->select('ncp.*, nc.name as main_category_name, nc.id as category_id');
		$this->db->from('news_category_profile ncp');
		$this->db->join('news_category nc', 'nc.id = ncp.news_category_id', 'left');
		$this->db->where($condition);
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}
}

?>
