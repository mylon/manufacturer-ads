<?php

Class Index_model extends CI_Model {
	
	public function getNewsList($limit, $start, $search) {
		$this->db->select('company_news.*, c.name as company_name, s1.username as created_by, s2.username as edited_by');
		$this->db->from('company_news');
		$this->db->join('staff s1', 's1.id = company_news.created_by', 'left');
		$this->db->join('staff s2', 's2.id = company_news.edited_by', 'left');
		$this->db->join('company c', 'c.id = company_news.company_id', 'left');

		/// search
		if(!empty($search['title'])) :
			$this->db->where("company_news.title LIKE '%".$search['title']."%'");
		endif;

		if(!empty($search['start_date'])) :
			$this->db->where("company_news.created_date >= '".$search['start_date']. ' 00:00:00' ."'");
		endif;

		if(!empty($search['end_date'])) :
			$this->db->where("company_news.created_date < '".$search['end_date']. ' 23:59:59' ."'");
		endif;

		if(!empty($search['created_by'])) :
			$this->db->where('company_news.created_by', $search['created_by']);
		endif;
		/// endsearch

		$this->db->order_by("updated_date", "desc"); 
		$this->db->limit($limit, $start); 


		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function countNews($search) {

		$this->db->select('*');
		$this->db->from('company_news');

		/// search
		if(!empty($search['title'])) :
			$this->db->where("title LIKE '%".$search['title']."%'");
		endif;

		if(!empty($search['start_date'])) :
			$this->db->where("created_date >= '".$search['start_date']. ' 00:00:00' ."'");
		endif;

		if(!empty($search['end_date'])) :
			$this->db->where("created_date < '".$search['end_date']. ' 23:59:59' ."'");
		endif;

		if(!empty($search['created_by'])) :
			$this->db->where('created_by', $search['created_by']);
		endif;
		/// endsearch

		$query = $this->db->get();

		return $query->num_rows();
	}

	
}

?>
