<?php

Class Search_model extends CI_Model {
	
	public function getLanguateList() {

		$this->db->select('id, name');
		$this->db->from('language');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();

		$language = ($query->num_rows() >= 1) ? $query->result() : array();

		return $language;

	}

	public function getCountryList() {

		$this->db->select('id, name');
		$this->db->from('country');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		
		$country = ($query->num_rows() >= 1) ? $query->result() : array();

		return $country;

	}

	public function getCompnayCategoryList() {

		$this->db->select('id, name');
		$this->db->from('company_category');
		$this->db->order_by("name", "asc");
		$query = $this->db->get();
		
		$country = ($query->num_rows() >= 1) ? $query->result() : array();

		return $country;

	}


	
	
}

?>
