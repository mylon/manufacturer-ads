<?php

Class Products_model extends CI_Model {


	public function getProductCategoryByID($company_id, $language_id, $category_id) {
		$condition = "pc.company_id =" . "'" . $company_id . "' AND pcp.language_id = '" . $language_id . "' AND pcp.products_category_id = '" . $category_id . "'";
		$this->db->select('pcp.*, pc.name as main_category_name, pc.id as category_id');
		$this->db->from('products_category_profile pcp');
		$this->db->join('products_category pc', 'pc.id = pcp.products_category_id', 'left');
		$this->db->where($condition);
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function getCompanyProudctsByCatID($company_id, $language_id, $category_id) {
		$condition = "cp.company_id =" . "'" . $company_id . "' AND cpp.language_id = '" . $language_id . "' AND cp.category_id = '" . $category_id . "'";
		$this->db->select('cpp.*, cp.images as images, cp.category_id as category_id');
		$this->db->from('company_products_profile cpp');
		$this->db->join('company_products cp', 'cp.id = cpp.company_products_id', 'left');
		$this->db->where($condition);
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function getCompanyProudctsByID($company_id, $language_id, $product_id) {
		$condition = "cp.company_id =" . "'" . $company_id . "' AND cpp.language_id = '" . $language_id . "' AND cpp.id = '" . $product_id . "'";
		$this->db->select('cpp.*, cp.images as images, cp.category_id as category_id, pc.name as br_cat_name, pcp.name as lang_cat_name');
		$this->db->from('company_products_profile cpp');
		$this->db->join('company_products cp', 'cp.id = cpp.company_products_id', 'left');
		$this->db->join('products_category pc', 'pc.id = cp.category_id', 'left');
		$this->db->join('products_category_profile pcp', 'pc.id = pcp.products_category_id AND cpp.language_id = pcp.language_id', 'left');
		$this->db->where($condition);

		$this->db->where($condition);
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	
}

?>
