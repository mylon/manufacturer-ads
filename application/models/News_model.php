<?php

Class News_model extends CI_Model {


	public function getCompanyNewsList($company_id, $language_id) {
		$condition = "cn.company_id =" . "'" . $company_id . "' AND cn.language_id = '" . $language_id . "'";
		$this->db->select('cn.*');
		$this->db->from('company_news cn');
		$this->db->where($condition);
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

	public function getCompanyNewsfromID($id, $company_id , $language_id) {
		$condition = "cn.company_id =" . "'" . $company_id . "' AND cn.language_id = '" . $language_id . " ' AND cn.id = '" . $id . "'";
		$this->db->select('cn.*');
		$this->db->from('company_news cn');
		$this->db->where($condition);
		$this->db->limit(1); 
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return  $query->result();
		} else {
			return false;
		}
	}

}

?>
