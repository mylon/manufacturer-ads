<?php

//defined('BASEPATH') OR exit('No direct script access allowed');

//session_start(); //we need to start session in order to access it through CI

Class Productscontroller extends CI_Controller {

	public function __construct() {

		parent::__construct();

		// Load database
		$this->load->model('products_model');
		$this->lang->load('custom');

		$this->language 	= $this->main_model->checkLanguagePath(uri_string());
		$this->company  	= $this->main_model->checkCompanyPath(uri_string());
		$this->langID 		= $this->main_model->getLanguageID($this->language);
		$this->currentPage 	= $this->main_model->checkCurrentPage(uri_string());
		if($this->language === false || $this->company === false) redirect('indexcontroller/index');

	}

	public function index() {

		$data = array();

		$data['companyData'] = $this->company;
		$data['languageCode'] = $this->language;
		$data['currentPage'] = $this->currentPage;
		$data['companySlideImage'] = $this->main_model->getCompanySlideImage($data['companyData'][0]->id);
		$data['companyLanguage']   = $this->main_model->getLanguageListfromCompanyID($data['companyData'][0]->id);
		$data['companyMenu']   = $this->main_model->getCompanyMenu($data['companyData'][0]->id, $this->langID);
		$data['companyNews'] = $this->main_model->getCompanyNews($data['companyData'][0]->id, $this->langID);
		$data['companyProfile']   = $this->main_model->getCompanyProfile($data['companyData'][0]->id, $this->langID);
		$data['companyCategory'] = $this->main_model->getCategoryList($data['companyProfile'][0]->sub_category_id);
		$data['productCategory'] = $this->main_model->getProductCategoryList($data['companyData'][0]->id, $this->langID);
		$data['companyProducts'] = $this->main_model->getCompanyProudcts($data['companyData'][0]->id, $this->langID);
		$data['newsCategory'] = $this->main_model->getNewsCategoryList($data['companyData'][0]->id, $this->langID);

		$data['meta_title'] = $data['companyProfile'][0]->product_meta_title;
		$data['meta_name'] = $data['companyProfile'][0]->name;
		$data['meta_description'] = str_replace("/", "", $data['companyProfile'][0]->product_meta_description);
		$data['meta_url'] = base_url().$data['languageCode'].'/'.$data['companyData'][0]->web_url.'/products';
		$data['meta_image'] = 'https://s3-ap-southeast-1.amazonaws.com/manufacturer-bucket/logo/'.$data['companyData'][0]->logo;
		$data['meta_type'] = "article";
		

		$this->load->company_template('products/index', $data);
	}

	public function category($lang, $company, $category_id) {
		$data['companyData'] = $this->company;
		$data['languageCode'] = $this->language;
		$data['currentPage'] = $this->currentPage;
		$data['companySlideImage'] = $this->main_model->getCompanySlideImage($data['companyData'][0]->id);
		$data['companyLanguage']   = $this->main_model->getLanguageListfromCompanyID($data['companyData'][0]->id);
		$data['companyMenu']   = $this->main_model->getCompanyMenu($data['companyData'][0]->id, $this->langID);
		$data['companyNews'] = $this->main_model->getCompanyNews($data['companyData'][0]->id, $this->langID);
		$data['companyProfile']   = $this->main_model->getCompanyProfile($data['companyData'][0]->id, $this->langID);
		$data['companyCategory'] = $this->main_model->getCategoryList($data['companyProfile'][0]->sub_category_id);
		$data['productCategory'] = $this->products_model->getProductCategoryByID($data['companyData'][0]->id, $this->langID, $category_id);
		$data['companyProducts'] = $this->products_model->getCompanyProudctsByCatID($data['companyData'][0]->id, $this->langID, $category_id);
		$data['newsCategory'] = $this->main_model->getNewsCategoryList($data['companyData'][0]->id, $this->langID);
		$data['productCategoryR'] = $this->main_model->getProductCategoryList($data['companyData'][0]->id, $this->langID);
		$data['companyProductsR'] = $this->main_model->getCompanyProudcts($data['companyData'][0]->id, $this->langID);

		$data['meta_title'] = ucfirst($data['productCategory'][0]->name).' : '.$data['companyProfile'][0]->title;
		$data['meta_name'] = $data['companyProfile'][0]->name;
		$data['meta_description'] = str_replace("/", "", ucfirst($data['productCategory'][0]->name));
		$data['meta_url'] = base_url().$data['languageCode'].'/'.$data['companyData'][0]->web_url.'/products/category/'.$data['companyProducts'][0]->category_id;
		$data['meta_image'] = 'https://s3-ap-southeast-1.amazonaws.com/manufacturer-bucket/logo/'.$data['companyData'][0]->logo;
		$data['meta_type'] = "article";

		$this->load->company_template('products/category', $data);
	}

	public function product($lang, $company, $product_id) {
		$data['companyData'] = $this->company;
		$data['languageCode'] = $this->language;
		$data['currentPage'] = $this->currentPage;
		$data['companySlideImage'] = $this->main_model->getCompanySlideImage($data['companyData'][0]->id);
		$data['companyLanguage']   = $this->main_model->getLanguageListfromCompanyID($data['companyData'][0]->id);
		$data['companyMenu']   = $this->main_model->getCompanyMenu($data['companyData'][0]->id, $this->langID);
		$data['companyNews'] = $this->main_model->getCompanyNews($data['companyData'][0]->id, $this->langID);
		$data['companyProfile']   = $this->main_model->getCompanyProfile($data['companyData'][0]->id, $this->langID);
		$data['companyCategory'] = $this->main_model->getCategoryList($data['companyProfile'][0]->sub_category_id);
		$data['companyProduct'] = $this->products_model->getCompanyProudctsByID($data['companyData'][0]->id, $this->langID, $product_id);
		$data['companyProducts'] = $this->main_model->getCompanyProudcts($data['companyData'][0]->id, $this->langID);
		$data['newsCategory'] = $this->main_model->getNewsCategoryList($data['companyData'][0]->id, $this->langID);
		$data['productCategory'] = $this->main_model->getProductCategoryList($data['companyData'][0]->id, $this->langID);

		$logo_url = 'https://s3-ap-southeast-1.amazonaws.com/manufacturer-bucket/logo/'.$data['companyData'][0]->logo;
		$data['meta_title'] = $data['companyProducts'][0]->meta_title;
		$data['meta_name'] = $data['companyProfile'][0]->name;
		$data['meta_description'] = str_replace("/", "", $data['companyProducts'][0]->meta_description);
		$data['meta_url'] =  base_url().$data['languageCode'].'/'.$data['companyData'][0]->web_url.'/products/'.$data['companyProducts'][0]->id;
		$data['meta_image'] = ($data['companyProducts'][0]->images != "") ? 'https://s3-ap-southeast-1.amazonaws.com/manufacturer-bucket/company-products/'.$data['companyProducts'][0]->images : $logo_url;
		$data['meta_type'] = "article";

		$this->load->company_template('products/product', $data);
	}


}
