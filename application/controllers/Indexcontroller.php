<?php

//defined('BASEPATH') OR exit('No direct script access allowed');

//session_start(); //we need to start session in order to access it through CI

Class Indexcontroller extends CI_Controller {

	public function __construct() {
		parent::__construct();
		// Load database
		$this->load->model('index_model');
		$this->load->model('search_model');
	}

	public function index() {

		$data = array();

		$data['quick_search'] = (!empty($this->input->post('quick_search'))) ? strtolower(trim($this->input->post('quick_search'))) : "";
		$data['language'] 	  = (!empty($this->input->post('language'))) ? strtolower(trim($this->input->post('language'))) : "";
		$data['country']	  = (!empty($this->input->post('country'))) ? strtolower(trim($this->input->post('country'))) : "";

		$data['languageList'] = $this->search_model->getLanguateList();
		$data['countryList'] = $this->search_model->getCountryList();
		$data['comapanyCategoryList'] = $this->search_model->getCompnayCategoryList();

		//$this->load->template('index', $data);
	}


}
